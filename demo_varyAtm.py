# -*- coding: utf-8 -*-
"""
demo_varyAtm.py:
    Demonstration of the impact of varying atmospheres for aerocapture trajs.

Created on Mon Oct 25 17:17:07 2021

@author: Samuel Albert
"""

from src import sim
from src import planetaryConstants
from src import atmosphere
from src import conversions

import numpy as np
import matplotlib.pyplot as plt
import copy

plt.close('all')

mars = planetaryConstants.MARS
mars.atmBaseFilename = 'data/MarsGRAM_MC5000/'

shield = sim.Vehicle('SHIELD')
shield.BC = 130 # kg/m^2
shield.LD = 0
shield.getAero = lambda r, u, Planet: (shield.BC, shield.LD)

params = sim.Params()
params.rtol = 1e-10
params.atol = 1e-10
params.tspan = (0, 10e3) # s
params.rmin = mars.rad
params.rmax = (mars.rad + mars.halt)

fig = plt.figure()
ax = fig.add_subplot(111)

for Natm in range(1,20):
    # =============================================================================
    # Set up base Planet, Vehicle, and Params objects
    # =============================================================================
    mars.atmfilename = mars.atmBaseFilename + str(Natm) + '.txt'
    # atmfilename = 'data/MarsGRAMNominal.txt'
    mars.hList, mars.rhoList = atmosphere.readMarsGRAM(mars.atmfilename,
                                                       perturbed = True)
    mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)
    
    # =============================================================================
    # Set up spherical coordinates case
    # =============================================================================
    shieldSph = copy.deepcopy(shield)
    shieldSph.bankFun = lambda t, yy: 0
    
    r0 = (mars.rad + mars.halt) # m
    lon0 = np.radians(30)
    lat0 = np.radians(40)
    u0 = 6e3 # m/s
    gam0 = np.radians(-10.65)
    hda0 = np.radians(90)
    shieldSph.y0 = np.array([r0, lon0, lat0, u0, gam0, hda0])
    
    paramsSph = copy.deepcopy(params)
    
    event1Sph = lambda t, y: sim.belowMinAltSph(t, y, params)
    event1Sph.terminal = True
    
    event2Sph = lambda t, y: sim.aboveMaxAltSph(t, y, params)
    event2Sph.terminal = True
    event2Sph.direction = 1
    
    paramsSph.events = (event1Sph, event2Sph)
    
    # =============================================================================
    # Propagate
    # =============================================================================
    solSph = sim.runSpherical(mars, shieldSph, paramsSph)
    
    # =============================================================================
    # Wrap latitude and longitude for spherical results
    # =============================================================================
    solSph.y = conversions.wrapLonLatArray(solSph.y)
    
    # =============================================================================
    # Plot trajectories
    # =============================================================================
    # ax.plot(solSph.y[3,:]/1e3, (solSph.y[0,:] - mars.rad)/1e3)
    ax.plot(solSph.t, (solSph.y[0,:] - mars.rad)/1e3)


ax.set_xlabel('velocity, km/s')
ax.set_ylabel('altitude, km')
ax.grid()
plt.tight_layout()