# -*- coding: utf-8 -*-
"""
atmosphere.py:
    support functions related to atmosphere models
    
Created on Mon Oct 25 17:55:11 2021

@author: Samuel Albert
"""

import numpy as np
import pandas as pd

def readMarsGRAM(filename, perturbed = False):
    df = pd.read_csv(filename, delim_whitespace = True)
    hList = df.HgtMOLA * 1000
    if perturbed:
        rhoList = df.Denkgm3 * df.DensP
    else:
        rhoList = df.Denkgm3
    
    return np.asarray(hList), np.asarray(rhoList)
