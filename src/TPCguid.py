# -*- coding: utf-8 -*-
"""
TPCguid.py:
    contains functions for terminal point controller aerocapture guidance
Created on Thu Nov  4 14:02:55 2021

@author: Samuel Albert
"""

import sys
sys.path.append('..')

from src import UQ
from src import sim
from src import conversions


import numpy as np
from scipy.integrate import solve_ivp

def getTC(refTraj, Planet):
    '''
    computes terminal conditions for costate for TPC alg., using Appendix C-2
    '''
    # get variables from refTraj
    xxfvec = refTraj.xxfvec
    tf = refTraj.tf
    rcTarget = refTraj.rcTarget
    
    # get spherical final state
    xxfsph = conversions.cartI2sphPR(xxfvec, Planet, tf)
    
    # rename some variables
    mu = Planet.mu
    rc = rcTarget
    rf = xxfsph[0]
    vf = xxfsph[3]
    gamf = xxfsph[4]
    
    # get energy and angular momentum magnitude at exit/apoapsis
    Hf = rf*vf*np.cos(gamf) # (4-C)
    engf = vf**2/2 - mu/rf # (3-C)
    
    # get radius and velocity at apoapsis
    va = (mu - np.sqrt(mu**2 + 2*engf*Hf**2)) / Hf # (11-C)
    ra = Hf / va # (23-C)
    
    # get v1 and v2
    v1 = np.sqrt(2*mu*rc / (ra*(ra + rc))) # (20-C)
    v2 = np.sqrt(2*mu*ra / (rc*(ra + rc))) # (17-C)
    
    # compute partials of delta-V w.r.t. exit velocity, dDV_dvf
    dengf_dvf = vf # (40-C)
    dHf_dvf = rf * np.cos(gamf) # (39-C)
    dva_dvf = (-va/Hf - 2*engf / np.sqrt(mu**2 + 2*engf*Hf**2)) * dHf_dvf\
               - Hf / np.sqrt(mu**2 + 2*engf*Hf**2) * dengf_dvf # (37-C)
    dra_dvf = 1/va * dHf_dvf - Hf/va**2 * dva_dvf # (38-C)
    dv1_dvf = -mu*rc*(2*ra + rc) / (v1*ra**2*(ra + rc)**2) * dra_dvf # (35-C)
    dv2_dvf = mu/(v2*(ra + rc)**2) * dra_dvf # (36-C)
    dDV_dvf = dv1_dvf - dv2_dvf - dva_dvf # (34-C)
    
    # compute partials of delta-V w.r.t. exit flight-path angle, dDV_dgamf
    dengf_dgamf = 0 # (46-C)
    dHf_dgamf = -rf*vf*np.sin(gamf) # (45-C)
    dva_dgamf = (-va/Hf - 2*engf / np.sqrt(mu**2 + 2*engf*Hf**2)) * dHf_dgamf\
               - Hf / np.sqrt(mu**2 + 2*engf*Hf**2) * dengf_dgamf # (43-C)
    dra_dgamf = 1/va * dHf_dgamf - Hf/va**2 * dva_dgamf # (44-C)
    dv1_dgamf = -mu*rc*(2*ra + rc) / (v1*ra**2*(ra + rc)**2) * dra_dgamf#(41-C)
    dv2_dgamf = mu/(v2*(ra + rc)**2) * dra_dgamf # (42-C)
    dDV_dgamf = dv1_dgamf - dv2_dgamf - dva_dgamf # (34-C)
    
    # compute partials of delta-V w.r.t. exit altitude, dDV_dhf
    dengf_dhf = mu / rf**2 # (52-C)
    dHf_dhf = vf * np.cos(gamf) # (51-C)
    dva_dhf = (-va/Hf - 2*engf / np.sqrt(mu**2 + 2*engf*Hf**2)) * dHf_dhf\
               - Hf / np.sqrt(mu**2 + 2*engf*Hf**2) * dengf_dhf # (49-C)
    dra_dhf = 1/va * dHf_dhf - Hf/va**2 * dva_dhf # (50-C)
    dv1_dhf = -mu*rc*(2*ra + rc) / (v1*ra**2*(ra + rc)**2) * dra_dhf # (47-C)
    dv2_dhf = mu/(v2*(ra + rc)**2) * dra_dhf # (48-C)
    dDV_dhf = dv1_dhf - dv2_dhf - dva_dhf # (34-C)
    
    lambda_s_f = 0 # (29-C)
    lambda_v_f = dDV_dvf # (30-C)
    lambda_gam_f = dDV_dgamf # (31-C)
    lambda_h_f = dDV_dhf # (32-C)
    lambda_u_f = 0 # not in paper, but confirmed with Eric Queen 11.9.21
    
    return np.array([lambda_h_f, lambda_v_f, lambda_gam_f, lambda_s_f,
                     lambda_u_f])

def getRefState(t, refTraj):
    '''
    interpolate reference trajectory states at current time.
    refTrajVecList = [tNomvec, vNomvec, gamNomvec, hNomvec, L_mNomvec,
                      D_mNOmvec, bankNomvec]
    '''
    
    refState = sim.Traj()
    
    refState.v = np.interp(t, refTraj.t, refTraj.v)
    refState.gam = np.interp(t, refTraj.t, refTraj.gam)
    refState.h = np.interp(t, refTraj.t, refTraj.h)
    refState.L_m = np.interp(t, refTraj.t, refTraj.L_m)
    refState.D_m = np.interp(t, refTraj.t, refTraj.D_m)
    refState.bank = np.interp(t, refTraj.t, refTraj.bank)
    refState.eng = np.interp(t, refTraj.t, refTraj.eng)
    refState.hdot = np.interp(t, refTraj.t, refTraj.hdot)
    refState.Hscale = np.interp(t, refTraj.t, refTraj.Hscale)
    refState.gLoad = np.interp(t, refTraj.t, refTraj.gLoad)
    
    refState.r = np.interp(t, refTraj.t, refTraj.r)
    refState.lon = np.interp(t, refTraj.t, refTraj.lon)
    refState.lat = np.interp(t, refTraj.t, refTraj.lat)
    refState.hda = np.interp(t, refTraj.t, refTraj.hda)
    refState.rho = np.interp(t, refTraj.t, refTraj.rho)
    
    return refState

def TPCactive(refState, Params):
    g = refState.gLoad
    eng = refState.eng
    
    if (g < Params.gThresh) or (eng < Params.engThresh):
        return False
    else:
        return True

def getdlambdadt(t, yy, refTraj, Planet, Vehicle, Params):
    '''
    ODEs for costate vector, Eqs. 44-48
    '''
    
    mu = Planet.mu
    LD = Vehicle.LD
    BC = Vehicle.BC
    
    refState = getRefState(t, refTraj)
    r = refState.r
    v = refState.v
    gam = refState.gam
    bank = refState.bank
    rho = refState.rho
    H = refState.Hscale
    
    # get partial w.r.t. states
    dfrdr = 0
    dfrdv = np.sin(gam)
    dfrdgam = v * np.cos(gam)
    dfrdR = 0
    
    dfvdr = rho*v**2 / (2*H*BC) + 2*mu*np.sin(gam)/r**3
    dfvdv = -rho*v/BC
    dfvdgam = -mu*np.cos(gam)/r**2
    dfvdR = 0

    dfgamdr = -rho*v*LD/(2*H*BC)*np.cos(bank)\
        + (2*mu/r**3 - v**2/r**2)*np.cos(gam)/v
    dfgamdv = rho*LD/(2*BC)*np.cos(bank) + np.cos(gam)/r*(mu/(v**2*r) + 1)
    dfgamdgam = (mu/r**2 - v**2/r)*np.sin(gam)/v
    dfgamdR = 0
    
    dfRdr = 0
    dfRdv = np.cos(gam)
    dfRdgam = -v*np.sin(gam)
    dfRdR = 0
    
    A = np.array([[dfrdr, dfrdv, dfrdgam, dfrdR],
                    [dfvdr, dfvdv, dfvdgam, dfvdR],
                    [dfgamdr, dfgamdv, dfgamdgam, dfgamdR],
                    [dfRdr, dfRdv, dfRdgam, dfRdR]])
    
    # compute partials of costates w.r.t. time
    dlamdt = yy[:4] @ A
    # if TPCactive(refState, Params):
    #     dlamu_dt = -rho * v * LD / (2 * BC) * yy[2]
    # else:
    #     dlamu_dt = 0
        
    dlamu_dt = -rho * v * LD / (2 * BC) * yy[2]
    
    return np.block([dlamdt, np.array([dlamu_dt])])

def getlambda(lambdaf, refTraj, Planet, Params, Vehicle):
    '''
    backward propagates the ODEs for the lambda influence coefficients.
    Note: assumes that t0 = 0
    '''
    
    # propagate
    sol = solve_ivp(lambda t, y: getdlambdadt(t, y, refTraj, Planet,
                                              Vehicle, Params),
                    (refTraj.tf, 0), lambdaf, t_eval = refTraj.t[::-1],
                    rtol = Params.rtol, atol = Params.atol)
    
    if not sol.success:
        sys.exit('integration failed when back-propagating lambdas')
    
    # put results in forward-time order
    lambdavec = sol.y[:,::-1]
    
    return lambdavec


def computeGains(lambdavec, refTraj):
    vStar = refTraj.v
    gamStar = refTraj.gam
    D_mStar = refTraj.D_m
    HscaleStar = refTraj.Hscale
    
    # lambda_s = lambdavec[0,:]
    lambda_v = lambdavec[1,:]
    lambda_gam= lambdavec[2,:]
    lambda_h = lambdavec[3,:]
    lambda_u = lambdavec[4,:]
    
    F1 = -HscaleStar * lambda_h / D_mStar
    F2 = lambda_gam / (vStar * np.cos(gamStar))
    F3 = lambda_v
    F4 = lambda_u
    
    return F1, F2, F3, F4

def computeControl(t, xxsph, hdot, L_m, D_m, refTraj, K, F1s, F2s, F3s, F4s,
                   gThresh, planet, returnGains = False, returnStatus = True):
    # get current values of gains, interpolating using energy
    _, _, eng = conversions.getApsesSph(xxsph, planet, t)
    
    v = xxsph[3]
    
    F1 = np.interp(eng, refTraj.eng[::-1], F1s[::-1])
    F2 = np.interp(eng, refTraj.eng[::-1], F2s[::-1])
    F3 = np.interp(eng, refTraj.eng[::-1], F3s[::-1])
    F4 = np.interp(eng, refTraj.eng[::-1], F4s[::-1])
    
    bankStar =  np.interp(eng, refTraj.eng[::-1], refTraj.bank[::-1])
    vStar =     np.interp(eng, refTraj.eng[::-1], refTraj.v[::-1])
    hdotStar =  np.interp(eng, refTraj.eng[::-1], refTraj.hdot[::-1])
    D_mStar =   np.interp(eng, refTraj.eng[::-1], refTraj.D_m[::-1])
    
    if returnGains:
        return K, [F1, F2, F3, F4], bankStar, vStar, hdotStar, D_mStar
    
    if (np.sqrt(L_m**2 + D_m**2) < gThresh) or (eng < 0):
        # not above acceleration threshold, return reference control
        cosBankFinal = np.cos(bankStar)
        TPCstatus = 0
    else:
        # compute control update
        if abs(F4) < 1e-5:
            cosBank = np.cos(bankStar)
        else:
            cosBank = np.cos(bankStar)\
                      - K/F4 * (F3 * (v - vStar)\
                                + F2 * (hdot - hdotStar)\
                                + F1 * (D_m - D_mStar))
        # check for control saturation
        if cosBank < -1:
            TPCstatus = 2
            cosBankFinal = -1
        elif cosBank > 1:
            TPCstatus = 2
            cosBankFinal = 1
        else:
            TPCstatus = 1
            cosBankFinal = cosBank
    
    if returnStatus:
        return cosBankFinal, TPCstatus, np.sqrt(L_m**2 + D_m**2)
    else:
        return cosBankFinal
    
    
# =============================================================================
# LinCov supporting functions related to TPC
# =============================================================================

def getAclC(K, lambdavec, refTraj, planet, vehicle, evals, evecs, d, hList,
            rhoMean, t, perts = True):
    
    # extract states and other values from refTraj
    refState = getRefState(t, refTraj)
    
    r = refState.r
    h = refState.h
    v = refState.v
    gam = refState.gam
    
    bank = refState.bank
    
    H = refState.Hscale
    rho = refState.rho
    
    mu = planet.mu
    
    BC = vehicle.BC
    LD = vehicle.LD
    
    # interpolate costates at this time
    lambdar = np.interp(t, refTraj.t, lambdavec[0])
    lambdav = np.interp(t, refTraj.t, lambdavec[1])
    lambdagam = np.interp(t, refTraj.t, lambdavec[2])
    lambdaR = np.interp(t, refTraj.t, lambdavec[3])
    lambdau = np.interp(t, refTraj.t, lambdavec[4])
    
    # build open-loop A matrix
    dfrdr = 0
    dfrdv = np.sin(gam)
    dfrdgam = v * np.cos(gam)
    dfrdR = 0
    
    dfvdr = rho*v**2 / (2*H*BC) + 2*mu*np.sin(gam)/r**3
    dfvdv = -rho*v/BC
    dfvdgam = -mu*np.cos(gam)/r**2
    dfvdR = 0

    dfgamdr = -rho*v*LD/(2*H*BC)*np.cos(bank) + (2*mu/r**3 - v**2/r**2)*np.cos(gam)/v
    dfgamdv = rho*LD/(2*BC)*np.cos(bank) + np.cos(gam)/r*(mu/(v**2*r) + 1)
    dfgamdgam = (mu/r**2 - v**2/r)*np.sin(gam)/v
    dfgamdR = 0
    
    dfRdr = 0
    dfRdv = np.cos(gam)
    dfRdgam = -v*np.sin(gam)
    dfRdR = 0
    
    A = np.array([[dfrdr, dfrdv, dfrdgam, dfrdR],
                    [dfvdr, dfvdv, dfvdgam, dfvdR],
                    [dfgamdr, dfgamdv, dfgamdgam, dfgamdR],
                    [dfRdr, dfRdv, dfRdgam, dfRdR]])
    
    # build B matrix
    dfrdu = 0
    dfvdu = 0
    dfgamdu = rho * v * LD / (2 * BC)
    dfRdu = 0
    
    B = np.array([dfrdu, dfvdu, dfgamdu, dfRdu])
    
    # build K matrix
    costatevec = np.array([lambdar, lambdav, lambdagam, lambdaR])
    K = - K * costatevec.T / lambdau
    
    # build Acl matrix
    Acl = A + B[:,None] @ K[None,:]
    
    # set up KLE eigenproblem
    evalList, evecList = UQ.evalEproblem(evals, evecs, hList, h, d)
    if perts:
        rhoMeani = np.interp(h, hList, rhoMean)
    
    # compute multipliers for C matrix
    C1fact = 0
    C2fact = -v**2/(2*BC)
    C3fact = v*LD/(2*BC) * np.cos(bank)
    C4fact = 0
    
    # compute C elements using KLE eigenproblem
    Csub = np.empty((4, d))
    Csub[:] = np.NaN
    
    for i in range(d):
        drhodwi = np.sqrt(evalList[i]) * evecList[i]
        if perts:
            drhodwi = (drhodwi + 1) * rhoMeani
        Csub[0,i] = C1fact * drhodwi
        Csub[1,i] = C2fact * drhodwi
        Csub[2,i] = C3fact * drhodwi
        Csub[3,i] = C4fact * drhodwi
    
    C = np.block([np.zeros((4,4)), Csub])
    
    return Acl, C


def getSdot(t, yy, getMats):
    Acl, C = getMats(t)
    
    S = np.reshape(yy, C.shape)
    
    # Acl = np.eye(4)
    
    # Acl[0,2] = 0
    # Acl[2,:] = 0
    
    Sdot = Acl @ S + C
    
    # Sdot = C
    # print('DBUGGING WITHOUT C')
    # Sdot = Acl @ S
    
    dydt = np.reshape(Sdot, (C.size,))
    
    return dydt
    




















