# -*- coding: utf-8 -*-
"""
UQ.py:
    UQ-related functions (KLE, PCE, etc.)
Created on Wed Nov 17 11:31:26 2021

@author: Samuel Albert
"""

import numpy as np
from scipy import stats


def getEproblem(filename, pfact, alpha):
    '''
    takes in a filename and alpha, outputs the eigenvectors, eigenvalues, and d
    INPUTS:
        filename: .npz file with density profiles, in m and kg/m^3
        pfact: perturbation factor IN ADDITION to GRAM dispersions. Set 
            pfact = 1 to use GRAM data directly.
        alpha: accuracy parameter for truncating eigenvalues
    OUTPUTS:
        evals: array of eigenvalues
        evecs: array of eigenvectors ordered same as evals array
        rhoSampMean: sample mean density profile, != GRAM model mean
        d: number of eigenvalues kept for density
        h: altitude profile array, m
    NOTE: 
        d is output but nothing is truncated here, so to set d manually just
        use any value of alpha, call this func., then manually override d value
    '''
    
    # load density from numpy binary file
    dat = np.load(filename)
    h = dat['h']
    rhoData = dat['rhoData']
    
    # get centered density about sample mean
    rhoSampMean = np.mean(rhoData, axis = 1)
    rhoCentered = rhoData - rhoSampMean[:,None]
    rhoCentered *= pfact
    
    # get sample covariance matrix
    covmat = np.cov(rhoCentered) # biasd, by default, for sample estimate
    
    # eigenvalue decomposition of the sample covariance matrix, and sort desc.
    evals, evecs = np.linalg.eig(covmat)
    idx = evals.argsort()[::-1] # descending order
    evals = evals[idx]
    evecs = evecs[:,idx]
    
    # find d, number of e-vals needed to get at least alpha accuracy
    rat = 0
    d = 0
    while rat < alpha:
        d += 1
        rat = np.sum(evals[0:d]) / np.sum(evals)
    
    return evals, evecs, rhoSampMean, d, h


def getKLEfun(evals, evecs, mean, d, hvec, rhomean = None, perts = False):
    '''
    takes eigenproblem input, returns a function handle to compute rho via KLE
    INPUTS:
        evals: array of eigenvalues
        evecs: array of eigenvectors ordered same as evals array
        mean: profile of mean density, should be SAMPLE mean
        d: number of eigen terms to include before truncation
        hvec: given altitude profile, m
    OUTPUTS:
        getKLErho: function handle to interpolate from KLE density given alt.
        Ys: values of the standard normal random variables in this instance
    '''
    
    # generate d i.i.d. standard normal variable instances
    Ys = stats.norm.rvs(size = d)
    
    # get density at each given altitude step
    rhovec = []
    for i in range(hvec.size):
        rho = mean[i] + np.sum(np.sqrt(evals[0:d]) * evecs[i, 0:d] * Ys)
        if perts:
            rho = (rho + 1) * rhomean[i]
        rhovec.append(rho)
    rhovec = np.asarray(rhovec)
    
    # ensure sorted in ascending altitude order for interpolation
    rhovec = rhovec[np.argsort(hvec)]
    hvec = hvec[np.argsort(hvec)]
    
    # now define a function to interpolate rhovec for a given h value
    def getKLErho(h):
        '''
        function for a single instance of the density KLE
        '''
        return np.interp(h, hvec, rhovec)
    
    return getKLErho, Ys


def evalEproblem(evals, evecs, hList, h, d):
    '''
    interpolates and truncates eigenvalues and eigenvectors at a given altitude
    '''
    
    evecList = np.empty(d)
    evecList[:] = np.NaN
    
    for i in range(d):
        evecList[i] = np.interp(h, hList, evecs[:,i])
    
    evalList = evals[:d]
    
    return evalList, evecList
    
























    
    