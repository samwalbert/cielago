# -*- coding: utf-8 -*-
"""
conversions.py:
    functions to perform useful conversions, rotations, etc.

Created on Tue Oct 26 11:29:53 2021

@author: Samuel Albert
"""

import numpy as np

# =============================================================================
# DCMs (nomenclature based on EDL flight mechanics notes, Sam Albert)
# =============================================================================
def getM1(theta):
    '''
    principal rotation matrix about 1-axis by angle theta (rad)
    '''
    M1 = np.array([[1, 0, 0],
                   [0, np.cos(theta), np.sin(theta)],
                   [0, -np.sin(theta), np.cos(theta)]])
    return M1

def getM2(theta):
    '''
    principal rotation matrix about 2-axis by angle theta (rad)
    '''
    M2 = np.array([[np.cos(theta), 0, -np.sin(theta)],
                   [0, 1, 0],
                   [np.sin(theta), 0, np.cos(theta)]])
    return M2

def getM3(theta):
    '''
    principal rotation matrix about 2-axis by angle theta (rad)
    '''
    M3 = np.array([[np.cos(theta), np.sin(theta), 0],
                   [-np.sin(theta), np.cos(theta), 0],
                   [0, 0, 1]])
    return M3

def getEI(lat, lon):
    '''
    defines DCM from planet-fixed frame to position frame from lat, lon (rad)
    '''
    return getM2(-lat) @ getM3(lon)

def getIE(lat, lon):
    '''
    defines DCM from position frame to planet-fixed frame from lat, lon (rad)
    '''
    return getEI(lat,lon).T

def getSE(gam, hda):
    '''
    defines DCM from position frame to velocity frame from gam, hda (rad)
    '''
    return getM2(gam) @ getM1(-hda)

def getES(gam, hda):
    '''
    defines DCM from velocity frame to position frame from gam, hda (rad)
    '''
    return getSE(gam, hda).T

def getIN(Planet, deltat):
    '''
    defines DCM from inertial frame to planet-fixed frame from omega and deltat
    deltat = (t - t0), where typically t0 = 0
    '''
    Om = Planet.om * deltat
    return getM3(Om)

def getNI(Planet, deltat):
    '''
    defines DCM from planet-fixed frame to inertial frame from omega and deltat
    deltat = (t - t0), where typically t0 = 0
    '''
    return getIN(Planet, deltat).T
    

def inertial2planetRelative(rvec, vvec, Planet):
    omvec = np.array([0, 0, Planet.om])
    uvec = vvec - np.cross(omvec, rvec)
    
    return rvec, uvec
    
def planetRelative2inertial(rvec, uvec, Planet):
    omvec = np.array([0, 0, Planet.om])
    vvec = uvec + np.cross(omvec, rvec)
    
    return rvec, vvec

def sph2cart(xxsph, Planet, deltat):
    r = xxsph[0]
    lon = xxsph[1]
    lat = xxsph[2]
    u = xxsph[3]
    gam = xxsph[4]
    hda = xxsph[5]
    
    # wrap latitude and longitude
    lon, lat = wrapLonLat(lon, lat)
    
    NI = getNI(Planet, deltat)
    IE = getIE(lat, lon)
    ES = getES(gam, hda)
    
    e1_E = np.array([1,0,0])
    # rvec_N = (r * NI @ IE).dot(e1_E)
    rvec_N = (r * NI @ IE) @ e1_E
    
    s3_S = np.array([0,0,1])
    # uvec_N = (u * NI @ IE @ ES).dot(s3_S)    
    uvec_N = u * ( NI @ IE @ ES) @ s3_S
    
    return rvec_N, uvec_N

def cart2sph(xxvec, Planet, deltat):
    rvec_N = xxvec[:3]
    uvec_N = xxvec[3:]
    
    r = np.linalg.norm(rvec_N)
    u = np.linalg.norm(uvec_N)
    
    NI = getNI(Planet, deltat)
    i1_I = np.array([1,0,0])
    i2_I = np.array([0,1,0])
    i3_I = np.array([0,0,1])
    
    # lon = np.arctan2(rvec_N.dot(NI.dot(i2_I)), rvec_N.dot(NI.dot(i1_I)))
    # lat = np.arcsin(rvec_N.dot(NI.dot(i3_I)) / r)
    
    lon = np.arctan2(np.dot(rvec_N, (NI @ i2_I)), np.dot(rvec_N, (NI @ i1_I)))
    lat = np.arcsin(np.dot(rvec_N, (NI @ i3_I)) / r)
    
    lon, lat = wrapLonLat(lon, lat)
    
    NE = NI @ getIE(lat, lon)
    e1_E = i1_I
    e2_E = i2_I
    e3_E = i3_I
    
    # gam = np.arcsin(uvec_N.dot(NE.dot(e1_E)) / u)
    # hda = np.arctan2(uvec_N.dot(NE.dot(e2_E)), uvec_N.dot(NE.dot(e3_E)))
    
    gam = np.arcsin(np.dot(uvec_N, (NE @ e1_E)) / u)
    hda = np.arctan2(np.dot(uvec_N, (NE @ e2_E)), np.dot(uvec_N, (NE @ e3_E)))
    
    xxsph = np.array([r, lon, lat, u, gam, hda])
    return xxsph

def sphPR2cartI(xxsph, Planet, deltat):
    '''
    converts directly from planet-relative spherical to inertial cartesian.
    cartesian vectors returned in inertial frame.
    '''
    
    if xxsph.size <= 6:
        xxsph = np.array([xxsph]).T
        deltat = np.array([deltat])
    
    xxvec = np.empty(xxsph.shape)
    xxvec[:] = np.NaN
    
    for i, (xxsphi, deltati) in enumerate(zip(xxsph.T, deltat)):
        rvec_N, uvec_N = sph2cart(xxsphi, Planet, deltati)
        rvec_N, vvec_N = planetRelative2inertial(rvec_N, uvec_N, Planet)
        xxvec[:,i] = np.block([rvec_N, vvec_N]).flatten()
        
    if xxvec.shape[1] == 1:
        xxvec = xxvec.flatten()
    
    return xxvec

def cartI2sphPR(xxvec, Planet, deltat):
    '''
    converts directly from inertial cartesian to planet-relative spherical.
    Cartesian vectors given in inertial frame.
    '''
    
    if xxvec.size <= 6:
        xxvec = np.array([xxvec]).T
        deltat = np.array([deltat])
    
    xxsph = np.empty(xxvec.shape)
    xxsph[:] = np.NaN
    
    for i, (xxveci, deltati) in enumerate(zip(xxvec.T, deltat)):
        rvec_N, uvec_N = inertial2planetRelative(xxveci[:3], xxveci[3:],
                                                 Planet)
        xxsph[:,i] = cart2sph(np.block([rvec_N, uvec_N]).flatten(),
                              Planet, deltati)
    
    if xxsph.shape[1] == 1:
        xxsph = xxsph.flatten()
    
    return xxsph


def wrapLonLat(lon, lat):    
    # if latitude went over a pole, flip longitude to the other side
    if abs(lat) > np.pi/2:
        lon += np.pi
        
    # put longitude in range [-180,180] deg
    lon = (lon + np.pi) % (2 * np.pi) - np.pi
    
    # put latitude in range [-90,90]
    lat = np.arctan(np.sin(lat) / abs(np.cos(lat)))
    
    return lon, lat

def wrapLonLatArray(xxsphvec):
    '''
    wrapLonlat for an entire xxsphvec time series
    '''
    
    for i, xi in enumerate(xxsphvec.T):
        lon = xi[1]
        lat = xi[2]
        lon, lat = wrapLonLat(lon, lat)
        xxsphvec[1,i] = lon
        xxsphvec[2,i] = lat
    
    return xxsphvec
        
    

def getLVLH(rvec, vvec):
    '''
    computes radial (rhat), along-track (thetahat), and cross-track (hhat)
       unit vectors given position and INERTIAL velocity vectors
    '''
    
    rhat = rvec / np.linalg.norm(rvec)
    
    hvec = np.cross(rvec, vvec)
    hhat = hvec / np.linalg.norm(hvec)
    
    thetahat = np.cross(hhat, rhat)
    thetahat = thetahat / np.linalg.norm(thetahat)
    
    return rhat, thetahat, hhat

def getApses(xxvec, Planet):
    rvec = xxvec[:3]
    vvec = xxvec[3:]
    r = np.linalg.norm(rvec)
    v = np.linalg.norm(vvec)
    
    # compute Keplerian parameters
    hvec = np.cross(rvec, vvec)
    h = np.linalg.norm(hvec)
    
    ENG = v**2/2 - Planet.mu/r
    SMA = - Planet.mu / (2 * ENG)
    arg = 1 + 2 * ENG * h**2 / Planet.mu**2
    if abs(arg) < 1e-12:
        ECC = 0 # circular orbit case
    else:
        ECC = np.sqrt(arg)
    
    # get apses radii
    ra = SMA * (1 + ECC)
    rp = SMA * (1 - ECC)
    
    return ra, rp, ENG


def getApsesSph(xxsph, Planet, t):
    '''
    computes apses radii and energy given planet-relative spherical state.
    INPUT:
        yy: spherical planet-relative state
        Planet: Planet object, used for gravitational parameter
        t: current time, needed to find inertial vectors
    OUTPUT:
        ra: radius of apoapsis, m
        rp: radius of periapsis, m
        ENG: specific mechanical energy, m^2/s^2
        Note that ra < 0 results if orbit is hyperbolic, poorly defined
    '''
    
    # get inertial cartesian state
    xxvec = sphPR2cartI(xxsph, Planet, t)
    
    return getApses(xxvec, Planet)
    
    
def greatCircle(lon1, lon2, lat1, lat2, planet):
    dlat = abs(lat2 - lat1)
    dsig = np.arccos(np.sin(lon1) * np.sin(lon2)\
                     + np.cos(lon1) * np.cos(lon2) * np.cos(dlat))
    return dsig * planet.rad # meters








