# -*- coding: utf-8 -*-
"""
sim.py:
    defines classes and functions used to simulate trajectories

Created on Mon Oct 25 17:40:29 2021

@author: Samuel Albert
"""

import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp, trapz
from dataclasses import dataclass
import numpy as np
import sys

from src import dynamics
from src import conversions
from src import planetaryConstants as constants

# =============================================================================
# Classes
# =============================================================================
@dataclass
class Vehicle:
    '''Class for containing constant vehicle parameters'''
    name: str
    
@dataclass
class Params:
    '''Class for simulation settings and parameters'''
    pass

@dataclass
class Traj:
    '''Class for storing info about a trajectory'''
    pass

@dataclass
class Result:
    '''Class for storing sim results and input dispersions'''
    N: int
    
    def __post_init__(self):
        self.y0vecs = [np.NaN] * self.N
        # self.hLists = [np.NaN] * self.N
        # self.rhoLists = [np.NaN] * self.N
        self.xxfsphs = [np.NaN] * self.N
        self.tfs = np.empty(self.N)
        self.tfs[:] = np.NaN
        self.ras = np.empty(self.N)
        self.ras[:] = np.NaN
        self.rps = np.empty(self.N)
        self.rps[:] = np.NaN
        self.engfs = np.empty(self.N)
        self.engfs[:] = np.NaN
        self.atmfilenames = [np.NaN] * self.N
        self.xxsphs = [np.NaN] * self.N
        self.ts = [np.NaN] * self.N
        self.sigvecs = [np.NaN] * self.N
        self.TPCstatuses = [np.NaN] * self.N
        self.gloads = [np.NaN] * self.N
        self.rhos = [np.NaN] * self.N

# =============================================================================
# Event Functions
# =============================================================================
def belowMinAltSph(t, y, Params):
    return y[0] - Params.rmin

def aboveMaxAltSph(t, y, Params):
    return y[0] - Params.rmax

def belowMinAltCart(t, y, Params):
    r = np.linalg.norm(y[:3])
    return r - Params.rmin

def aboveMaxAltCart(t, y, Params):
    r = np.linalg.norm(y[:3])
    return r - Params.rmax



# =============================================================================
# Functions
# =============================================================================
def runSpherical(Planet, Vehicle, Params, t_eval = None):
    lon, lat = conversions.wrapLonLat(Vehicle.y0[1], Vehicle.y0[2])
    Vehicle.y0[1] = lon
    Vehicle.y0[2] = lat
    sol = solve_ivp(lambda t, y: dynamics.spherical(t, y, Planet, Vehicle),
                    (Params.tspan[0], Params.tspan[-1]), Vehicle.y0,
                    rtol = Params.rtol, atol = Params.atol,
                    events = Params.events, t_eval = t_eval)
    
    if not sol.success:
        print(sol.message)
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(sol.y[3,:]/1e3, (sol.y[0,:] - Planet.rad)/1e3)
        sys.exit('integration failed')
    if (not sol.status) and (len(Params.events) > 0):
        print('WARNING: no termination event reached:')
        print(sol.message)
    
    return sol

def runCartesian(Planet, Vehicle, Params, t_eval = None, warn = True):
    sol = solve_ivp(lambda t, y: dynamics.cartesian(t, y, Planet, Vehicle),
                    (Params.tspan[0], Params.tspan[-1]), Vehicle.y0,
                    rtol = Params.rtol, atol = Params.atol,
                    events = Params.events, t_eval = t_eval)
    
    if not sol.success:
        print(sol.message)
        sys.exit('integration failed')
    if (not sol.status) and (len(Params.events) > 0) and warn:
        print('WARNING: no termination event reached:')
        print(sol.message)
    
    return sol

def buildTrajCart(sol, Planet, traj = None):
    if traj is None:
        traj = Traj()
    traj.t = sol.t
    traj.x = sol.y[0,:]
    traj.y = sol.y[1,:]
    traj.z = sol.y[2,:]
    traj.vx = sol.y[3,:]
    traj.vy = sol.y[4,:]
    traj.vz = sol.y[5,:]
    traj.xxvec = sol.y
    
    xxsph = conversions.cartI2sphPR(sol.y, Planet, sol.t)
    traj.r = xxsph[0,:]
    traj.lon = xxsph[1,:]
    traj.lat = xxsph[2,:]
    traj.v = xxsph[3,:]
    traj.gam = xxsph[4,:]
    traj.hda = xxsph[5,:]
    traj.xxsph = np.block([[traj.r], [traj.lon], [traj.lat],
                           [traj.v], [traj.gam], [traj.hda]])
    
    return traj

def buildTrajSph(sol, traj = None):
    if traj is None:
        traj = Traj()
    traj.t = sol.t
    traj.r = sol.y[0,:]
    traj.lon = sol.y[1,:]
    traj.lat = sol.y[2,:]
    traj.v = sol.y[3,:]
    traj.gam = sol.y[4,:]
    traj.hda = sol.y[5,:]
    
    # TODO: convert to cartesian and add those as well
    
    return traj
    

def analyzeTraj(Planet, Vehicle, Params, Traj):
    # loop through trajectory, generating g-loads, heat flux, etc. at each time
    N = Traj.t.shape[0]
    Traj.h = np.empty(N)
    Traj.gload = np.empty(N)
    Traj.rho = np.empty(N)
    Traj.dynp = np.empty(N)
    Traj.qflux = np.empty(N)
    Traj.ra = np.empty(N)
    Traj.rp = np.empty(N)
    Traj.ENG = np.empty(N)
    Traj.Mach = np.empty(N)
    Traj.BC = np.empty(N)
    Traj.LD = np.empty(N)
    for i in range(N):
        # get altitude
        Traj.h[i] = Traj.r[i] - Planet.rad
        
        # get lift and drag magnitudes from dynamics
        _, L, D, _, _ = dynamics.spherical(Traj.t[i], Traj.xxsph[:,i],
                                            Planet, Vehicle, True)
        
        # compute sensed deceleration from aerodynamics in Earth g's
        Traj.gload[i] = np.sqrt(L**2 + D**2) / constants.G0
        
        # get current density
        Traj.rho[i] = Planet.rhoFun(Traj.r[i])
        
        # get current Mach number
        Traj.Mach[i] = Planet.MachFun(Traj.r[i], Traj.v[i])
        Traj.BC[i], Traj.LD[i] = Vehicle.getAero(Traj.r[i], Traj.v[i], Planet)
        
        # dynamic pressure in Pa
        Traj.dynp[i] = 1/2 * Traj.rho[i] * Traj.v[i]**2
        
        # heat flux in W/m^2
        Traj.qflux[i] = Planet.k * np.sqrt(Traj.rho[i] / Vehicle.Rn)\
            * Traj.v[i]**3
        
        # Keplerian properties
        Traj.ra[i], Traj.rp[i], Traj.ENG[i] = \
            conversions.getApsesSph(Traj.xxsph[:,i], Planet, Traj.t[i])
    
    # total heat load
    Traj.Qtotal = trapz(Traj.qflux, Traj.t) # J/m^2
    
    # extremum values
    Traj.peakg = np.max(Traj.gload)
    Traj.peakq = np.max(Traj.qflux)
    Traj.peakdynp = np.max(Traj.dynp)
    Traj.minalt = np.min(Traj.h)
        
    return Traj

def plotTraj(Traj):
    '''
    create generic plots for a trajectory that has already been analyzed
    '''
    handles = []
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(Traj.v/1e3, Traj.h/1e3)
    ax.grid()
    ax.set_xlabel('velocity, km/s')
    ax.set_ylabel('altitude, km')
    plt.tight_layout()
    handles.append((fig, ax))
    
    fig, ax = plt.subplots(3,1,sharex = True)
    ax[0].plot(Traj.t, Traj.h/1e3)
    ax[0].set_ylabel('altitude, km', color = 'C0')
    ax[0].grid()
    ax[0].tick_params('x', labelbottom=False)
    ax[0].tick_params(axis = 'y', labelcolor = 'C0')
    
    ax02 = ax[0].twinx()
    ax02.plot(Traj.t, Traj.v/1e3, 'C1--')
    ax02.set_ylabel('velocity, km/s', color = 'C1')
    ax02.tick_params(axis = 'y', labelcolor = 'C1')
    
    ax[1].plot(Traj.t, Traj.gload)
    ax[1].set_ylabel("g's", color = 'C0')
    ax[1].grid()
    ax[1].tick_params('x', labelbottom=False)
    ax[1].tick_params(axis = 'y', labelcolor = 'C0')
    
    ax12 = ax[1].twinx()
    ax12.plot(Traj.t, Traj.dynp/1e3, 'C1--')
    ax12.set_ylabel('dynamic pressure, kPa', color = 'C1')
    ax12.tick_params(axis = 'y', labelcolor = 'C1')
    
    ax[2].plot(Traj.t, Traj.qflux/1e4)
    ax[2].set_ylabel('heat flux, W/cm^2')
    ax[2].grid()
    ax[2].set_xlabel('time, s')
    
    plt.tight_layout()
    handles.append((fig, ax))
    
    fig, ax = plt.subplots(2,1,sharex = True)
    ax[0].plot(Traj.t, Traj.Mach)
    ax[0].set_ylabel('Mach number')
    ax[0].grid()
    ax[0].tick_params('x', labelbottom=False)
    
    ax[1].plot(Traj.t, Traj.BC)
    ax[1].set_ylabel(r'$\beta,\ kg/m^2$')
    ax[1].grid()
    # ax[1].tick_params('x', labelbottom=False)
    
    # ax[2].plot(Traj.t, Traj.LD)
    # ax[2].set_ylabel('L/D')
    # ax[2].grid()
    ax[1].set_xlabel('time, s')
    
    plt.tight_layout()
    handles.append((fig, ax))
    
    return handles
    
    
        
        