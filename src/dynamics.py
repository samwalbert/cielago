# -*- coding: utf-8 -*-
"""
dynamics.py:
    contains EOMs and associated functions for EDL dynamics

Created on Mon Oct 25 17:16:19 2021

@author: Samuel Albert
"""

from src import conversions

import numpy as np

def cartesian(t, yy, Planet, Vehicle, returnAccels = False):
    '''
    Simple entry EOMs for 3DOF, rotating planet, J2 dynamics.
    Note: requires that Vehicle.t0 be defined (usually = 0)
    '''
    # extract constants
    mu = Planet.mu
    R = Planet.rad
    J2 = Planet.J2
    
    # BC = Vehicle.BC
    # LD = Vehicle.LD
    
    # extract state variables
    rvec = yy[:3]
    vvec = yy[3:]
    
    r = np.linalg.norm(rvec)
    
    # get planet-relative velocity
    _, uvec = conversions.inertial2planetRelative(rvec, vvec, Planet)
    u = np.linalg.norm(uvec)
        
    # get E frame unit vectors
    i3 = np.array([0,0,1]) # true in both inertial and planet-relative coord.s
    e1 = rvec / r
    e2 = np.cross(i3, e1)
    e2 = e2 / np.linalg.norm(e2)
    
    # get S frame unit vectors
    s3 = uvec / u
    s2 = np.cross(s3, e1)
    s2 = s2 / np.linalg.norm(s2)
    s1 = np.cross(s2, s3)
    
    # gravity acceleration
    aGrav = -mu/r**2 * (1 +\
                        3 * J2 * R**2 / (2 * r**2) * (1 -\
                                                      5 * (e1.dot(i3)**2)))\
            * e1 - mu/r**2 * (3 * J2 * R**2 / (2 * r**2) * 2 * e1.dot(i3)) * i3
    
    if r < Planet.rlim:
        # get density at this altitude
        rho = Planet.rhoFun(r)
        
        # get aerodynamic properties
        BC, LD = Vehicle.getAero(r, u, Planet)
        
        # compute lift and drag accelerations
        D = rho * u**2 / (2 * BC)
        L = D * LD
        
        # get bank angle at current state
        bank = Vehicle.bankFun(t, yy)
        
        # drag acceleration
        aDrag = -D * s3
        
        # lift acceleration
        aLift = L * np.cos(bank) * s1 + L * np.sin(bank) * s2
        
        # sum accelerations
        aTotal = aGrav + aDrag + aLift
    else:
        aTotal = aGrav
    
    # EOMs
    dydt = np.empty(6)
    dydt[:] = np.NaN
    
    dydt[:3] = vvec
    dydt[3:] = aTotal
    
    if returnAccels:
        return dydt, aLift, aDrag, aGrav
    else:
        return dydt    

def spherical(t, yy, Planet, Vehicle, returnAccels = False):
    '''
    Simple entry EOMs for 3DOF, rotating planet, J2 dynamics, dimensional vars.
    '''
    # extract constants
    mu = Planet.mu
    R = Planet.rad
    J2 = Planet.J2
    omp = Planet.om
    
    # BC = Vehicle.BC
    # LD = Vehicle.LD
    
    # extract state variables
    r = yy[0]
    lat = yy[2]
    u = yy[3]
    gam = yy[4]
    hda = yy[5]
    
    # get gravity from J2 model
    gr = mu / r**2 *\
        (1 + 3 * J2 * R**2 / (2 * r**2) * (1 - 3 * np.sin(lat)**2))
    gphi = mu / r**2 *\
        (3 * J2 * R**2 / (2 * r**2) * 2 * np.sin(lat) * np.cos(lat))
    
    # get density at this altitude
    rho = Planet.rhoFun(r)
    
    # get aerodynamic properties
    BC, LD = Vehicle.getAero(r, u, Planet)
    
    # compute lift and drag accelerations
    D = rho * u**2 / (2 * BC)
    L = D * LD
    
    # get bank angle at current state
    bank = Vehicle.bankFun(t, yy)
    
    # EOMs
    dydt = np.empty(6)
    dydt[:] = np.NaN
    
    dydt[0] = u * np.sin(gam)
    dydt[1] = u * np.cos(gam) * np.sin(hda) / (r * np.cos(lat))
    dydt[2] = u * np.cos(gam) * np.cos(hda) / r
    
    dydt[3] = -D - gr * np.sin(gam) - gphi * np.cos(gam) * np.cos(hda)\
        + omp**2 * r * np.cos(lat) *\
            (np.cos(lat) * np.sin(gam)\
             - np.sin(lat) * np.cos(gam) * np.cos(hda))
    
    dydt[4] = 1/u * (L * np.cos(bank) + np.cos(gam) * (u**2 / r - gr)\
                     + gphi * np.sin(gam) * np.cos(hda)\
                     + 2 * omp * u * np.cos(lat) * np.sin(hda)\
                     + omp**2 * r * np.cos(lat) *\
                         (np.cos(lat) * np.cos(gam)\
                         + np.sin(lat) * np.sin(gam) * np.cos(hda)))
    
    dydt[5] = 1/u * (L * np.sin(bank) / np.cos(gam)\
                     + u**2 / r * np.tan(lat) * np.cos(gam) * np.sin(hda)\
                     + gphi * np.sin(hda) / np.cos(gam)\
                     - 2 * omp * u *\
                         (np.cos(lat) * np.tan(gam) * np.cos(hda)\
                          - np.sin(lat))\
                     + omp**2 * r / np.cos(gam) *\
                         np.cos(lat) * np.sin(lat) * np.sin(hda))
    
    if returnAccels:
        return dydt, L, D, gr, gphi
    else:
        return dydt 
    
    
    
    
    
    