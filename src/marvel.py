# -*- coding: utf-8 -*-
"""
marvel.py:
    functions related to B-plane targeting and deployment of a surface network
Created on Sat Nov 27 20:54:49 2021

@author: Samuel
"""

import sys
import copy
import numpy as np
from scipy.integrate import trapz

from src import sim, conversions, dynamics
from src import planetaryConstants as constants

def runSHIELDcartesian(planet, vehicle, Mdeploy, Mjettison, tspan = None):
    '''
    runs Cartesian sim for SHIELD vehicle in 3 parts:
        - entry config until M <= Mdeploy
        - descent config until M <= Mjettison
        - landing config until landing
    NOTE, may run into issues if SHIELD does not impact the surface
    '''
    # create generic params and events
    params = sim.Params()
    params.rtol = 1e-11
    params.atol = 1e-11
    params.rmin = planet.rad
    params.rmax = (planet.rad + planet.halt)
    params.trigger = 'Mach'

    # initial and final time
    if tspan is None:
        params.t0 = 0 # s
        params.tf = params.t0 + 5e3 # s
    else:
        params.t0 = tspan[0]
        params.tf = tspan[1]
    params.tspan = (params.t0, params.tf)

    params.event1 = lambda t, y: sim.belowMinAltCart(t, y, params)
    params.event1.terminal = True

    params.event2 = lambda t, y: sim.aboveMaxAltCart(t, y, params)
    # params.event2.terminal = True
    params.event2.terminal = False
    
    # create deploy and jettison events
    def MachEvent(t, y, planet, Mevent):
        xxsph = conversions.cartI2sphPR(y, planet, t)
        r = xxsph[0]
        u = xxsph[3]
        Mach = planet.MachFun(r, u)
        return Mach - Mevent
    
    params.event3 = lambda t, y: MachEvent(t, y, planet, Mdeploy)
    params.event3.terminal = True
    params.event3.direction = -1
    
    params.event4 = lambda t, y: MachEvent(t, y, planet, Mjettison)
    params.event4.terminal = True
    params.event4.direction = -1
    
    # ENTRY
    params.events = (params.event1, params.event2, params.event3)
    vehicle.getAero = getSHIELDAeroFun(vehicle, 'entry')
    sol1 = sim.runCartesian(planet, vehicle, params)
    
    if sol1.t_events[2].any():
        # DESCENT
        params.tdeployed = sol1.t[-1]
        params.tspan = (params.tdeployed, params.tf)
        vehicle.y0 = sol1.y[:,-1]
        
        params.events = (params.event1, params.event2, params.event4)
        vehicle.getAero = getSHIELDAeroFun(vehicle, 'descent')
        sol2 = sim.runCartesian(planet, vehicle, params)
        
        if sol2.t_events[2].any():
            # LANDING
            params.tjettisoned = sol2.t[-1]
            params.tspan = (params.tjettisoned, params.tf)
            vehicle.y0 = sol2.y[:,-1]
            
            params.events = (params.event1, params.event2)
            vehicle.getAero = getSHIELDAeroFun(vehicle, 'landing')
            sol3 = sim.runCartesian(planet, vehicle, params)
            
            # concatenate
            sol = copy.deepcopy(sol1)
            sol.t = np.block([sol1.t, sol2.t, sol3.t])
            sol.y = np.block([sol1.y, sol2.y, sol3.y])
            
        else:
            params.tjettisoned = np.NaN
            # concatenate
            sol = copy.deepcopy(sol1)
            sol.t = np.block([sol1.t, sol2.t])
            sol.y = np.block([sol1.y, sol2.y])
        
    else:
        params.tdeployed = np.NaN
        params.tjettisoned = np.NaN
        sol = sol1

    return sol, params

def runSHIELDtimed(planet, vehicle, tdeploy, tjettison, gThresh, tspan = None):
    '''
    NOTE: tdeploy and tjettison are time FROM ENTRY (based on gThresh), not from t0
    runs Cartesian sim for SHIELD vehicle in 3 parts:
        - entry config until t+ <= tdeploy
        - descent config until t+ <= tjettison
        - landing config until landing
    '''
    # create generic params and events
    params = sim.Params()
    params.rtol = 1e-11
    params.atol = 1e-11
    params.rmin = planet.rad
    params.rmax = (planet.rad + planet.halt)
    params.gThreshold = gThresh
    params.tdeploy = tdeploy
    params.tjettison = tjettison
    params.trigger = 'timer'

    # initial and final time
    if tspan is None:
        params.t0 = 0 # s
        params.tf = params.t0 + 5e3 # s
    else:
        params.t0 = tspan[0]
        params.tf = tspan[1]

    params.event1 = lambda t, y: sim.belowMinAltCart(t, y, params)
    params.event1.terminal = True
    
    def sensedGs(t, y, planet, vehicle, params):
        xxsph = conversions.cartI2sphPR(y, planet, t)
        _, L, D, _, _ = dynamics.spherical(t, xxsph, planet, vehicle, True)
        gload = np.sqrt(L**2 + D**2) / constants.G0
        return gload - params.gThreshold
    
    params.event2 = lambda t, y: sensedGs(t, y, planet, vehicle, params)
    params.event2.terminal = True
    params.event2.direction = 1
    
    # EXOATMOSPHERIC
    params.tspan = (params.t0, params.tf)
    params.events = (params.event1, params.event2)
    vehicle.getAero = getSHIELDAeroFun(vehicle, 'entry')
    sol0 = sim.runCartesian(planet, vehicle, params)
    
    if sol0.t_events[1].any():
        # ENTRY
        params.tentry = sol0.t[-1]
        params.tspan = (params.tentry, params.tentry + tdeploy)
        vehicle.y0 = sol0.y[:,-1]
        
        params.events = [params.event1]
        vehicle.getAero = getSHIELDAeroFun(vehicle, 'entry')
        sol1 = sim.runCartesian(planet, vehicle, params, warn = False)
        
        if sol1.status == 0:
            # DESCENT
            params.tdeployed = sol1.t[-1]
            params.tspan = (params.tdeployed, params.tentry + tjettison)
            vehicle.y0 = sol1.y[:,-1]
            
            params.events = [params.event1]
            vehicle.getAero = getSHIELDAeroFun(vehicle, 'descent')
            sol2 = sim.runCartesian(planet, vehicle, params, warn = False)
            
            if sol2.status == 0:
                # LANDING
                params.tjettisoned = sol2.t[-1]
                params.tspan = (params.tjettisoned, params.tf)
                vehicle.y0 = sol2.y[:,-1]
                
                params.events = [params.event1]
                vehicle.getAero = getSHIELDAeroFun(vehicle, 'landing')
                sol3 = sim.runCartesian(planet, vehicle, params)
                
                # concatenate
                sol = copy.deepcopy(sol0)
                sol.t = np.block([sol0.t, sol1.t, sol2.t, sol3.t])
                sol.y = np.block([sol0.y, sol1.y, sol2.y, sol3.y])
                
            else:
                params.tjettisoned = np.NaN
                # concatenate
                sol = copy.deepcopy(sol0)
                sol.t = np.block([sol0.t, sol1.t, sol2.t])
                sol.y = np.block([sol0.y, sol1.y, sol2.y])
            
        else:
            params.tdeployed = np.NaN
            params.tjettisoned = np.NaN
            # contcatenate
            sol = copy.deepcopy(sol0)
            sol.t = np.block([sol0.t, sol1.t])
            sol.y = np.block([sol0.y, sol1.y])
        
    else:
        params.tentry = np.NaN
        params.tdeployed = np.NaN
        params.tjettisoned = np.NaN
        sol = sol0

    return sol, params

def analyzeSHIELDTraj(Planet, Vehicle, Params, Traj):
    '''
    version of sim.analyzeTraj() specific to varying-config SHIELD
    '''
    
    # loop through trajectory, generating g-loads, heat flux, etc. at each time
    N = Traj.t.shape[0]
    Traj.h = np.empty(N)
    Traj.gload = np.empty(N)
    Traj.rho = np.empty(N)
    Traj.dynp = np.empty(N)
    Traj.qflux = np.empty(N)
    Traj.ra = np.empty(N)
    Traj.rp = np.empty(N)
    Traj.ENG = np.empty(N)
    Traj.Mach = np.empty(N)
    Traj.BC = np.empty(N)
    Traj.LD = np.empty(N)
    Traj.Mdeploy = np.NaN
    Traj.Mjettison = np.NaN
    Traj.tentry = np.NaN
    for i in range(N):
        # get altitude
        Traj.h[i] = Traj.r[i] - Planet.rad
        
        # get lift and drag magnitudes from dynamics
        _, L, D, _, _ = dynamics.spherical(Traj.t[i], Traj.xxsph[:,i],
                                            Planet, Vehicle, True)
        
        # compute sensed deceleration from aerodynamics in Earth g's
        Traj.gload[i] = np.sqrt(L**2 + D**2) / constants.G0
        
        # set time of entry if not already set
        if (np.isnan(Traj.tentry) and (Params.trigger == 'Mach') and (Traj.gload[i] > 1)):
            Traj.entry = Traj.t[i]
        
        # get current density
        Traj.rho[i] = Planet.rhoFun(Traj.r[i])
        
        # get current Mach number
        Traj.Mach[i] = Planet.MachFun(Traj.r[i], Traj.v[i])
        
        # get current config aerodynamics
        if Traj.t[i] > Params.tjettisoned:
            config = 'landing'
            if np.isnan(Traj.Mjettison):
                Traj.Mjettison = Traj.Mach[i]
        elif Traj.t[i] > Params.tdeployed:
            config = 'descent'
            if np.isnan(Traj.Mdeploy):
                Traj.Mdeploy = Traj.Mach[i]
        else:
            config = 'entry'
        
        getAero = getSHIELDAeroFun(Vehicle, config)
        Traj.BC[i], Traj.LD[i] = getAero(Traj.r[i], Traj.v[i], Planet)
        
        # dynamic pressure in Pa
        Traj.dynp[i] = 1/2 * Traj.rho[i] * Traj.v[i]**2
        
        # heat flux in W/m^2
        Traj.qflux[i] = Planet.k * np.sqrt(Traj.rho[i] / Vehicle.Rn)\
            * Traj.v[i]**3
        
        # Keplerian properties
        Traj.ra[i], Traj.rp[i], Traj.ENG[i] = \
            conversions.getApsesSph(Traj.xxsph[:,i], Planet, Traj.t[i])
    
    # total heat load
    Traj.Qtotal = trapz(Traj.qflux, Traj.t) # J/m^2
    
    # event times
    Traj.tjettisoned = Params.tjettisoned
    Traj.tdeployed = Params.tdeployed
    if Params.trigger == 'timer':
        Traj.tentry = Params.tentry
        Traj.tjettison = Params.tjettison
        Traj.tdeploy = Params.tdeploy
    
    # extremum values
    Traj.peakg = np.max(Traj.gload)
    Traj.peakq = np.max(Traj.qflux)
    Traj.peakdynp = np.max(Traj.dynp)
    Traj.minalt = np.min(Traj.h)
        
    return Traj
    

def loadSHIELDAeroData(shield):
    shield.aero_entry = np.loadtxt('../data/SHIELD_aero_entry.txt',
                                   delimiter = ',', skiprows = 1)
    # sort by ascending Mach
    shield.aero_entry = shield.aero_entry[shield.aero_entry[:,0].argsort()]

    shield.aero_descent = np.loadtxt('../data/SHIELD_aero_descent.txt',
                                     delimiter = ',', skiprows = 1)
    shield.aero_descent = shield.aero_descent[shield.aero_descent[:,0].argsort()]

    shield.aero_landing = np.loadtxt('../data/SHIELD_aero_landing.txt',
                                     delimiter = ',', skiprows = 1)
    shield.aero_landing = shield.aero_landing[shield.aero_landing[:,0].argsort()]
    

def getSHIELDAeroFun(vehicle, config):
    if config.lower() == 'entry':
        Mtable = vehicle.aero_entry[:,0]
        A = vehicle.aero_entry[0,1]
        m = vehicle.aero_entry[0,2]
        CDtable = vehicle.aero_entry[:,3]
        CLtable = vehicle.aero_entry[:,4]
    elif config.lower() == 'descent':
        Mtable = vehicle.aero_descent[:,0]
        A = vehicle.aero_descent[0,1]
        m = vehicle.aero_descent[0,2]
        CDtable = vehicle.aero_descent[:,3]
        CLtable = vehicle.aero_descent[:,4]
    elif config.lower() == 'landing':
        Mtable = vehicle.aero_landing[:,0]
        A = vehicle.aero_landing[0,1]
        m = vehicle.aero_landing[0,2]
        CDtable = vehicle.aero_landing[:,3]
        CLtable = vehicle.aero_landing[:,4]
    else:
        sys.exit('Error: SHIELD config not recognized')
        
    def getAero(r, u, planet):
        Mach = planet.MachFun(r, u)
        CD = np.interp(Mach, Mtable, CDtable)
        CL = np.interp(Mach, Mtable, CLtable)
        BC = m / (CD * A)
        LD = CL / CD
        if vehicle.scaleAero:
            return BC * vehicle.BCscale, LD * vehicle.LDscale
        else:
            return BC, LD
    
    return getAero
        

def sep2surface(planet, vehicle, params, dlon, dlat):
    vehicle = copy.deepcopy(vehicle)
    xx0vecCenter, xxfsphCenter = getCenterState(planet, vehicle, params)
    J = getJ(planet, vehicle, params, xx0vecCenter, xxfsphCenter)
    DVvec, (lonf, latf), (lonErr, latErr) = targetLonLat(planet, vehicle,
                                                         params, xx0vecCenter,
                                                         xxfsphCenter, J, dlon,
                                                         dlat)
    return DVvec, (lonf, latf), (lonErr, latErr)

def sep2surfaceTimed(planet, vehicle, params, dlon, dlat):
    vehicle = copy.deepcopy(vehicle)
    xx0vecCenter, xxfsphCenter = getCenterStateTimed(planet, vehicle, params)
    J = getJTimed(planet, vehicle, params, xx0vecCenter, xxfsphCenter)
    DVvec, (lonf, latf), (lonErr, latErr) = targetLonLatTimed(planet, vehicle,
                                                         params, xx0vecCenter,
                                                         xxfsphCenter, J, dlon,
                                                         dlat)
    return DVvec, (lonf, latf), (lonErr, latErr)
    

def getCenterState(planet, vehicle, params):
    # =========================================================================
    # Back-propagate by tback seconds
    # =========================================================================
    params.tspan = (params.t0, (params.t0 - params.tback))
    params.events = ()
    
    # print('\nBack-propagating center trajectory...')
    solBack = sim.runCartesian(planet, vehicle, params)
    xx0vecCenter = solBack.y[:,-1]
    
    # =========================================================================
    # Forward-propagate to get lat, lon of center trajectory
    # =========================================================================
    vehicle.y0 = xx0vecCenter
    params.tspan = ((params.t0 - params.tback), params.tf)
    params.events = (params.event1, params.event2)
    solCenter = sim.runCartesian(planet, vehicle, params)
    
    # convert to spherical along trajectory
    xxfsphCenter = conversions.cartI2sphPR(solCenter.y[:,-1], planet,
                                          solCenter.t[-1])
    
    return xx0vecCenter, xxfsphCenter

def getCenterStateTimed(planet, vehicle, params):
    # =========================================================================
    # Back-propagate by tback seconds
    # =========================================================================
    vehicle.getAero = getSHIELDAeroFun(vehicle, 'entry')
    params.tspan = (params.t0, (params.t0 - params.tback))
    params.events = ()
    
    # print('\nBack-propagating center trajectory...')
    solBack = sim.runCartesian(planet, vehicle, params)
    xx0vecCenter = solBack.y[:,-1]
    
    # =========================================================================
    # Forward-propagate to get lat, lon of center trajectory
    # =========================================================================
    vehicle.y0 = xx0vecCenter
    params.tspan = ((params.t0 - params.tback), params.tf)
    solCenter, _ = runSHIELDtimed(planet, vehicle, params.tdeploy,
                                  params.tjettison, params.gThresh,
                                  params.tspan)
    
    # convert to spherical along trajectory
    xxfsphCenter = conversions.cartI2sphPR(solCenter.y[:,-1], planet,
                                          solCenter.t[-1])
    
    return xx0vecCenter, xxfsphCenter

def getJ(planet, vehicle, params, xx0vecCenter, xxfsphCenter):
    pert = params.pert
    xdir = np.array([1,0,0])
    ydir = np.array([0,1,0])
    zdir = np.array([0,0,1])
    
    x0vecCenter = xx0vecCenter[:3]
    v0vecCenter = xx0vecCenter[3:]
    
    lonfCenter = xxfsphCenter[1]
    latfCenter = xxfsphCenter[2]
    
    # perturb x-direction
    v0vecXpert = v0vecCenter + pert * xdir
    xx0vecXpert = np.block([x0vecCenter, v0vecXpert]).flatten()
    
    vehicle.y0 = xx0vecXpert
    solXpert = sim.runCartesian(planet, vehicle, params)
    xxsphXpert = conversions.cartI2sphPR(solXpert.y, planet, solXpert.t)
    lonfXpert = xxsphXpert[1,-1]
    latfXpert = xxsphXpert[2,-1]
    
    dlondX = (lonfXpert - lonfCenter) / pert
    dlatdX = (latfXpert - latfCenter) / pert
    
    # perturb y-direction
    v0vecYpert = v0vecCenter + pert * ydir
    xx0vecYpert = np.block([x0vecCenter, v0vecYpert]).flatten()
    
    vehicle.y0 = xx0vecYpert
    solYpert = sim.runCartesian(planet, vehicle, params)
    xxsphYpert = conversions.cartI2sphPR(solYpert.y, planet, solYpert.t)
    lonfYpert = xxsphYpert[1,-1]
    latfYpert = xxsphYpert[2,-1]
    
    dlondY = (lonfYpert - lonfCenter) / pert
    dlatdY = (latfYpert - latfCenter) / pert
    
    # perturb z-direction
    v0vecZpert = v0vecCenter + pert * zdir
    xx0vecZpert = np.block([x0vecCenter, v0vecZpert]).flatten()
    
    vehicle.y0 = xx0vecZpert
    solZpert = sim.runCartesian(planet, vehicle, params)
    xxsphZpert = conversions.cartI2sphPR(solZpert.y, planet, solZpert.t)
    lonfZpert = xxsphZpert[1,-1]
    latfZpert = xxsphZpert[2,-1]
    
    dlondZ = (lonfZpert - lonfCenter) / pert
    dlatdZ = (latfZpert - latfCenter) / pert    
    
    J = np.array([[dlondX, dlondY, dlondZ],
                  [dlatdX, dlatdY, dlatdZ]])
    
    return J

def getJTimed(planet, vehicle, params, xx0vecCenter, xxfsphCenter):
    pert = params.pert
    xdir = np.array([1,0,0])
    ydir = np.array([0,1,0])
    zdir = np.array([0,0,1])
    
    x0vecCenter = xx0vecCenter[:3]
    v0vecCenter = xx0vecCenter[3:]
    
    lonfCenter = xxfsphCenter[1]
    latfCenter = xxfsphCenter[2]
    
    # perturb x-direction
    v0vecXpert = v0vecCenter + pert * xdir
    xx0vecXpert = np.block([x0vecCenter, v0vecXpert]).flatten()
    
    vehicle.y0 = xx0vecXpert
    solXpert, _ = runSHIELDtimed(planet, vehicle, params.tdeploy,
                                 params.tjettison, params.gThresh, params.tspan)
    xxsphXpert = conversions.cartI2sphPR(solXpert.y[:,-1], planet, solXpert.t[-1])
    lonfXpert = xxsphXpert[1]
    latfXpert = xxsphXpert[2]
    
    dlondX = (lonfXpert - lonfCenter) / pert
    dlatdX = (latfXpert - latfCenter) / pert
    
    # perturb y-direction
    v0vecYpert = v0vecCenter + pert * ydir
    xx0vecYpert = np.block([x0vecCenter, v0vecYpert]).flatten()
    
    vehicle.y0 = xx0vecYpert
    solYpert, _ = runSHIELDtimed(planet, vehicle, params.tdeploy,
                                 params.tjettison, params.gThresh, params.tspan)
    xxsphYpert = conversions.cartI2sphPR(solYpert.y[:,-1], planet, solYpert.t[-1])
    lonfYpert = xxsphYpert[1]
    latfYpert = xxsphYpert[2]
    
    dlondY = (lonfYpert - lonfCenter) / pert
    dlatdY = (latfYpert - latfCenter) / pert
    
    # perturb z-direction
    v0vecZpert = v0vecCenter + pert * zdir
    xx0vecZpert = np.block([x0vecCenter, v0vecZpert]).flatten()
    
    vehicle.y0 = xx0vecZpert
    solZpert, _ = runSHIELDtimed(planet, vehicle, params.tdeploy,
                                 params.tjettison, params.gThresh, params.tspan)
    xxsphZpert = conversions.cartI2sphPR(solZpert.y[:,-1], planet, solZpert.t[-1])
    lonfZpert = xxsphZpert[1]
    latfZpert = xxsphZpert[2]
    
    dlondZ = (lonfZpert - lonfCenter) / pert
    dlatdZ = (latfZpert - latfCenter) / pert    
    
    J = np.array([[dlondX, dlondY, dlondZ],
                  [dlatdX, dlatdY, dlatdZ]])
    
    return J


def targetLonLat(planet, vehicle, params, xx0vecCenter, xxfsphCenter, J,
                 dlon, dlat):
    x0vecCenter = xx0vecCenter[:3]
    v0vecCenter = xx0vecCenter[3:]
    
    lonfCenter = xxfsphCenter[1]
    latfCenter = xxfsphCenter[2]
    
    # compute Delta-V
    deltaSurface = np.array([dlon, dlat])
    DVvec = J.T @ np.linalg.inv(J @ J.T) @ deltaSurface
    
    # propagate
    v0vec = v0vecCenter + DVvec
    xx0vec = np.block([x0vecCenter, v0vec]).flatten()
    vehicle.y0 = xx0vec
    sol = sim.runCartesian(planet, vehicle, params)
    xxfsph = conversions.cartI2sphPR(sol.y[:,-1], planet, sol.t[-1])
    lonf = xxfsph[1]
    latf = xxfsph[2]
    lonErr = abs((lonf - lonfCenter) - deltaSurface[0])
    latErr = abs((latf - latfCenter) - deltaSurface[1])
    
    if xxfsph[0] > params.rmin + 1e3: # if final altitude not at surface
        print('WARNING: probe missed the planet!')
        return np.NaN, (np.NaN, np.NaN), (np.NaN, np.NaN)
    
    return DVvec, (lonf, latf), (lonErr, latErr)

def targetLonLatTimed(planet, vehicle, params, xx0vecCenter, xxfsphCenter, J,
                 dlon, dlat):
    x0vecCenter = xx0vecCenter[:3]
    v0vecCenter = xx0vecCenter[3:]
    
    lonfCenter = xxfsphCenter[1]
    latfCenter = xxfsphCenter[2]
    
    # compute Delta-V
    deltaSurface = np.array([dlon, dlat])
    DVvec = J.T @ np.linalg.inv(J @ J.T) @ deltaSurface
    
    # propagate
    v0vec = v0vecCenter + DVvec
    xx0vec = np.block([x0vecCenter, v0vec]).flatten()
    vehicle.y0 = xx0vec
    sol, _ = runSHIELDtimed(planet, vehicle, params.tdeploy, params.tjettison,
                            params.gThresh, params.tspan)
    xxfsph = conversions.cartI2sphPR(sol.y[:,-1], planet, sol.t[-1])
    lonf = xxfsph[1]
    latf = xxfsph[2]
    lonErr = abs((lonf - lonfCenter) - deltaSurface[0])
    latErr = abs((latf - latfCenter) - deltaSurface[1])
    
    if xxfsph[0] > params.rmin + 1e3: # if final altitude not at surface
        print('WARNING: probe missed the planet!')
        return np.NaN, (np.NaN, np.NaN), (np.NaN, np.NaN)
    
    return DVvec, (lonf, latf), (lonErr, latErr)














