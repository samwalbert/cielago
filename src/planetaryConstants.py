# -*- coding: utf-8 -*-
"""
planetaryConstants.py:
    creates dataclasses for supported central bodies and adds constants
    
Created on Mon Oct 25 17:19:41 2021

@author: Samuel Albert
"""

from dataclasses import dataclass
import numpy as np

@dataclass
class Planet:
    '''Class for containing constants assocaited with a planet or moon'''
    name: str
    
# =============================================================================
# PLANETARY CONSTANTS (mostly from Vallado, in m, kg, s, rad)
# =============================================================================
# g ratios:
    # https://nssdc.gsfc.nasa.gov/planetary/factsheet/planet_table_ratio.html
# scale heights from Braun & Justus paper

G0 = 9.80665 # Earth standard gravity, m/s^2

EARTH = Planet('Earth')
EARTH.rad = 6378.1363 * 1e3 # equatorial radius, m
EARTH.mu = 3.986004415e5 * 1e9 # gravitational parameter, m^3/s^2
EARTH.om = 2 * np.pi / (0.99726968 * 86400) # rotation rate, rad/s
EARTH.k = 1.748e-04 # S-G coefficient (see spreadsheet), kg^0.5/m
EARTH.halt = 125 * 1e3 # atmospheric interface altitude, m
EARTH.J2 = 0.0010826269
EARTH.g = 1 * G0

MARS = Planet('Mars')
MARS.rad = 3397.2 * 1e3 # equatorial radius, m
MARS.mu = 4.305e4 * 1e9 # gravitational parameter, m^3/s^2
MARS.om = 2 * np.pi / (1.02595675 * 86400) # rotation rate, rad/s
MARS.k = 1.904e-04 # S-G coefficient (see spreadsheet), kg^0.5/m
MARS.halt = 125 * 1e3 # atmospheric interface altitude, m
MARS.J2 = 0.001964
MARS.g = 0.377 * G0
MARS.Hscale = 11.1e3 # m, https://nssdc.gsfc.nasa.gov/planetary/factsheet/marsfact.html
MARS.hlim = 500 * 1e3 # limiting extent of atmosphere, arbitrarily defined
MARS.rlim = MARS.hlim + MARS.rad

VENUS = Planet('Venus')
VENUS.rad = 6052.0 * 1e3 # equatorial radius, m
VENUS.mu = 3.257e5 * 1e9 # gravitational parameter, m^3/s^2
VENUS.om = 2 * np.pi / (-243 * 86400) # rotation rate, rad/s
VENUS.k = 1.897E-04 # S-G coefficient (see spreadsheet), kg^0.5/m
VENUS.halt = 135 * 1e3 # atmospheric interface altitude, m
VENUS.J2 = 0.000027
VENUS.g = 0.907 * G0

TITAN = Planet('Titan')
TITAN.rad = 2574.73 * 1e3 # mean radius, m
TITAN.mu = 8978.1382 * 1e9 # gravitational parameter, m^3/s^2
TITAN.om = 2 * np.pi / (15.945 * 86400) # rotation rate, rad/s
TITAN.k = 1.758E-04 # S-G coefficient (see spreadsheet), kg^0.5/m
TITAN.halt = 800 * 1e3 # atmospheric interface altitude, m
# TODO: add J2 value for Titan

NEPTUNE = Planet('Neptune')
NEPTUNE.rad = 24764.0 * 1e3 # equatorial radius, m
NEPTUNE.mu = 6.809e6 * 1e9 # gravitational parameter, m^3/s^2
NEPTUNE.om = 2 * np.pi / (0.768 * 86400) # rotation rate, rad/s
NEPTUNE.k = 7.361E-05 # S-G coefficient (see spreadsheet), kg^0.5/m
NEPTUNE.halt = 1000 * 1e3 # atmospheric interface altitude, m
NEPTUNE.J2 = 0.004
NEPTUNE.g = 1.12 * G0

# =============================================================================
# Mars speed of sound table (from Braun & Justus Atm. Env. paper)
# =============================================================================
# altitude in m, speed of sound in m/s
MARS.sound = np.array([[0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120,
                        130, 140, 150],
                       [233.6, 228.5, 218.8, 211.0, 203.6, 197.0, 191.6, 188.4,
                        187.9, 188.2, 188.1, 195.5, 202.5, 208.6, 251.9, 275.5]])
MARS.sound[0,:] *= 1e3