# -*- coding: utf-8 -*-
"""
test_TPC.py:
    test functions developed for TPC guidance
Created on Tue Nov  9 11:04:28 2021

@author: Samuel Albert
"""

import numpy as np
import matplotlib.pyplot as plt

from src import sim
from src import TPCguid
from src import dynamics
from src import atmosphere
from src import conversions
from src import planetaryConstants

plt.close('all')

# =============================================================================
# Load reference trajectory data
# =============================================================================
refTraj = sim.Traj()

refTraj.datafilename = 'data/TPCrefTraj.npz'

with np.load(refTraj.datafilename) as data:
    refTraj.xxfvec = data['xxfvec']
    refTraj.tf = data['tf']
    refTraj.raTarget = data['raTarget']
    refTraj.rcTarget = data['rcTarget']
    refTraj.t = data['tNom']
    refTraj.v = data['vNom']
    refTraj.gam = data['gamNom']
    refTraj.h = data['hNom']
    refTraj.L_m = data['L_mNom']
    refTraj.D_m = data['D_mNom']
    refTraj.bank = data['bankNom']
    refTraj.eng = data['engNom']
    refTraj.hdot = data['hdotNom']
    refTraj.Hscale = data['Hscale']

mars = planetaryConstants.MARS
mars.J2 = 0

# =============================================================================
# Set up basic params
# =============================================================================
params = sim.Params()
params.rtol = 1e-10
params.atol = 1e-10


# =============================================================================
# Get lambda TCs and time series
# =============================================================================
lambdafvec = TPCguid.getTC(refTraj, mars)
lambdavec = TPCguid.getlambda(lambdafvec, refTraj, mars, params)

# =============================================================================
# Get gains time series
# =============================================================================
F1, F2, F3, F4 = TPCguid.computeGains(lambdavec, refTraj)

# =============================================================================
# Set overcontrol
# =============================================================================
# K = 0 # turn off closed-loop updates
K = 1 # 

# =============================================================================
# Closed-loop guided trajectory
# =============================================================================
perturbedDens = True

atmfilename = 'data/MarsGRAM_MC10/10.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename, perturbedDens)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)

params.tspan = (0, 10e3) # s
params.rmin = mars.rad
params.rmax = (mars.rad + mars.halt)
params.gThresh = 1

event1 = lambda t, y: sim.belowMinAltCart(t, y, params)
event1.terminal = True

event2 = lambda t, y: sim.aboveMaxAltCart(t, y, params)
event2.terminal = True
event2.direction = 1

params.events = (event1, event2)

# set apoapsis radius target
params.raTarget = 400e3 + mars.rad # m
params.rcTarget = 400e3 + mars.rad # m

MSL = sim.Vehicle('MSL')
MSL.BC = 130 # kg/m^2
MSL.LD = 0.24
MSL.bankPrev = refTraj.bank[0]

r0 = refTraj.h[0] + mars.rad # m
lon0 = 0 # doesn't matter
lat0 = 0 # doesn't matter
u0 = refTraj.v[0] # m/s
gam0 = refTraj.gam[0] # rad
hda0 = 0 # due-North

MSL.y0 = np.array([r0, lon0, lat0, u0, gam0, hda0])

def getBank(t, yy, refTraj, K, F1, F2, F3, F4, planet, vehicle, params):
    r = yy[0]
    v = yy[3]
    gam = yy[4]
    
    # compute hdot , L_m, and D_m
    rho = planet.rhoFun(r)
    D_m = rho * v**2 / (2 * vehicle.BC)
    L_m = D_m * vehicle.LD
    hdot = v * np.sin(gam)
    
    # if acceleraction below threshold, use previous bank angle
    if (np.sqrt(D_m**2 + L_m**2) < params.gThresh):
        cosBank = np.cos(vehicle.bankPrev)
    else:

        # compute control update    
        cosBank = TPCguid.computeControl(t, yy, hdot, D_m, refTraj,
                                 K, F1, F2, F3, F4, planet)
    MSL.bankPrev = np.arccos(cosBank)
    
    return np.arccos(cosBank) # rad

MSL.bankFun = lambda t, yy: getBank(t, yy, refTraj, K, F1, F2, F3, F4,
                                    mars, MSL, params)

# propagate
sol = sim.runSpherical(mars, MSL, params)

# =============================================================================
# Analyze
# =============================================================================
# step through solved trajectory
sigvec = np.empty(sol.t.shape) # bank angle profile
sigvec[:] = np.NaN
L_mvec = np.empty(sol.t.shape)
L_mvec[:] = np.NaN
D_mvec = np.empty(sol.t.shape)
D_mvec[:] = np.NaN
guidOn = np.empty(sol.t.shape)
guidOn[:] = np.NaN

# RESET initial bank angle
MSL.bankPrev = refTraj.bank[0]

for i, (xxsphi, ti) in enumerate(zip(sol.y.T, sol.t)):
    sigvec[i] = MSL.bankFun(ti, xxsphi)
    dydt, L_mvec[i], D_mvec[i], _, _ = dynamics.spherical(ti, xxsphi, mars,
                                                             MSL, True)
    if (np.sqrt(L_mvec[i]**2 + D_mvec[i]**2) < params.gThresh):
        guidOn[i] = 0
    else:
        guidOn[i] = 1

ra, rp, engf = conversions.getApsesSph(sol.y[:,-1], mars, sol.t[-1])
print('Final orbit: {0:.2f} x {1:.2f} km'.format((ra-mars.rad)/1e3,
                                                 (rp-mars.rad)/1e3))

# reference trajectory info
raRef, rpRef, engfRef = conversions.getApses(refTraj.xxfvec, mars)
print('Reference final orbit: {0:.2f} x {1:.2f} km'.format((raRef-mars.rad)/1e3,
                                                           (rpRef-mars.rad)/1e3))


# =============================================================================
# Plot
# =============================================================================
fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(sol.y[3,:]/1e3, (sol.y[0,:] - mars.rad)/1e3, label = 'guided traj.')
ax.plot(refTraj.v/1e3, refTraj.h/1e3, '--', label = 'ref. traj.')
ax.set_xlabel('velocity, km/s')
ax.set_ylabel('altitude, km')
ax.grid()
ax.legend()

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(sol.t, L_mvec, label = 'lift')
ax.plot(sol.t, D_mvec, label = 'drag')
ax.set_xlabel('time, s')
ax.set_ylabel('acceleration, m/s^2')
ax.grid()
ax.legend()

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(sol.t, np.degrees(sigvec), label = 'guided traj.')
ax.plot(refTraj.t, np.degrees(refTraj.bank), '--', label = 'ref. traj.')
ax.plot(sol.t, guidOn*10, '-.', label = 'guidance status')
ax.set_xlabel('time, s')
ax.set_ylabel('bank angle, deg')
ax.grid()
ax.legend()


    
    
    
    
    


















