# -*- coding: utf-8 -*-
"""
spitGRAMdata.py:
    takes MarsGRAM data and splits a single N-run file into N single-run files.
Created on Wed Nov 10 14:50:46 2021

@author: Samuel Albert
"""

import pandas as pd

# =============================================================================
# Read data and split into multiple dataframs
# =============================================================================
# NOTE: main file MUST have altitude in ascending order for the below to work
mainfilename = 'data/EarthGRAM_MC5001'
df = pd.read_csv(mainfilename + '.txt', delim_whitespace = True)

i = 0
hPrev = -1
dfList = []

for j, h in enumerate(df.Hgtkm):
    if h < hPrev:
        dfList.append(df.iloc[i:j])
        i = j
    hPrev = h

# get the last series
dfList.append(df.iloc[i:j+1])

# =============================================================================
# Write new file for each dataframe
# =============================================================================
# NOTE: create appropriately named directory by hand
# NOTE: the data manipulation is specific to MarsGRAM, tweak for other GRAMs
for i, dfi in enumerate(dfList):
    dfi.to_csv(mainfilename + '/' + str(i+1) + '.txt',
               sep = ' ', columns = ['Hgtkm', 'DensMean', 'DensPert'])
