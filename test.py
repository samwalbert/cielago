# -*- coding: utf-8 -*-
"""
test.py:
    temporary testing script

Created on Mon Oct 25 17:17:07 2021

@author: Samuel Albert
"""

from src import sim
from src import planetaryConstants
from src import atmosphere
from src import conversions

import numpy as np
import matplotlib.pyplot as plt

plt.close('all')

# =============================================================================
# Set up Planet, Vehicle, Params
# =============================================================================
mars = planetaryConstants.MARS
atmfilename = 'data/dens_Mars_nom.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)

shield = sim.Vehicle('SHIELD')
shield.BC = 10 # kg/m^2
shield.LD = 0

# must define bank function, but it doesn't matter since L/D = 0
shield.bankFun = lambda t, yy: 0

# initial state
r0 = (mars.rad + mars.halt) # m
lon0 = 30
lat0 = 40
u0 = 6e3 # m/s
gam0 = np.radians(-12)
hda0 = np.radians(90)
shield.y0 = np.array([r0, lon0, lat0, u0, gam0, hda0])

params = sim.Params()
params.rtol = 1e-11
params.atol = 1e-11
params.tspan = (0, 10e3) # s
params.rmin = mars.rad
params.rmax = (mars.rad + mars.halt)

event1 = lambda t, y: sim.belowMinAltSph(t, y, params)
event1.terminal = True

event2 = lambda t, y: sim.aboveMaxAltSph(t, y, params)
event2.terminal = True
event2.direction = 1

params.events = (event1, event2)

# =============================================================================
# Propagate backwards from atm entry to get state at maneuver
# =============================================================================
tback = 1 * 24 * 60 * 60 # s
params.tspan = (tback, 0)
params.events = ()

solBack = sim.runSpherical(mars, shield, params)
xx0sphCenter = solBack.y[:,-1]

# =============================================================================
# Get LVLH directions at this initial state, pre-maneuver
# =============================================================================
xx0vecCenter = conversions.sphPR2cartI(xx0sphCenter, mars, 0) # deltat = 0
r0vecCenter = xx0vecCenter[:3]
v0vecCenter = xx0vecCenter[3:]
rhat, thetahat, hhat = conversions.getLVLH(r0vecCenter, v0vecCenter)

# =============================================================================
# Propagate to get lat, lon values for center trajectory
# =============================================================================
shield.y0 = xx0sphCenter
params.tspan = (0, 5e5)
params.events = (event1, event2)
solCenter = sim.runSpherical(mars, shield, params)

fig = plt.figure()
ax = fig.add_subplot(111)
# ax.plot(solCenter.y[3,:]/1e3, (solCenter.y[0,:] - mars.rad)/1e3)
ax.plot(solCenter.t, solCenter.y[3,:]/1e3)
ax.grid()


# =============================================================================
# Numerically build Jacobian
# =============================================================================
pert = 1e-5 # m

# perturb radial velocity
v0vecRadial = v0vecCenter # + pert * rhat
xx0vecRadial = np.block([r0vecCenter, v0vecRadial]).flatten()
xx0sphRadial = conversions.cartI2sphPR(xx0vecRadial, mars, 0)
shield.y0 = xx0sphRadial
solRadial = sim.runSpherical(mars, shield, params)
lonRadial = solRadial.y[1,-1]
latRadial = solRadial.y[2,-1]








