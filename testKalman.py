# -*- coding: utf-8 -*-
"""
testKalman.py:
    test updating with density measurements via Kalman gain
Created on Tue Jun 21 13:43:18 2022

@author: Samuel Albert
"""

import numpy as np
import matplotlib.pyplot as plt

plt.close('all')

#%% load data
# filename = 'data/MarsGRAM_MC5000/all_perts.npz'
filename = 'data/MarsGRAM_MC5000/all.npz'

dat = np.load(filename)
h = dat['h']
rho = dat['rhoData'][:,:3000]

# density statistics
mean_prior = np.mean(rho, axis = 1)
STD_prior = np.std(rho, axis = 1)

# compute perturbations and their statistics
rho_pert = rho / mean_prior[:,None] - 1

pert_mean_prior = np.mean(rho_pert, axis = 1)

rho_pert_centered = rho_pert - pert_mean_prior[:,None]
covmat_prior = np.cov(rho_pert_centered)

pert_STD_prior = np.sqrt(np.diag(covmat_prior))

#%% plot before measurement update
fig, ax = plt.subplots()
ax.plot(pert_mean_prior, h/1e3, 'C0--', label = 'prior mean')
ax.fill_betweenx(h/1e3, pert_mean_prior - 3*pert_STD_prior,
                 pert_mean_prior + 3*pert_STD_prior, alpha = 0.2,
                 label = r'prior $3\sigma$ uncertainty')
ax.grid()
ax.set_xlabel('normalized density perturbation')
ax.set_ylabel('altitude, km')
# ax.legend()

#%% measurement update
R = np.array([[0.01]])
obs_profile = dat['rhoData'][:,3001]
obs_pert_profile = obs_profile / mean_prior - 1
dd = obs_profile.shape[0]

# ob_ind_list = np.linspace(0, 250)
# for ob_ind in range(300):
    
ob_ind_list = [235, 225, 215, 205, 195]
for i, ob_ind in enumerate(ob_ind_list):

    h_ob = h[ob_ind]
    rho_pert_ob = np.array([obs_pert_profile[ob_ind]])
    H = np.zeros((1, dd))
    H[0,ob_ind] = 1
    P = covmat_prior
    
    K = P @ H.T @ np.linalg.inv(H @ P @ H.T + R)
    pert_mean_post = pert_mean_prior + K @ (rho_pert_ob - pert_mean_prior[ob_ind])
    cov_post = (np.eye(dd) - K @ H) @ P @ (np.eye(dd) - K @ H).T + K @ R @ K.T
    
    pert_STD_post = np.sqrt(np.diag(cov_post))
    
    if i == 0:
        ax.plot(rho_pert_ob, h_ob/1e3, 'kx', label = 'observations')
    else:
        ax.plot(rho_pert_ob, h_ob/1e3, 'kx')
    
    pert_mean_prior = pert_mean_post
    covmat_prior = cov_post

#%%
ax.fill_betweenx(h/1e3, pert_mean_post - 3*pert_STD_post,
                 pert_mean_post + 3*pert_STD_post, alpha = 0.4,
                 label = r'posterior $3\sigma$ uncertainty')
ax.plot(obs_pert_profile, h/1e3, 'C2--', alpha = 0.6, label = 'true profile')
ax.plot(pert_mean_post, h/1e3, 'C1--', label = 'posterior mean')
ax.set_xlabel(r'normalized density perturbation')
ax.set_ylabel('altitude, km')
ax.legend()
plt.tight_layout()