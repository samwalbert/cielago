# -*- coding: utf-8 -*-
"""
TPCAnalysis.py:
    analysis of Monte Carlo results 
Created on Tue Nov 16 21:59:28 2021

@author: Samuel Albert
"""

import dill
import pickle
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt

from src import sim
from src import planetaryConstants

plt.close('all')

# =============================================================================
# Load
# =============================================================================
mars = planetaryConstants.MARS

resfilename = 'results/TPC_MC2_N50.results'

with open(resfilename, 'rb') as pfile:
    res = pickle.load(pfile)

# =============================================================================
# Print statistics
# =============================================================================
print('apoapsis altitude results (km):')
minmax, mean, var = stats.describe((res.ras - mars.rad)/1e3)[1:4]
print('mean of {0:.3f} km, STD of {1:.3f} km'.format(mean, np.sqrt(var)))
print('range: {0:.3f} to {1:.3f} km'.format(minmax[0], minmax[1]))

print('\nfinal state variance results:')
xxfvariance = stats.describe(res.xxfsphs)[3]
print('radius: {0:.3e} m^2'.format(xxfvariance[0]))
print('velocity: {0:.3e} m^2/s^2'.format(xxfvariance[3]))
print('flight-path angle: {0:.3e} rad^2'.format(xxfvariance[4]))

# print('\nstate variance at 1001 seconds:')

refTrajfilename = 'data/TPCrefTrajConst.refTraj'
with open(refTrajfilename, 'rb') as pfile:
    refTraj = dill.load(pfile)

# =============================================================================
# Plots
# =============================================================================
fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
fig2 = plt.figure()
ax2 = fig2.add_subplot(111)
fig3 = plt.figure()
ax3 = fig3.add_subplot(111)

for i, sigvec in enumerate(res.sigvecs):
    ax1.plot(res.ts[i], np.degrees(sigvec))
    ax1.plot(refTraj.t, np.degrees(refTraj.bank), '--')
    
    ax2.plot(res.ts[i], 10 * res.TPCstatuses[i])
    ax2.plot(res.ts[i], res.gloads[i], '.')
    ax2.plot(refTraj.t, refTraj.gLoad, '--')
    
    ax3.plot(res.xxsphs[i][3,:]/1e3, (res.xxsphs[i][0,:]-mars.rad)/1e3)
    ax3.plot(refTraj.v/1e3, (refTraj.r-mars.rad)/1e3, '--')
    
ax1.grid()
ax2.grid()
ax3.grid()












    