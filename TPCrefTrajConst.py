# -*- coding: utf-8 -*-
"""
TPCrefTraj.py:
    Script to execute open-loop reference trajectory to be tracked by TPC guid.
Created on Thu Nov  4 11:13:29 2021

@author: Samuel Albert
"""

from src import sim
from src import TPCguid
from src import dynamics
from src import atmosphere
from src import conversions
from src import planetaryConstants

import dill
import numpy as np
import pandas as pd
from scipy.io import savemat
import matplotlib.pyplot as plt
from scipy.optimize import root_scalar

plt.close('all')

#%%
# =============================================================================
# Set up Planet
# =============================================================================
mars = planetaryConstants.MARS
mars.J2 = 0 # override real J2 value to ensure energy monotonically decreases
mars.om = 0
atmfilename = 'data/MarsGRAMNominal.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)

Tpresfilename = 'data/TpresHgtNominal.txt'
df = pd.read_csv(Tpresfilename, delim_whitespace = True)
mars.hListH = df.Var_X * 1000
mars.HscaleList = df.Hrho * 1000
mars.HscaleFun = lambda r: np.interp((r - mars.rad), mars.hListH,
                                     mars.HscaleList)


#%%
# =============================================================================
# Set up Params
# =============================================================================
params = sim.Params()
params.rtol = 1e-12
params.atol = 1e-12
params.tspan = (0, 750) # s
# params.rmin = mars.rad
# params.rmax = (mars.rad + mars.halt)

# event1 = lambda t, y: sim.belowMinAltCart(t, y, params)
# event1.terminal = True

# event2 = lambda t, y: sim.aboveMaxAltCart(t, y, params)
# event2.terminal = True
# event2.direction = 1

# params.events = (event1, event2)
params.events = ()

# set apoapsis radius target
params.raTarget = 5000e3 + mars.rad # m
params.rcTarget = 5000e3 + mars.rad # m

#%%
# =============================================================================
# Set up Vehicle
# =============================================================================
MSL = sim.Vehicle('MSL')
MSL.BC = 130 # kg/m^2
MSL.LD = 0.24

# initial conditions, spherical planet-relative
r0 = mars.halt + mars.rad # m
lon0 = 0
lat0 = 0
u0 = 5.8e3 # m/s
gam0 = np.radians(-9.8) # rad
hda0 = 0 # due-North
MSL.y0Sph = np.array([r0, lon0, lat0, u0, gam0, hda0])

# convert ICs to Cartesian inertial
MSL.y0 = conversions.sphPR2cartI(MSL.y0Sph, mars, params.tspan[0])

# define bank-angle profile
def bankProfile(t, yy, sig0):
    return sig0

#%%
# =============================================================================
# Define objective function to target an apoapsis
# =============================================================================
def runRefTraj(sig0):
    # set bank profile using updated bank angle
    MSL.bankFun = lambda t, yy: bankProfile(t, yy, sig0)
    
    # propagate
    sol = sim.runCartesian(mars, MSL, params)
    
    # get apses
    ra, rp, eng = conversions.getApses(sol.y[:,-1], mars)
    
    # return error
    print('sig0 = {0:.5f} deg, err = {1:.3f} m'.format(np.degrees(sig0), (ra - params.raTarget)))
    return ra - params.raTarget

#%%
# =============================================================================
# Solve for bank angle
# =============================================================================
res = root_scalar(runRefTraj, bracket = (np.radians(85), np.radians(95)),
                      method = 'brentq')
assert res.converged, 'failed to root-find switching time'

#%%
# =============================================================================
# Run optimized trajectory
# =============================================================================
MSL.bankFun = lambda t, yy: bankProfile(t, yy, res.root)
# MSL.bankFun = lambda t, yy: bankProfile(t, yy, np.radians(85))

# t_eval = np.arange(0, 1500+0.1, 0.1)
# t_eval = np.around(np.linspace(0, 1500, 15001), 2)
t_eval = None
params.t_eval = t_eval
sol = sim.runCartesian(mars, MSL, params, t_eval)

# get apses
ra, rp, eng = conversions.getApses(sol.y[:,-1], mars)
print((ra-mars.rad)/1e3)

# Convert cartesian results to spherical coordinates at every timestep
xxsph = conversions.cartI2sphPR(sol.y, mars, sol.t)

# analyze solved trajectory
sigvec = np.empty(sol.t.shape) # bank angle profile
sigvec[:] = np.NaN
L_mvec = np.empty(sol.t.shape) # lift acceleration magnitude
L_mvec[:] = np.NaN
D_mvec = np.empty(sol.t.shape) # drag acceleration magnitude
D_mvec[:] = np.NaN
engvec = np.empty(sol.t.shape) # specific energy, later used as ind. var.
engvec[:] = np.NaN
hdot = np.empty(sol.t.shape) # altitude rate, m/s
hdot[:] = np.NaN
Hscale = np.empty(sol.t.shape) # scale height, m
Hscale[:] = np.NaN
rhoNom = np.empty(sol.t.shape)
rhoNom[:] = np.NaN # density, kg/m^3
gLoad = np.empty(sol.t.shape)
gLoad[:] = np.NaN

for i, (xxveci, ti) in enumerate(zip(sol.y.T, sol.t)):
    sigvec[i] = MSL.bankFun(ti, xxveci)
    dydt, Lvec, Dvec, gvec = dynamics.cartesian(ti, xxveci, mars, MSL, True)
    L_mvec[i] = np.linalg.norm(Lvec)
    D_mvec[i] = np.linalg.norm(Dvec)
    _, _, engvec[i] = conversions.getApses(xxveci, mars)
    gLoad[i] = np.sqrt(L_mvec[i]**2 + D_mvec[i]**2)
    
    # compute hdot by hand for ref. traj.
    hdot[i] = xxsph[3,i] * np.sin(xxsph[4,i]) # u * sin(gamma)
    
    # interpolate current density scale height from MarsGRAM data
    r = np.linalg.norm(xxveci[:3])
    Hscale[i] = mars.HscaleFun(r)
    rhoNom[i] = mars.rhoFun(r)


#%%
# =============================================================================
# Plots
# =============================================================================
fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(xxsph[3,:]/1e3, (xxsph[0,:] - mars.rad)/1e3, label = 'ref. traj.')
ax.set_xlabel('velocity, km/s')
ax.set_ylabel('altitude, km')
ax.grid()
ax.legend()

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(sol.t, np.degrees(sigvec), label = 'ref. traj.')
ax.set_xlabel('time, s')
ax.set_ylabel('bank angle, deg')
ax.grid()
ax.legend()

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(sol.t, L_mvec, label = 'lift')
ax.plot(sol.t, D_mvec, label = 'drag')
ax.plot(sol.t, np.sqrt(L_mvec**2 + D_mvec**2))
ax.set_xlabel('time, s')
ax.set_ylabel('acceleration, m/s^2')
ax.grid()
ax.legend()

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(sol.t, engvec)
ax.set_xlabel('time, s')
ax.set_ylabel('energy, m^2/s^2')
ax.grid()


#%%
# =============================================================================
# Save reference trajectory data to a file
# =============================================================================
# build Traj object containing the useful data
refTraj = sim.Traj()
refTraj.datafilename = 'data/TPCrefTrajConst.refTraj'

refTraj.xxfvec = sol.y[:,-1]
refTraj.tf = sol.t[-1]
refTraj.xxsph = xxsph
refTraj.t = sol.t
refTraj.r = xxsph[0,:]
refTraj.h = (xxsph[0,:] - mars.rad)
refTraj.lon = xxsph[1,:]
refTraj.lat = xxsph[2,:]
refTraj.v = xxsph[3,:]
refTraj.gam = xxsph[4,:]
refTraj.hda = xxsph[5,:]
refTraj.L_m = L_mvec
refTraj.D_m = D_mvec
refTraj.bank = sigvec
refTraj.eng = engvec
refTraj.hdot = hdot
refTraj.Hscale = Hscale
refTraj.rho = rhoNom
refTraj.raTarget = params.raTarget
refTraj.rcTarget = params.rcTarget
refTraj.gLoad = gLoad

refTraj.planet = mars
refTraj.params = params
refTraj.vehicle = MSL

# =============================================================================
# Generate costates at each time step
# =============================================================================
params.gThresh = 5
params.engThresh = -2e6
lambdafvec = TPCguid.getTC(refTraj, mars)
lambdavec = TPCguid.getlambda(lambdafvec, refTraj, mars, params, MSL)

refTraj.gThresh = params.gThresh
refTraj.engThresh = params.engThresh
refTraj.lambdafvec = lambdafvec
refTraj.lambdavec = lambdavec


# =============================================================================
# Save to dill pickle file
# =============================================================================
with open(refTraj.datafilename, 'wb') as pfile:
    dill.dump(refTraj, pfile)


# #%%
# # Save to .mat file
# mdic = {"r": xxsph[0,:],
#         "lon": xxsph[1,:],
#         "lat": xxsph[2,:],
#         "v": xxsph[3,:],
#         "fpa": xxsph[4,:],
#         "hda": xxsph[5,:],
#         "t": sol.t,
#         "bank": sigvec,
#         "rho": rhoNom,
#         "H": Hscale,
#         "LD": MSL.LD,
#         "BC": MSL.BC,
#         "mu": mars.mu,
#         "rad": mars.rad,
#         "raTarget": params.raTarget,
#         "rcTarget": params.rcTarget,
#         "gLoad": gLoad
#         }
# savemat('refTraj.mat',mdic)



