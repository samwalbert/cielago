# -*- coding: utf-8 -*-
"""
testHscale.py:
    check the way scale height is computed
Created on Sat Nov 20 15:24:49 2021

@author: Samuel Albert
"""

from src import planetaryConstants, atmosphere
import numpy as np
import pandas as pd
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt

plt.close('all')

atmfilename = 'data/MarsGRAMNominal.txt'
hList, rhoList = atmosphere.readMarsGRAM(atmfilename)

Tpresfilename = 'data/TpresHgtNominal.txt'
df = pd.read_csv(Tpresfilename, delim_whitespace = True)
hListH = df.Var_X * 1000
HscaleList = df.Hrho * 1000



def getdrho(h, rho):
    H = np.interp(h, hList, HscaleList)
    drhodh = - rho / H
    return drhodh

rho0 = [rhoList[0]]
sol = solve_ivp(getdrho, (0, 200e3), rho0, atol = 1e-12, rtol = 1e-12)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.semilogy(sol.t/1e3, sol.y[0,:], label = 'scale height density')
ax.semilogy(hList/1e3, rhoList, '--', label = 'GRAM density')
ax.grid()
ax.legend()
ax.set_xlabel('altitude, km')
ax.set_ylabel('density, kg/m^3')