# -*- coding: utf-8 -*-
"""
testLinCov.py:
    test script for LinCov related functions
Created on Wed Nov 17 14:32:58 2021

@author: Samuel Albert
"""

import sys
import dill
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp

from src import UQ
from src import TPCguid

plt.close('all')

# =============================================================================
# Load in reference trajectory info
# =============================================================================

refTrajfilename = 'data/TPCrefTrajConst.refTraj'
with open(refTrajfilename, 'rb') as pfile:
    refTraj = dill.load(pfile)

params = refTraj.params
planet = refTraj.planet
vehicle = refTraj.vehicle

# compute costate vectors offline
lambdafvec = TPCguid.getTC(refTraj, planet)
lambdavec = TPCguid.getlambda(lambdafvec, refTraj, planet, params)

# set costate manually - should be the same as in the ref. traj.
Koc = 1
gThresh = 3

# =============================================================================
# Set up density KLE using density perturbations
# =============================================================================
filename = 'data/MarsGRAM_MC5000/all_perts.npz'
pfact = 1
alpha = 0.95

evals, evecs, rhoSampMean, d, hList = UQ.getEproblem(filename, pfact, alpha)
# truncate based on d value
evals = evals[:d]
evecs = evecs[:,:d]
# rhoFun, Ys = UQ.getKLEfun(evals, evecs, rhoSampMean, d, hList)
dat = np.load(filename)
rhoMean = dat['rhoMean']


# =============================================================================
# Define dynamics function
# =============================================================================
LD = vehicle.LD
BC = vehicle.BC
mu = planet.mu

eveci = np.empty(evals.shape[0])
eveci[:] = np.NaN

def getSdot(S, t):
    # debug
    # print(np.max(S))
    # extract states, costates, and params at current time
    i = np.where(refTraj.t == t)[0][0]
    
    rho = refTraj.rho[i]
    H = refTraj.Hscale[i]
    r = refTraj.r[i]
    h = r - planet.rad
    v = refTraj.v[i]
    gam = refTraj.gam[i]
    bank = refTraj.bank[i]
    gLoad = refTraj.gLoad[i]
    eng = refTraj.eng[i]
    
    costates = lambdavec[:4,i]
    lambdau = lambdavec[4,i]

    # build A matrix
    dfrdr = 0
    dfrdv = np.sin(gam)
    dfrdgam = v * np.cos(gam)
    dfrdR = 0
    
    dfvdr = rho * v**2 / (2 * H * BC) + 2 * mu * np.sin(gam) / r**3
    dfvdv = -rho * v / BC
    dfvdgam = - mu * np.cos(gam) / r**2
    dfvdR = 0
    
    dfgamdr = -rho * v * LD / (2 * H * BC) * np.cos(bank)\
            + (2 * mu / r**3 - v**2/r**2) * np.cos(gam) / v
    dfgamdv = rho * LD / (2 * BC) * np.cos(bank)\
            + np.cos(gam) / r * (mu / (v**2 * r) + 1)
    dfgamdgam = (mu/r**2 - v**2/r) * np.sin(gam) / v
    dfgamdR = 0
    
    dfRdr = 0
    dfRdv = np.cos(gam)
    dfRdgam = -v * np.sin(gam)
    dfRdR = 0
    
    A = np.array([[dfrdr, dfrdv, dfrdgam, dfrdR],
                  [dfvdr, dfvdv, dfvdgam, dfvdR],
                  [dfgamdr, dfgamdv, dfgamdgam, dfgamdR],
                  [dfRdr, dfRdv, dfRdgam, dfRdR]])
    
    # build B matrix
    if (gLoad < gThresh) or (eng < 0):
        # print('control is off')
        B = np.zeros(4)
    else:
        # print('control is on')
        dfrdu = 0
        dfvdu = 0
        dfgamdu = rho * v * LD / (2 * BC)
        dfRdu = 0
        B = np.array([dfrdu, dfvdu, dfgamdu, dfRdu])
    
    # compute K matrix
    K = - Koc * costates / lambdau
    
    Acl = A + B[:,None] @ K[None,:]
    
    # get eigenvalue and eigenvector element for current altitude
    for jj in range(evals.shape[0]):
        eveci[jj] = np.interp(h, hList, evecs[:,jj])
    
    rhoMeani = np.interp(h, hList, rhoMean)
    
    # build C matrix
    C1fact = 0
    C2fact = - v**2 / (2*BC)
    C3fact = v * LD / (2*BC) * np.cos(bank)
    C4fact = 0
    
    Csub2 = np.empty((4, d))
    for kk in range(d):
        drhodwi = np.sqrt(evals[kk]) * eveci[kk] * rhoMeani # rhoMeani for pert
        Csub2[0, kk] = C1fact * drhodwi
        Csub2[1, kk] = C2fact * drhodwi
        Csub2[2, kk] = C3fact * drhodwi
        Csub2[3, kk] = C4fact * drhodwi
    
    Csub1 = np.zeros((4,4))
    C = np.block([Csub1, Csub2])
    
    Sdot = Acl @ S + C
    # Sdot = Acl @ S
    # Sdot = A @ S
    
    return Sdot


# =============================================================================
# RK4 propagate S
# =============================================================================
S0 = np.block([np.eye(4), np.zeros((4, d))])
# Sdot = getSdot(S0, 0)

tvec = refTraj.t[1:1001:2]

S = np.empty((len(tvec), S0.shape[0], S0.shape[1]))
S[:] = np.NaN
S[0,:,:] = S0

for i in range(len(tvec)-1):
    Si = S[i,:,:]
    ti = tvec[i]
    h = tvec[i+1] - ti
    # print(ti)
    
    k1 = h * getSdot(Si, ti)
    k2 = h * getSdot(Si + 1/2 * k1, ti + 1/2 * h)
    k3 = h * getSdot(Si + 1/2 * k2, ti + 1/2 * h)
    k4 = h * getSdot(Si + k3, ti + h)
    S[i+1,:,:] = Si + 1/6 * k1 + 1/3 * k2 + 1/3 * k3 + 1/6 * k4
    
    if np.isnan(np.sum(S[i+1,:,:])):
        sys.exit('NaN value in S')


Sf = S[-1,:,:]

Sigmap0 = np.eye(d)
# P0 = np.diag([0, (20/3)**2, (np.radians(0.5)/3)**2, 0])
# note: make Sigmap0 = zeros when atm dispersions off
P0 = np.diag([0, 0, 0, 0])
Sig = np.block([[P0, np.zeros((4, d))], [np.zeros((d, 4)), Sigmap0]])

# get P at each time
P = np.empty((len(tvec), P0.shape[0], P0.shape[1]))
P[:] = np.NaN
P[0,:,:] = P0
for i, t in enumerate(tvec):
    P[i,:,:] = S[i,:,:] @ Sig @ S[i,:,:].T


Pf = Sf @ Sig @ Sf.T

print(Pf)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.semilogy(tvec, P[:,0,0])
ax.grid()
ax.set_ylabel('radius variance')


    























# # =============================================================================
# # Set up function to get Acl, C matrices
# # =============================================================================
# K = 3

# getMatsFun = lambda t: TPCguid.getAclC(K, lambdavec, refTraj, planet, vehicle,
#                                       evals, evecs, d, hList, rhoMean, t, True)

# # # =============================================================================
# # # Debugging: find A and C matrices at every time
# # # =============================================================================
# # AList = []
# # CList = []
# # # for t in sol.t:
# # for t in range(782):
# #     A, C = getMatsFun(t)
# #     AList.append(A)
# #     CList.append(C)

# # =============================================================================
# # Propagate S matrix
# # =============================================================================

# S0 = np.block([np.eye(4), np.zeros((4, d))])
# S0vec = np.reshape(S0, (S0.size,))

# sol = solve_ivp(lambda t, yy: TPCguid.getSdot(t, yy, getMatsFun),
#                 (refTraj.t[0], refTraj.t[-1]), S0vec,
#                 rtol = params.rtol, atol = params.atol)

# print(sol.message)
# Sfvec = sol.y[:,-1]
# Sf = np.reshape(Sfvec, S0.shape)




# Sigmap0 = np.eye(d)
# P0 = np.diag([0, (20/3)**2, (np.radians(0.5)/3)**2, 0])
# # P0 = np.diag([0, 0, 0, 0])
# Sig = np.block([[P0, np.zeros((4, d))], [np.zeros((d, 4)), Sigmap0]])


# Pf = Sf @ Sig @ Sf.T

# print(Pf)









