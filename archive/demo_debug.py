# -*- coding: utf-8 -*-
"""
demo.py:
    Demonstrates propagation in both spherical and cartesian coordinates, and
        shows the equivalence of the results through plots. Also demos 
        conversions between spherical and cartesian for single values & arrays.

Created on Mon Oct 25 17:17:07 2021

@author: Samuel Albert
"""

from src import sim
from src import planetaryConstants
from src import atmosphere
from src import conversions

import numpy as np
import matplotlib.pyplot as plt
import copy

plt.close('all')

# =============================================================================
# Set up base Planet, Vehicle, and Params objects
# =============================================================================
mars = planetaryConstants.MARS
atmfilename = 'data/MarsGRAMNominal.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)

### DEBUG
mars.rhoFun = lambda r: 0
# mars.J2 = 0
### 

shield = sim.Vehicle('SHIELD')
shield.BC = 10 # kg/m^2
shield.LD = 0

params = sim.Params()
params.rtol = 1e-10
params.atol = 1e-10
params.tspan = (0, 10e3) # s
params.rmin = mars.rad
params.rmax = (mars.rad + mars.halt)

# =============================================================================
# Set up spherical coordinates case
# =============================================================================
shieldSph = copy.deepcopy(shield)
shieldSph.bankFun = lambda t, yy: 0

r0 = (mars.rad + mars.halt) # m
lon0 = np.radians(1718.873)
lat0 = np.radians(2291.831)
# lon0 = np.radians(680)
# lat0 = np.radians(400)
u0 = 6e3 # m/s
gam0 = np.radians(-12)
hda0 = np.radians(90)
shieldSph.y0 = np.array([r0, lon0, lat0, u0, gam0, hda0])

# =============================================================================
# debug conversions
# =============================================================================
sph1 = shieldSph.y0
cart1 = conversions.sphPR2cartI(sph1, mars, 0)
sph2 = conversions.cartI2sphPR(cart1, mars, 0)
cart2 = conversions.sphPR2cartI(sph2, mars, 0)

print(sph1)
print(sph2)
print()
print(cart1)
print(cart2)

#####################################

paramsSph = copy.deepcopy(params)

event1Sph = lambda t, y: sim.belowMinAltSph(t, y, params)
event1Sph.terminal = True

event2Sph = lambda t, y: sim.aboveMaxAltSph(t, y, params)
event2Sph.terminal = True
event2Sph.direction = 1

paramsSph.events = (event1Sph, event2Sph)

# =============================================================================
# Set up cartesian coordinates case
# =============================================================================
shieldCart = copy.deepcopy(shield)
shieldCart.bankFun = lambda t, yy: 0

# get initial state by converting the spherical initial state
shieldCart.y0 = conversions.sphPR2cartI(shieldSph.y0, mars, params.tspan[0])

paramsCart = copy.deepcopy(params)

event1Cart = lambda t, y: sim.belowMinAltCart(t, y, params)
event1Cart.terminal = True

event2Cart = lambda t, y: sim.aboveMaxAltCart(t, y, params)
event2Cart.terminal = True
event2Cart.direction = 1

paramsCart.events = (event1Cart, event2Cart)

# =============================================================================
# Propagate both trajectories
# =============================================================================
solSph = sim.runSpherical(mars, shieldSph, paramsSph)
solCart = sim.runCartesian(mars, shieldCart, paramsCart)

# =============================================================================
# Wrap latitude and longitude for spherical results
# =============================================================================
solSph.y = conversions.wrapLonLatArray(solSph.y)

# =============================================================================
# Convert cartesian results to spherical coordinates at every timestep
# =============================================================================
xxsphvecCart = conversions.cartI2sphPR(solCart.y, mars, solCart.t)

# =============================================================================
# Analyze
# =============================================================================
print('initial lat, lon:')
print('lat, lon = {0:.3f} deg, {1:.3f} deg'.format(np.degrees(shieldSph.y0[2]),
                                                  np.degrees(shieldSph.y0[1])))
print('wrapped lat, lon:')
print('lat, lon = {0:.3f} deg, {1:.3f} deg'.format(np.degrees(solSph.y[2,0]),
                                                   np.degrees(solSph.y[1,0])))

print()
print('initial cartesian:')
print(solCart.y[:,0])

print('reconverted cartesian:')
cart2 = conversions.sphPR2cartI(solSph.y[:,0], mars, solSph.t[0])
print(cart2)

_, _, engCart1 = conversions.getApses(solCart.y[:,0], mars)
_, _, engCart2 = conversions.getApses(cart2, mars)

# =============================================================================
# Get energy at each time step for each trajectory
# =============================================================================
engSph = np.empty(solSph.t.shape)
engSph[:] = np.NaN

for i, (xxsphi, ti) in enumerate(zip(solSph.y.T, solSph.t)):
    _, _, engSph[i] = conversions.getApsesSph(xxsphi, mars, ti)

engCart = np.empty(solCart.t.shape)
engCart[:] = np.NaN

for i, (xxveci, ti) in enumerate(zip(solCart.y.T, solCart.t)):
    _, _, engCart[i] = conversions.getApses(xxveci, mars)

# =============================================================================
# Plot both trajectories
# =============================================================================
fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(solSph.y[3,:]/1e3, (solSph.y[0,:] - mars.rad)/1e3, label = 'spherical')
ax.plot(xxsphvecCart[3,:]/1e3, (xxsphvecCart[0,:] - mars.rad)/1e3,
        'C1--', label = 'cartesian')
ax.set_xlabel('velocity, km/s')
ax.set_ylabel('altitude, km')
ax.grid()
ax.legend()

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(np.degrees(solSph.y[1,:]), np.degrees(solSph.y[2,:]),
        label = 'spherical')
ax.plot(np.degrees(xxsphvecCart[1,:]), np.degrees(xxsphvecCart[2,:]),
        'C1--', label = 'cartesian')
ax.set_xlabel('longitude, deg')
ax.set_ylabel('latitude, deg')
ax.grid()
ax.legend()

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(solSph.t, np.degrees(solSph.y[4,:]),
        label = 'spherical')
ax.plot(solCart.t, np.degrees(xxsphvecCart[4,:]),
        'C1--', label = 'cartesian')
ax.set_xlabel('time, s')
ax.set_ylabel('flight-path angle, deg')
ax.grid()
ax.legend()

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(solSph.t, engSph, label = 'spherical')
ax.plot(solCart.t, engCart, 'C1--', label = 'cartesian')
ax.set_xlabel('time, s')
ax.set_ylabel('energy, m^2/s^2')
ax.grid()
ax.legend()
















