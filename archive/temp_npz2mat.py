# -*- coding: utf-8 -*-
"""
Created on Fri Feb 25 13:08:48 2022

@author: Samuel Albert
"""

import numpy as np
import scipy.io as sio

filename = 'data/MarsGRAM_MC5000/all.npz'
data = np.load(filename)

sio.savemat('data/density.mat', data)