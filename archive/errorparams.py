# -*- coding: utf-8 -*-
"""
Created on Tue Dec  7 22:55:07 2021

@author: Samuel Albert
"""

import numpy as np
import matplotlib.pyplot as plt

s = 10
h = np.sqrt(s**2 - (s/2)**2)
tri1x = [0, s/2, s, 0]
tri1y = [0, h, 0, 0]

tri2x = np.asarray(tri1x) + 2
tri2y = tri1y

err = np.sqrt(3) * 2
s3 = 10-err
h3 = np.sqrt(s3**2 - (s3/2)**2)
tri3x = np.array([0, s3/2, s3, 0]) + err/2
tri3y = np.array([0, h3, 0, 0]) + (h-h3)/3


plt.close('all')
fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(tri1x, tri1y, 'o:', label = 'nominal')
ax.plot(tri2x, tri2y, 's:', label = 'center error')
ax.plot(tri3x, tri3y, '^:', label = 'shape error')
ax.grid()
ax.legend()

#%% do analysis for shape error of tri3
dist11 = np.sqrt((tri1x[1] - tri1x[0])**2 + (tri1y[1] - tri1y[0])**2)
dist12 = np.sqrt((tri1x[2] - tri1x[1])**2 + (tri1y[2] - tri1y[1])**2)
dist13 = np.sqrt((tri1x[0] - tri1x[2])**2 + (tri1y[0] - tri1y[2])**2)

dist31 = np.sqrt((tri3x[1] - tri3x[0])**2 + (tri3y[1] - tri3y[0])**2)
dist32 = np.sqrt((tri3x[2] - tri3x[1])**2 + (tri3y[2] - tri3y[1])**2)
dist33 = np.sqrt((tri3x[0] - tri3x[2])**2 + (tri3y[0] - tri3y[2])**2)

centx3 = np.mean(tri3x)

print(np.sqrt((dist11-dist31)**2 + (dist12-dist32)**2 + (dist13-dist33)**2) / 3)