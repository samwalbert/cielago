# -*- coding: utf-8 -*-
"""
testConversions.py:
    performs a few checks on functions defined in conversions.py
    
Created on Tue Oct 26 13:23:03 2021

@author: Samuel Albert
"""

from src import conversions
from src import planetaryConstants

import numpy as np

mars = planetaryConstants.MARS

# initial state
# r0 = (mars.rad + mars.halt) # m
# lon0 = np.radians(2173.192272329296)
# lat0 = np.radians(40)
# u0 = 6e3 # m/s
# gam0 = np.radians(-12)
# hda0 = np.radians(95)

r0 = 2.91928133e+08
lon0 = 3.79293604e+01
lat0 = 4.10220326e+01
# lat0 = 
u0 = 2.13418435e+04 # velocity super high because this is planet-relative and we are really far away, w x r gets huge
gam0 = -1.60821505e-01
hda0 = 1.57314304e+00

# r0 = 7000
# lon0 = 4
# lat0 = 7
# u0 = 8000
# gam0 = -1
# hda0 = 500

xx0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])
t = 7

# print(xx0sph)

# # convert to inertial cartesian
# print()
# xx0vec = conversions.sphPR2cartI(xx0sph, mars, t)
# print(xx0vec)

# # convert back again
# print()
# xx0sph_2 = conversions.cartI2sphPR(xx0vec, mars, t)
# print(xx0sph_2)

# just convert to cartesian and back
r0vec, v0vec = conversions.sph2cart(xx0sph, mars, t)
xx0vec = np.block([r0vec, v0vec]).flatten()
xx0sph_2 = conversions.cart2sph(xx0vec, mars, t)

# then check the readouts to match intuition,
#    and that the first and third sets are the same

# print('error:')
# print(xx0sph - xx0sph_2)

print('longitudes:')
print(np.degrees(xx0sph[1]) % 360)
print(np.degrees(xx0sph_2[1]))

print('latitudes:')
print(np.degrees(xx0sph[2]) % 360)
print(np.degrees(xx0sph_2[2]))

