# -*- coding: utf-8 -*-
"""
SHIELD_NLopt.py:
    nonlinear targeting script for SHIELD probes, using variable aerodynamics
Created on Fri Jul 15 11:54:30 2022

@author: Samuel Albert
"""

import sys
sys.path.append('..')

from src import sim
from src import planetaryConstants
from src import atmosphere
from src import conversions
from src import marvel

import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize as opt
import datetime

datestring = datetime.datetime.now().strftime('%m%d%H%M%S')
plt.close('all')

#%%
# =============================================================================
# Set up Planet, Vehicle, Params
# =============================================================================
mars = planetaryConstants.MARS
atmfilename = '../data/MarsGRAMNominal.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)
mars.MachFun = lambda r, u: u / np.interp((r - mars.rad), mars.sound[0,:],
                                          mars.sound[1,:])

shield = sim.Vehicle('SHIELD')
marvel.loadSHIELDAeroData(shield)
shield.getAero = marvel.getSHIELDAeroFun(shield, 'entry')
shield.Rn = 0.85 # m

# must define bank function, but it doesn't matter since L/D = 0
shield.bankFun = lambda t, yy: 0

# initial state
t0 = 0 # s
r0 = (mars.rad + mars.halt) # m
lon0 = np.radians(0)
lat0 = np.radians(0)
u0 = 6e3 # m/s
gam0degmag = 18
gam0 = np.radians(-gam0degmag)
hda0 = np.radians(90) # due-East
y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])
shield.y0 = conversions.sphPR2cartI(y0sph, mars, t0)

params = sim.Params()
params.rtol = 1e-11
params.atol = 1e-11
params.rmin = mars.rad
params.rmax = (mars.rad + mars.halt)

# initial and final time
params.t0 = 7e6 # s
params.tf = params.t0 + 500 # s

# define parameters for config changes
g0 = 1 # m/s^2
if gam0degmag == 12:
    tdeploy = 285
    tjettison = 295
if gam0degmag == 18:
    tdeploy = 170 # s
    tjettison = 180 # s
if gam0degmag == 24:
    tdeploy = 130
    tjettison = 140
tdeploy += params.t0
tjettison += params.t0

y0sph = conversions.sphPR2cartI(y0sph, mars, params.t0)
shield.y0 = y0sph

params.event1 = lambda t, y: sim.belowMinAltCart(t, y, params)
params.event1.terminal = True

#%%
# =============================================================================
# Back-propagate to jettison time, with constant aero
# =============================================================================
daysout = 3

shield.getAero = marvel.getSHIELDAeroFun(shield, 'entry')
params.tback = daysout * 24 * 60 * 60 # s, jettison time

params.tspan = (params.t0, (params.t0 - params.tback))
params.events = ()

solBack = sim.runCartesian(mars, shield, params)
xx0vecCenter = solBack.y[:,-1]


#%%
# =============================================================================
# Forward-propagate to get center landing site, with varying aero
# =============================================================================
shield.y0 = xx0vecCenter
params.tspan = ((params.t0 - params.tback), params.tf)
solCenter, _ = marvel.runSHIELDtimed(mars, shield, tdeploy, tjettison, g0,
                                     tspan = params.tspan)

xxfvecCenter = solCenter.y[:,-1]
tfCenter = solCenter.t[-1]
params.tf = tfCenter + 1e5
params.tspan = ((params.t0 - params.tback), params.tf)

# convert to spherical
xxfsphCenter = conversions.cartI2sphPR(solCenter.y[:,-1], mars,
                                      solCenter.t[-1])

x0vecCenter = xx0vecCenter[:3]
v0vecCenter = xx0vecCenter[3:]
lonfCenter = xxfsphCenter[1]
latfCenter = xxfsphCenter[2]

# compute LVLH unit vectors at jettison time (r, theta, h)
rhat, thetahat, hhat = conversions.getLVLH(x0vecCenter, v0vecCenter)
NO = np.block([[rhat], [thetahat], [hhat]]).T


#%% define objective function
def jettison(DVvecLVLH, target, tolcap = True):
    '''
    target = (lon, lat) in radians
    '''
    
    # convert LVLH to inertial
    DVvec = NO @ DVvecLVLH
    
    # apply DV
    v0vec = v0vecCenter + DVvec
    xx0vec = np.block([x0vecCenter, v0vec]).flatten()
    shield.y0 = xx0vec
    
    # propagate
    sol, _ = marvel.runSHIELDtimed(mars, shield, tdeploy, tjettison, g0,
                                         tspan = params.tspan)
    xxfvec = sol.y[:,-1]


    # convert target to inertial cartesian
    xxfTarget = conversions.sphPR2cartI(np.array([xxfsphCenter[0], target[0],
                                                  target[1], xxfsphCenter[3],
                                                  xxfsphCenter[4],
                                                  xxfsphCenter[5]]),
                                        mars, solCenter.t[-1])
    
    # compute error
    xxErr = xxfvec - xxfTarget
    errMag = np.linalg.norm(xxErr)
    DVmag = np.linalg.norm(DVvecLVLH)
    
    print('error: {0:.3f} km; DV: {1:.3e} m/s,  '.format(errMag/1e3, DVmag),
          DVvecLVLH, '\n')
    # print(errMag, DVvecLVLH)
    # # print(DVvecLVLH)
    # print()
    
    # if error is less than 1 km, round down to 0 error. this is necesssary
    #     because the tolerance setting on scipy.optimize.minimize is dumb.
    
    if tolcap and errMag < 5e3: # if error below 1 km,
        errMag = 0   # report 0 error
    
    return errMag


#%% 
# =============================================================================
# optimize across desired targets
# =============================================================================
# initialize
resList = []
solList = []
xxfsphList = []
DVinertialList = []
DVLVLHList = []

plabel = 'A'
offset = ((30,0))
x0 = np.array([-0.00140397,  0.67706844, -0.01078272])

target = (lonfCenter + np.radians(offset[0]),
          latfCenter + np.radians(offset[1]))

fname = '../results/maneuver_' + plabel + '_' + datestring + '.npz'

# optimize
jfun = lambda DVvecLVLH: jettison(DVvecLVLH, target)
res = opt.minimize(jfun, x0, options = {'disp': True, 'return_all': True})
print(res)

## run optimized solution
# apply DV
DVvec = NO @ res.x
v0vec = v0vecCenter + DVvec
xx0vec = np.block([x0vecCenter, v0vec]).flatten()
shield.y0 = xx0vec

# propagate
sol, _ = marvel.runSHIELDtimed(mars, shield, tdeploy, tjettison, g0,
                                     tspan = params.tspan)
xxfvec = sol.y[:,-1]
xxfsph = conversions.cartI2sphPR(sol.y[:,-1], mars, sol.t[-1])

# append to lists
resList.append(res)
solList.append(sol)
xxfsphList.append(xxfsph)
DVinertialList.append(DVvec)
DVLVLHList.append(res.x)

# save to file
np.savez(fname,
         resList = resList,
         solList = solList,
         xxfsphList = xxfsphList,
         DVinertialList = DVinertialList,
         DVLVLHList = DVLVLHList,
         offset = offset,
         lonfCenter = lonfCenter,
         latfCenter = latfCenter,
         daysout = daysout,
         allow_pickle = True)
