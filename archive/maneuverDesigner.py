# -*- coding: utf-8 -*-
"""
NOTE: scrapped this attempt because root-finding wrapped around the nonlinear
    targeter is extremely slow and annoying.
Created on Fri Jul 15 11:54:30 2022

@author: Samuel Albert
"""

import sys
sys.path.append('..')

from src import sim
from src import planetaryConstants
from src import atmosphere
from src import conversions
from src import marvel

import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize as opt
import datetime

datestring = datetime.datetime.now().strftime('%m%d%H%M%S')
plt.close('all')

#%%
# =============================================================================
# Set up Planet, Vehicle, Params
# =============================================================================
mars = planetaryConstants.MARS
atmfilename = '../data/MarsGRAMNominal.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)
mars.MachFun = lambda r, u: u / np.interp((r - mars.rad), mars.sound[0,:],
                                          mars.sound[1,:])

shield = sim.Vehicle('SHIELD')
marvel.loadSHIELDAeroData(shield)

# define Mach numbers for config changes
Mdeploy = 1
Mjettison = 0.75

# must define bank function, but it doesn't matter since L/D = 0
shield.bankFun = lambda t, yy: 0

# initial state
t0 = 0 # s
r0 = (mars.rad + mars.halt) # m
lon0 = np.radians(151)
lat0 = np.radians(7.5)
u0 = 6e3 # m/s
gam0degmag = 24
gam0 = np.radians(-gam0degmag)
hda0 = np.radians(80)
y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])
shield.y0 = conversions.sphPR2cartI(y0sph, mars, t0)

params = sim.Params()
params.rtol = 1e-11
params.atol = 1e-11
params.rmin = mars.rad
params.rmax = (mars.rad + mars.halt)

# initial and final time
params.t0 = 1e6 # s
params.tf = params.t0 + 5e3 # s

shield.y0 = conversions.sphPR2cartI(y0sph, mars, params.t0)
shield.y0Center = conversions.sphPR2cartI(y0sph, mars, params.t0)

params.event1 = lambda t, y: sim.belowMinAltCart(t, y, params)
params.event1.terminal = True

#%%
# =============================================================================
# Back-propagate to jettison time, with constant aero
# =============================================================================
shield.getAero = marvel.getSHIELDAeroFun(shield, 'entry')
daysout = 18
params.tback = daysout * 24 * 60 * 60 # s, jettison time

params.tspan = (params.t0, (params.t0 - params.tback))
params.events = ()

solBack = sim.runCartesian(mars, shield, params)
xx0vecCenter = solBack.y[:,-1]


#%%
# =============================================================================
# Forward-propagate to get center landing site, with varying aero
# =============================================================================
shield.y0 = xx0vecCenter
params.tspan = ((params.t0 - params.tback), params.tf)
solCenter, _ = marvel.runSHIELDcartesian(mars, shield, Mdeploy, Mjettison, params.tspan)

xxfvecCenter = solCenter.y[:,-1]
tfCenter = solCenter.t[-1]
params.tf = tfCenter + 1e5
params.tspan = ((params.t0 - params.tback), params.tf)

# convert to spherical
xxfsphCenter = conversions.cartI2sphPR(solCenter.y[:,-1], mars,
                                      solCenter.t[-1])

x0vecCenter = xx0vecCenter[:3]
v0vecCenter = xx0vecCenter[3:]
lonfCenter = xxfsphCenter[1]
latfCenter = xxfsphCenter[2]

# compute LVLH unit vectors at jettison time (r, theta, h)
rhat, thetahat, hhat = conversions.getLVLH(x0vecCenter, v0vecCenter)
NO = np.block([[rhat], [thetahat], [hhat]]).T

#%%
# =============================================================================
# Set up desired target points and desired DV magnitude
# =============================================================================
L = np.radians(0.5)
dlonList90 = np.array([2*L, L/2, L/2])
dlatList90 = np.array([0, L/2, -L/(2*np.sqrt(3))])
dlonList = dlonList90 * np.sin(hda0) - dlatList90 * np.cos(hda0)
dlatList = dlatList90 * np.sin(hda0) + dlonList90 * np.cos(hda0)
# dlonList = np.radians([0.1])
# dlatList = np.radians([0])

# check desired network geometry
# img = plt.imread('../data/CF_CTX_screencap.png')
fig = plt.figure()
ax = fig.add_subplot(111)
# ax.imshow(img, extent=[160.1914394, 163.8111954, 8.2036983, 10.52815])
ax.plot(np.degrees(dlonList + lonfCenter),
        np.degrees(dlatList + latfCenter), 'o')
ax.plot(np.degrees(lonfCenter), np.degrees(latfCenter), 'x')
ax.grid()
ax.set_xlabel('longitude, deg')
ax.set_ylabel('latitude, deg')
ax.set_aspect('equal', 'box')

def lon2dist(lon):
    return (np.radians(lon) - lonfCenter) * mars.rad / 1e3

def dist2lon(dist):
    return np.degrees((dist * 1e3 / mars.rad) + lonfCenter)

def lat2dist(lat):
    return (np.radians(lat) - latfCenter) * mars.rad / 1e3

def dist2lat(dist):
    return np.degrees((dist * 1e3 / mars.rad) + latfCenter)

secax = ax.secondary_xaxis('top', functions = (lon2dist, dist2lon))
secax.set_xlabel('distance, km')
secay = ax.secondary_yaxis('right', functions = (lat2dist, dist2lat))
secay.set_ylabel('distance, km')


DVdes = 0.1 # m/s


#%% define objective function
def jettison(DVvecLVLH, target, tolcap = True):
    '''
    target = (lon, lat) in radians
    '''
    
    # convert LVLH to inertial
    DVvec = NO @ DVvecLVLH
    
    # apply DV
    v0vec = v0vecCenter + DVvec
    xx0vec = np.block([x0vecCenter, v0vec]).flatten()
    shield.y0 = xx0vec
    
    # propagate
    sol, _ = marvel.runSHIELDcartesian(mars, shield, Mdeploy, Mjettison, params.tspan)
    xxfvec = sol.y[:,-1]


    # convert target to inertial cartesian
    xxfTarget = conversions.sphPR2cartI(np.array([xxfsphCenter[0], target[0],
                                                  target[1], xxfsphCenter[3],
                                                  xxfsphCenter[4],
                                                  xxfsphCenter[5]]),
                                        mars, solCenter.t[-1])
    
    # compute error
    xxErr = xxfvec - xxfTarget
    errMag = np.linalg.norm(xxErr)
    DVmag = np.linalg.norm(DVvecLVLH)
    
    print('error: {0:.3f} km; DV: {1:.3e} m/s,  '.format(errMag/1e3, DVmag),
          DVvecLVLH)
    # print(errMag, DVvecLVLH)
    # # print(DVvecLVLH)
    # print()
    
    # if error is less than 1 km, round down to 0 error. this is necesssary
    #     because the tolerance setting on scipy.optimize.minimize is dumb.
    
    if tolcap and errMag < 15e3: # if error below 1 km,
        errMag = 0   # report 0 error
    
    return errMag

#%% Root-solve to get tback for each target point
x0 = np.array([0,0,0])
ic = 0
def targetPoint(tback, offset, DVdes, x0):
    params.tback = tback
    shield.y0 = shield.y0Center
    
    # hard-coded initial guesses
    if ic == 0:
        x0 = np.array([ 0.00497772, -0.01315021, -0.00279298])
    
    # compute required DV for this combo of target and tback
    target = (lonfCenter + offset[0], latfCenter + offset[1])
    jfun = lambda DVvecLVLH: jettison(DVvecLVLH, target)
    res = opt.minimize(jfun, x0, options = {'disp': True, 'return_all': True})
    DV = res.x
    print(res.x)
    print()
    
    return DV - DVdes

tbackList = []
DVvecs = np.empty((len(dlonList), 3))
DVvecs[:] = np.NaN

for i, (dlon, dlat) in enumerate(zip(dlonList, dlatList)):
    res = opt.root_scalar(lambda tback: targetPoint(tback, (dlon, dlat), DVdes),
                          bracket = (0.5*86400, 4*86400), method = 'brentq',
                          maxiter = 10)
    # assert res.converged, 'failed to root-find tback'
    print(res.flag)
    tback = res.root
    tbackList.append(tback)
    print('tback = {0:.4f} days for target point {1:d}'.format(tback/86400, i))
    
    # get final DVvec
    params.tback = tback
    shield.y0 = shield.y0Center
    
    # compute required DV for this combo of target and tback
    target = (lonfCenter + dlon, latfCenter + dlat)
    jfun = lambda DVvecLVLH: jettison(DVvecLVLH, target)
    res = opt.minimize(jfun, x0, options = {'disp': True, 'return_all': True})
    DVvec = res.x
            
    DVvecs[i,:] = DVvec
    


#%% 
# =============================================================================
# optimize across desired targets
# =============================================================================
# initialize
resList = []
solList = []
xxfsphList = []
DVinertialList = []
DVLVLHList = []
x0 = np.array([0, 0, 0])

# set target list
# offsetList = [5, 10, 11, 12, 13, 14, 15, 30, 60, 90, 120, 150, 180]
# offsetList = np.linspace(1,180,180)
# axisOff = 'lon'
# x0 = np.array([-0.00056478,  0.07622511, -0.0011712 ])

offsetList = [5, 10, 15, 20, 25, 30, 45, 60, 75, 90]
# offsetList = [30]
# offsetList = np.linspace(1,30,30)
# offsetList = [0.5]
axisOff = 'lat'
# x0 = np.array([ 4.71251922e-06, -1.37433599e-03,  1.82260913e-01])

fname = '../results/NL_' + str(gam0degmag) + '_' + str(daysout) + '_'\
    + axisOff + '_' + datestring + '.npz'

# loop through each desired target
for offset in offsetList:
    print('\n\nTARGETING: {0:.1f} deg offset, '.format(offset) + axisOff)
    
    # set target
    if axisOff == 'lon':
        target = (lonfCenter + np.radians(offset), latfCenter)
    else:
        target = (lonfCenter, latfCenter + np.radians(offset))
        
    # hard-coded initial guesses
    if (axisOff == 'lat') and (gam0degmag == 12) and (daysout == 3):
        if (offset == 5):
            x0 = np.array([ 0.30287263, -0.09993873,  2.03498934])
        if (offset == 10):
            x0 = np.array([-0.71170743, -0.3887758 ,  4.04064815])
        if (offset == 15):
            x0 = np.array([-0.30121754, -0.87185902,  5.99858263])
        if (offset == 30):
            x0 = np.array([-4.95176813, -3.33833881, 11.38753481])
    if (axisOff == 'lat') and (gam0degmag == 18) and (daysout == 3):
        if (offset == 5):
            x0 = np.array([ 0.23806684, -0.0859011 ,  1.85396519])
        if (offset == 10):
            x0 = np.array([-0.28319593, -0.33660767,  3.69475098])
        if (offset == 15):
            x0 = np.array([ 0.05645908, -0.75274684,  5.49167027])
        if (offset == 30):
            x0 = np.array([ 6.76428906, -2.97564052, 10.55711959])
    if (axisOff == 'lat') and (gam0degmag == 24) and (daysout == 3):
        if (offset == 5):
            x0 = np.array([ 0.23808795, -0.07808996,  1.77145235])
        if (offset == 10):
            x0 = np.array([-1.09080096, -0.30195012,  3.51644304])
        if (offset == 15):
            x0 = np.array([ 1.79974759, -0.69881054,  5.24196632])
        if (offset == 30):
            x0 = np.array([ 0.60146543, -2.71448654, 10.12996567])
    if (axisOff == 'lat') and (gam0degmag == 18) and (daysout == 10):
        if (offset == 5):
            x0 = np.array([-3.80343867e-01, -1.28487508e-05, -5.65364986e-01])
        if (offset == 10):
            x0 = np.array([ 1.06722031,  0.17243542, -1.1141016 ])
        if (offset == 15):
            x0 = np.array([-0.32492119,  0.2106529 , -1.67172379])
    if (axisOff == 'lon') and (gam0degmag == 18) and (daysout == 10):
        if (offset == 5):
            x0 = np.array([ 5.45532405e-03, -7.90932282e-02,  7.40895855e-05])
        if (offset == 10):
            x0 = np.array([ 0.01254546, -0.20904882, -0.00175061])
    if (axisOff == 'lat') and (gam0degmag == 18) and (daysout == 18):
        if offset == 5:
            x0 = np.array([-0.24976529, -0.08199277, -0.31900484])
        if offset == 10:
            x0 = np.array([-0.72906019, -0.22751773, -0.6230653 ])
        if offset == 15:
            x0 = np.array([-0.7543251 , -0.15976256, -0.92760124])
        # if offset == 30:
        #     x0 = np.array([-2.5833896 , -0.4864242 , -1.77923036])
        if offset == 40:
            x0 = np.array([-1.73444902,  0.23399932, -2.28828522])
        if offset == 45:
            x0 = np.array([-1.81288432,  0.43560125, -2.51282289])
        if offset == 60:
            x0 = np.array([-2.07889473,  1.11997279, -3.04345888])
        if offset == 75:
            x0 = np.array([-2.35162821,  1.88642864, -3.35064558])
        if offset == 90:
            x0 = np.array([-2.00908803,  2.94640265, -3.42777989])
            
    
    # optimize
    jfun = lambda DVvecLVLH: jettison(DVvecLVLH, target)
    res = opt.minimize(jfun, x0, options = {'disp': True, 'return_all': True})
    print(res)
    
    # run optimized solution
    # apply DV
    DVvec = NO @ res.x
    v0vec = v0vecCenter + DVvec
    xx0vec = np.block([x0vecCenter, v0vec]).flatten()
    shield.y0 = xx0vec

    # propagate
    sol, _ = marvel.runSHIELDcartesian(mars, shield, Mdeploy, Mjettison, params.tspan)
    xxfvec = sol.y[:,-1]
    xxfsph = conversions.cartI2sphPR(sol.y[:,-1], mars, sol.t[-1])
    
    # append to lists
    resList.append(res)
    solList.append(sol)
    xxfsphList.append(xxfsph)
    DVinertialList.append(DVvec)
    DVLVLHList.append(res.x)
    
    # save to file
    np.savez(fname,
             resList = resList,
             solList = solList,
             xxfsphList = xxfsphList,
             DVinertialList = DVinertialList,
             DVLVLHList = DVLVLHList,
             offsetList = offsetList,
             lonfCenter = lonfCenter,
             latfCenter = latfCenter,
             allow_pickle = True)
    
    # update initial guess
    x0 = res.x



# #%% plot
# fig, ax = plt.subplots()
# ax.plot(sol.y[0,:]/1e3, sol.y[1,:]/1e3, label = 'targeted')
# ax.plot(solCenter.y[0,:]/1e3, solCenter.y[1,:]/1e3, '--', label = 'center')
# circ1 = plt.Circle((0,0), params.rmin/1e3, facecolor = None, label = 'surface')
# ax.add_patch(circ1)
# ax.grid()
# ax.legend()
# plt.axis('equal')

# fig, ax = plt.subplots()
# ax.plot(sol.y[1,:]/1e3, sol.y[2,:]/1e3, label = 'targeted')
# ax.plot(solCenter.y[1,:]/1e3, solCenter.y[2,:]/1e3, '--', label = 'center')
# circ1 = plt.Circle((0,0), params.rmin/1e3, facecolor = None, label = 'surface')
# ax.add_patch(circ1)
# ax.grid()
# ax.legend()
# plt.axis('equal')