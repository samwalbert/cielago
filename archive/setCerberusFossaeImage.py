# -*- coding: utf-8 -*-
"""
Created on Sun Nov 28 23:06:58 2021

@author: Samuel
"""
from src import sim
from src import planetaryConstants
from src import atmosphere
from src import conversions

import numpy as np
import matplotlib.pyplot as plt

plt.close('all')

#%%
# =============================================================================
# Set up Planet, Vehicle, Params basics
# =============================================================================
mars = planetaryConstants.MARS
atmfilename = 'data/MarsGRAMNominal.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)

shield = sim.Vehicle('SHIELD')
shield.BC = 10 # kg/m^2
shield.LD = 0

# must define bank function, but it doesn't matter since L/D = 0
shield.bankFun = lambda t, yy: 0

# initial state
r0 = (mars.rad + mars.halt) # m
lon0 = np.radians(151)
lat0 = np.radians(7.5)
u0 = 6e3 # m/s
gam0 = np.radians(-12)
hda0 = np.radians(80)
y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])

params = sim.Params()
params.rtol = 1e-11
params.atol = 1e-11
params.rmin = mars.rad
params.rmax = (mars.rad + mars.halt)

# initial and final time
params.t0 = 1e6 # s
params.tf = params.t0 + 5e3 # s

shield.y0 = conversions.sphPR2cartI(y0sph, mars, params.t0)
shield.y0Center = conversions.sphPR2cartI(y0sph, mars, params.t0)

params.event1 = lambda t, y: sim.belowMinAltCart(t, y, params)
params.event1.terminal = True

params.event2 = lambda t, y: sim.aboveMaxAltCart(t, y, params)
params.event2.terminal = True
params.event2.direction = 1

params.pert = 1e-4

#%%
# =============================================================================
# Get center states at surface
# =============================================================================
params.tspan = (params.t0, params.tf)
params.events = (params.event1, params.event2)
sol = sim.runCartesian(mars, shield, params)

# convert to spherical along trajectory
xxsph = conversions.cartI2sphPR(sol.y, mars, sol.t)
lonfCenter = xxsph[1,-1]
latfCenter = xxsph[2,-1]

print(np.degrees(lonfCenter))
print(np.degrees(latfCenter))

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(np.degrees(xxsph[1,:]), np.degrees(xxsph[2,:]))
ax.grid()

#%%
# =============================================================================
# Set up desired network geometry
# =============================================================================
L = np.radians(0.5)
dlonList90 = np.array([2*L, L/2, L/2])
dlatList90 = np.array([0, L/2, -L/(2*np.sqrt(3))])
dlonList = dlonList90 * np.sin(hda0) - dlatList90 * np.cos(hda0)
dlatList = dlatList90 * np.sin(hda0) + dlonList90 * np.cos(hda0)

dlonList = np.append(dlonList, -dlonList)
dlatList = np.append(dlatList, -dlatList)

#%%
# =============================================================================
# Plot against image
# =============================================================================
plt.close('all')
img = plt.imread('data/CF_CTX_screencap.png')
fig = plt.figure()
ax = fig.add_subplot(111)
ax.imshow(img, extent=[160.1914394, 163.8111954, 8.2036983, 10.52815])
ax.grid()
ax.set_xlabel('longitude, deg')
ax.set_ylabel('latitude, deg')
ax.set_aspect('equal', 'box')

def lon2dist(lon):
    return (np.radians(lon) - lonfCenter) * mars.rad / 1e3

def dist2lon(dist):
    return np.degrees((dist * 1e3 / mars.rad) + lonfCenter)

def lat2dist(lat):
    return (np.radians(lat) - latfCenter) * mars.rad / 1e3

def dist2lat(dist):
    return np.degrees((dist * 1e3 / mars.rad) + latfCenter)

secax = ax.secondary_xaxis('top', functions = (lon2dist, dist2lon))
secax.set_xlabel('distance, km')
secay = ax.secondary_yaxis('right', functions = (lat2dist, dist2lat))
secay.set_ylabel('distance, km')







