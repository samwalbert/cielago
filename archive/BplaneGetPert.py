# -*- coding: utf-8 -*-
"""
testBplane.py:
    test B-plane targeting related functions
    NOTE: do B-plane targeting in inertial frame, convert DV to LVLH after
Created on Fri Nov 26 15:08:12 2021

@author: Samuel
"""


from src import sim
from src import planetaryConstants
from src import atmosphere
from src import conversions

import numpy as np
import matplotlib.pyplot as plt

plt.close('all')

#%%
# =============================================================================
# Set up Planet, Vehicle, Params
# =============================================================================
mars = planetaryConstants.MARS
atmfilename = 'data/MarsGRAMNominal.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)

shield = sim.Vehicle('SHIELD')
shield.BC = 10 # kg/m^2
shield.LD = 0

# must define bank function, but it doesn't matter since L/D = 0
shield.bankFun = lambda t, yy: 0

# initial and final time
t0 = 1e6 # s
tf = t0 + 5e3 # s

# initial state
r0 = (mars.rad + mars.halt) # m
lon0 = 30
lat0 = 40
u0 = 6e3 # m/s
gam0 = np.radians(-12)
hda0 = np.radians(90)
y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])
shield.y0 = conversions.sphPR2cartI(y0sph, mars, t0)

params = sim.Params()
params.rtol = 1e-11
params.atol = 1e-11
tspan = (t0, tf) # s
params.rmin = mars.rad
params.rmax = (mars.rad + mars.halt)

event1 = lambda t, y: sim.belowMinAltCart(t, y, params)
event1.terminal = True

event2 = lambda t, y: sim.aboveMaxAltCart(t, y, params)
event2.terminal = True
event2.direction = 1

#%%
# =============================================================================
# Propagate backwards from atm entry to get state at maneuver
# =============================================================================
print('Back-propagating center vehicle...')
tback = 1 * 24 * 60 * 60 # s
params.tspan = (t0, (t0 - tback))
params.events = ()

solBack = sim.runCartesian(mars, shield, params)
xx0vecCenter = solBack.y[:,-1]

#%%
# =============================================================================
# Propagate to get lat, lon values for center trajectory
# =============================================================================
print('Forward-propagating center vehicle...')
shield.y0 = xx0vecCenter
params.tspan = ((t0 - tback), tf)
params.events = (event1, event2)
solCenter = sim.runCartesian(mars, shield, params)

# convert to spherical along trajectory
xxsphCenter = conversions.cartI2sphPR(solCenter.y, mars, solCenter.t)
lonfCenter = xxsphCenter[1,-1]
latfCenter = xxsphCenter[2,-1]

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(xxsphCenter[3,:]/1e3, (xxsphCenter[0,:] - mars.rad)/1e3)
# ax.plot(solCenter.t, xxsph[3,:]/1e3)
ax.grid()

print('center solution:')
print('(lon, lat) = ({0:.3f}, {1:.3f}) deg'\
      .format(np.degrees(lonfCenter), np.degrees(latfCenter)))

#%%
# # =============================================================================
# # TEST: propagate from original state, confirm same final state
# # =============================================================================
# print('testing for equivalent trajectory...')
# shield.y0 = conversions.sphPR2cartI(y0sph, mars, t0)
# params.tspan = (t0, tf)
# solCheck = sim.runCartesian(mars, shield, params)

# # convert to spherical along trajectory
# xxsphCheck = conversions.cartI2sphPR(solCheck.y, mars, solCheck.t)

# ax.plot(xxsphCheck[3,:]/1e3, (xxsphCheck[0,:] - mars.rad)/1e3)

# print('original solution:')
# print('(lon, lat) = ({0:.3f}, {1:.3f}) deg'\
#       .format(np.degrees(xxsphCheck[1,-1]), np.degrees(xxsphCheck[2,-1])))

# # reset tspan
# params.tspan = ((t0 - tback), tf)

#%%
# =============================================================================
# Numerically build Jacobian
# =============================================================================
fig = plt.figure()
ax = fig.add_subplot(111)

xdir = np.array([1,0,0])
ydir = np.array([0,1,0])
zdir = np.array([0,0,1])
x0vecCenter = xx0vecCenter[:3]
v0vecCenter = xx0vecCenter[3:]

for pertExp in np.arange(-15, 3, 1, dtype = float):
    pert = 10**pertExp
    print('\n\npert = {0:.2e}'.format(pert))
    
    # perturb x-direction
    print('perturbing in x-direction...')
    v0vecXpert = v0vecCenter + pert * xdir
    xx0vecXpert = np.block([x0vecCenter, v0vecXpert]).flatten()
    
    shield.y0 = xx0vecXpert
    solXpert = sim.runCartesian(mars, shield, params)
    xxsphXpert = conversions.cartI2sphPR(solXpert.y, mars, solXpert.t)
    lonfXpert = xxsphXpert[1,-1]
    latfXpert = xxsphXpert[2,-1]
    
    dlondX = (lonfXpert - lonfCenter) / pert
    dlatdX = (latfXpert - latfCenter) / pert
    
    print('x-perturbed solution:')
    print('(lon, lat) = ({0:.3f}, {1:.3f}) deg'\
          .format(np.degrees(lonfXpert), np.degrees(latfXpert)))
    
        
        
    # perturb y-direction
    print('perturbing in y-direction...')
    v0vecYpert = v0vecCenter + pert * ydir
    xx0vecYpert = np.block([x0vecCenter, v0vecYpert]).flatten()
    
    shield.y0 = xx0vecYpert
    solYpert = sim.runCartesian(mars, shield, params)
    xxsphYpert = conversions.cartI2sphPR(solYpert.y, mars, solYpert.t)
    lonfYpert = xxsphYpert[1,-1]
    latfYpert = xxsphYpert[2,-1]
    
    dlondY = (lonfYpert - lonfCenter) / pert
    dlatdY = (latfYpert - latfCenter) / pert
    
    print('y-perturbed solution:')
    print('(lon, lat) = ({0:.3f}, {1:.3f}) deg'\
          .format(np.degrees(lonfYpert), np.degrees(latfYpert)))
    
        
        
    # perturb z-direction
    print('perturbing in z-direction...')
    v0vecZpert = v0vecCenter + pert * zdir
    xx0vecZpert = np.block([x0vecCenter, v0vecZpert]).flatten()
    
    shield.y0 = xx0vecZpert
    solZpert = sim.runCartesian(mars, shield, params)
    xxsphZpert = conversions.cartI2sphPR(solZpert.y, mars, solZpert.t)
    lonfZpert = xxsphZpert[1,-1]
    latfZpert = xxsphZpert[2,-1]
    
    dlondZ = (lonfZpert - lonfCenter) / pert
    dlatdZ = (latfZpert - latfCenter) / pert
    
    print('z-perturbed solution:')
    print('(lon, lat) = ({0:.3f}, {1:.3f}) deg'\
          .format(np.degrees(lonfZpert), np.degrees(latfZpert)))
    
    
    J = np.array([[dlondX, dlondY, dlondZ],
                  [dlatdX, dlatdY, dlatdZ]])
    
    ax.plot(pertExp, dlondX, 'C0.')
    ax.plot(pertExp, dlondY, 'C1.')
    ax.plot(pertExp, dlondZ, 'C3.')
    ax.plot(pertExp, dlatdX, 'C3.')
    ax.plot(pertExp, dlatdY, 'C4.')
    ax.plot(pertExp, dlatdZ, 'C5.')


# #%%
# # =============================================================================
# # Target new (lon,lat) and test
# # =============================================================================
# lonfTarget = lonfCenter + np.radians(0.001)
# latfTarget = latfCenter + np.radians(0.001)
# deltaSurface = np.array([lonfTarget, latfTarget])
# deltaV = J.T @ np.linalg.inv(J @ J.T) @ deltaSurface

# print('\nsimulating retargeted vehicle...')
# print('delta V:')
# print(deltaV)
# v0vec1 = v0vecCenter + deltaV
# xx0vec1 = np.block([x0vecCenter, v0vec1]).flatten()

# shield.y0 = xx0vec1
# sol1 = sim.runCartesian(mars, shield, params)
# xxsph1 = conversions.cartI2sphPR(sol1.y, mars, sol1.t)
# lonf1 = xxsph1[1,-1]
# latf1 = xxsph1[2,-1]

# print('\nsolution 1:')
# print('(lon, lat) = ({0:.3f}, {1:.3f}) deg'\
#       .format(np.degrees(lonf1), np.degrees(latf1)))


# fig = plt.figure()
# ax = fig.add_subplot(111)
# # ax.plot(xxsph1[3,:]/1e3, (xxsph1[0,:] - mars.rad)/1e3)
# ax.plot(sol1.t, (xxsph1[0,:] - mars.rad)/1e3)
# ax.grid()
# ax.set_ylabel('altitude, km')










