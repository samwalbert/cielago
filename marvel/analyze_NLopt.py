# -*- coding: utf-8 -*-
"""
analyze_NLopt.py:
    analyze results of nonlinear network targeting
Created on Sun Jul 17 18:02:43 2022

@author: Samuel Albert
"""

import sys
sys.path.append('..')

import numpy as np
import matplotlib.pyplot as plt

from src import planetaryConstants, conversions


#%% Setup
plt.close('all')
plt.rcParams.update({'font.size': 16, 'lines.linewidth': 2, 'lines.markersize': 10})

mars = planetaryConstants.MARS

# define angle-distance mapping
def ang2dist(ang):
    return np.radians(ang) * mars.rad / 1e3

def dist2ang(dist):
    return np.degrees(dist * 1e3 / mars.rad)

# dat = np.load('../results/NL_18_10_lon_0717203307.npz', allow_pickle = True)
# resList = dat['resList']
# solList = dat['solList']
# offsetList = dat['offsetList']

#%%
# =============================================================================
# Compare across EFPAs, 3 days out, longitudinal (along-track)
# =============================================================================
fig1, ax1 = plt.subplots()
ax1.set_xlabel('separation, deg')
ax1.set_ylabel(r'normalized jettison speed, $V_j/V_{j,max}$')
secax1 = ax1.secondary_xaxis('top', functions = (ang2dist, dist2ang))
secax1.set_xlabel('distance, km')
ax1.grid()

fnamelist = ['../results/NL_12_3_lon.npz',
             '../results/NL_18_3_lon.npz',
             '../results/NL_24_3_lon.npz']

gam0list = [12, 18, 24]
for i, gam0 in enumerate(gam0list):
    # load data
    # dat = np.load('../results/NL_' + str(gam0) + '_3_lon.npz',
    #               allow_pickle = True)
    dat = np.load(fnamelist[i], allow_pickle = True)
    resList = dat['resList']
    solList = dat['solList']
    offsetList = dat['offsetList']
    DVLVLHList = dat['DVLVLHList']
    lonfCenter = dat['lonfCenter']
    
    # get DV magnitudes
    DVmags = np.asarray([np.linalg.norm(DV) for DV in DVLVLHList])
    
    # get actual longitudes
    lons = []
    for sol in solList:
        xxfsph = conversions.cartI2sphPR(sol.y[:,-1], mars, sol.t[-1])
        lons.append(np.degrees(xxfsph[1] - lonfCenter))

    # plot
    ax1.plot(lons, DVmags/DVmags[-1], '.--',
             label = r'$\gamma_0 = -' + str(gam0) +\
                 '\degree$, $V_{{j,max}} = {0:.0f}$ cm/s'.format(DVmags[-1]*100),
             markersize = 10)

ax1.legend()
plt.tight_layout()

#%%
# =============================================================================
# Compare across EFPAs, 3 days out, latitudinal (cross-track)
# =============================================================================
fig2, ax2 = plt.subplots()
ax2.set_xlabel('separation, deg')
ax2.set_ylabel('jettison speed, m/s')
secax2 = ax2.secondary_xaxis('top', functions = (ang2dist, dist2ang))
secax2.set_xlabel('distance, km')
ax2.grid()

fnamelist = ['../results/NL_12_3_lat.npz',
             '../results/NL_18_3_lat.npz',
             '../results/NL_24_3_lat.npz']

gam0list = [12, 18, 24]
for i, gam0 in enumerate(gam0list):
    # load data
    # dat = np.load('../results/NL_' + str(gam0) + '_3_lat.npz',
    #               allow_pickle = True)
    dat = np.load(fnamelist[i], allow_pickle = True)
    resList = dat['resList']
    solList = dat['solList']
    offsetList = dat['offsetList']
    DVLVLHList = dat['DVLVLHList']
    latfCenter = dat['latfCenter']
    
    # get DV magnitudes
    DVmags = np.asarray([np.linalg.norm(DV) for DV in DVLVLHList])
    
    # get actual latitudes
    lats = []
    for sol in solList:
        xxfsph = conversions.cartI2sphPR(sol.y[:,-1], mars, sol.t[-1])
        lats.append(np.degrees(xxfsph[2] - latfCenter))

    # plot
    # ax2.plot(offsetList, DVmags, '.--',
    #          label = r'$\gamma_0 = -' + str(gam0) +\
    #              '\degree$, $|V_j|_{{max}} = {0:.0f}$ m/s'.format(DVmags[-1]),
    #          markersize = 10)
    ax2.plot(lats, DVmags, '.--',
             label = r'$\gamma_0 = -' + str(gam0) +'\degree$', markersize = 10)

ax2.legend()
plt.tight_layout()

#%%
# =============================================================================
# Plot trajectories, for 18 deg longitudinal offset
# =============================================================================
dat = np.load('../results/NL_18_3_lon.npz', allow_pickle = True)
resList = dat['resList']
solList = dat['solList']
offsetList = dat['offsetList']
DVLVLHList = dat['DVLVLHList']
lonfCenter = dat['lonfCenter']

ratm = mars.rad + mars.halt
rsurf = mars.rad

fig3, ax3 = plt.subplots()
circ1 = plt.Circle((0,0), ratm/1e3, edgecolor = 'k', linestyle = '--',
                    facecolor = 'none', label = 'surface', zorder = 4)
ax3.add_patch(circ1)
circ2 = plt.Circle((0,0), rsurf/1e3, edgecolor = 'k',
                    facecolor = 'k', alpha = 0.5, label = 'surface')
ax3.add_patch(circ2)

cm = plt.get_cmap('gist_rainbow')
NUM_COLORS = len(solList) # about 50
ax3.set_prop_cycle(color = [cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])

for sol in np.flip(solList):
    ax3.plot(sol.y[0,:]/1e3, sol.y[1,:]/1e3, lw = 3)
ax3.grid()
ax3.axis('equal')
bound = 4000
plt.xlim([-bound, bound])
plt.ylim([-bound, bound])
plt.xlabel('x, km')
plt.ylabel('y, km')
plt.tight_layout()


fig33, ax33 = plt.subplots()
ax33.set_prop_cycle(color = [cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
ax33.set_xlabel('downrange, deg')
ax33.set_ylabel('altitude, km')
for sol in np.flip(solList):
    xxsph = conversions.cartI2sphPR(sol.y, mars, sol.t)
    # ax33.plot(xxsph[3,:]/1e3, (xxsph[0,:] - mars.rad)/1e3, lw = 4)
    ax33.plot(np.degrees(xxsph[1,:]-lonfCenter), (xxsph[0,:] - mars.rad)/1e3, lw = 2)
plt.axhline(125, color = 'r', linestyle = '--', linewidth = 2, label = 'atm interface')
secax33 = ax33.secondary_xaxis('top', functions = (ang2dist, dist2ang))
secax33.set_xlabel('downrange, km')
plt.xlim([-10, 200])
plt.ylim([-5, 275])
ax33.grid()
ax33.legend()
plt.tight_layout()

#%% check EFPAs
for sol in solList:
    xxsph = conversions.cartI2sphPR(sol.y, mars, sol.t)
    kentry = np.abs(xxsph[0,:] - (mars.rad + mars.halt)).argmin()
    efpa = xxsph[4,kentry]
    print(np.degrees(efpa))


#%%
# # =============================================================================
# # Compare different jettison times, 18 deg EFPA 30 deg longitudinal
# # =============================================================================
# dat = np.load('../results/NL_timer_18_30_0.npz', allow_pickle = True)
# resList = dat['resList']
# DVLVLHList = dat['DVLVLHList']
# dayslist = dat['dayslist']

# # get DV magnitudes
# DVmags = np.asarray([np.linalg.norm(DV)*1e3 for DV in DVLVLHList])

# fig4, ax4 = plt.subplots()
# ax4.plot(dayslist, DVmags, '.--', markersize = 10)

# ax4.grid()
# ax4.set_xlabel('jettison time, days')
# ax4.set_ylabel('jettison speed, m/s')
# plt.tight_layout()

#%%
# =============================================================================
# Compare different jettison times, 18 deg EFPA 5 deg latitudinal
# =============================================================================
dat = np.load('../results/NL_timer_18_0_5.npz', allow_pickle = True)
resList = dat['resList']
DVLVLHList = dat['DVLVLHList']
dayslist = dat['dayslist']

# get DV magnitudes
DVmags = np.asarray([np.linalg.norm(DV) for DV in DVLVLHList])

fig5, ax5 = plt.subplots()
ax5.plot(dayslist, DVmags, '.--', markersize = 10)

ax5.grid()
ax5.set_xlabel('jettison time, days')
ax5.set_ylabel('jettison speed, m/s')
plt.tight_layout()

#%%
# =============================================================================
# Compare timing, 18 deg EFPA latitudinal, 3 vs. 18 days out
# =============================================================================
fig6, ax6 = plt.subplots()
ax6.set_xlabel('separation, deg')
ax6.set_ylabel('jettison speed, m/s')
secax6 = ax6.secondary_xaxis('top', functions = (ang2dist, dist2ang))
secax6.set_xlabel('distance, km')
ax6.grid()

# load data
dat = np.load('../results/NL_18_3_lat.npz', allow_pickle = True)
resList = dat['resList']
solList = dat['solList']
offsetList = dat['offsetList']
DVLVLHList = dat['DVLVLHList']
latfCenter = dat['latfCenter']

# get DV magnitudes
DVmags = np.asarray([np.linalg.norm(DV) for DV in DVLVLHList])

# get actual latitudes
lats = []
for sol in solList:
    xxfsph = conversions.cartI2sphPR(sol.y[:,-1], mars, sol.t[-1])
    lats.append(np.degrees(xxfsph[2] - latfCenter))

# plot
ax6.plot(lats, DVmags, 'C1.--',
         label = 'E-3 days', markersize = 10)

# # load data
# dat = np.load('../results/NL_18_15_lat_0724103039.npz', allow_pickle = True)
# resList = dat['resList']
# solList = dat['solList']
# offsetListRaw = dat['offsetList']
# DVLVLHListRaw = dat['DVLVLHList']

# # filter data
# checklist = [5, 10, 15, 30, 45, 60, 75, 90]
# offsetList = np.empty(len(checklist))
# DVLVLHList = np.empty((len(checklist), 3))
# ci = 0
# for i, offset in enumerate(offsetListRaw):
#     if offset in checklist:
#         offsetList[ci] = offset
#         DVLVLHList[ci,:] = DVLVLHListRaw[i,:]
#         ci += 1

# # get DV magnitudes
# DVmags = np.asarray([np.linalg.norm(DV) for DV in DVLVLHList])

# # plot
# ax6.plot(offsetList, DVmags, 'C4.--',
#          label = 'E-15 days', markersize = 10)

# load data
# dat = np.load('../results/NL_18_18_lat.npz', allow_pickle = True)
dat = np.load('../results/NL_18_18_lat.npz', allow_pickle = True)
resList = dat['resList']
solList = dat['solList']
offsetListRaw = dat['offsetList']
DVLVLHListRaw = dat['DVLVLHList']
latfCenter = dat['latfCenter']

# filter data
checklist = [5, 10, 15, 30, 45, 60, 75, 90]
offsetList = np.empty(len(checklist))
DVLVLHList = np.empty((len(checklist), 3))
ci = 0
for i, offset in enumerate(offsetListRaw):
    if offset in checklist:
        offsetList[ci] = offset
        DVLVLHList[ci,:] = DVLVLHListRaw[i,:]
        ci += 1

# get DV magnitudes
DVmags = np.asarray([np.linalg.norm(DV) for DV in DVLVLHList])

# get actual latitudes
lats = []
for sol in solList:
    xxfsph = conversions.cartI2sphPR(sol.y[:,-1], mars, sol.t[-1])
    lats.append(np.degrees(xxfsph[2] - latfCenter))

# plot
ax6.plot(offsetList, DVmags, 'C4.--',
         label = 'E-18 days', markersize = 10)

ax6.legend()
plt.tight_layout()