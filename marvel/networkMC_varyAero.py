# -*- coding: utf-8 -*-
"""
networkMC_varyAero.py:
    Monte Carlo analysis for delivery of a network to the Martian surface.
    Incorporates updated varying aerodynamics model.
Created on Mon Nov 29 12:08:45 2021

@author: Samuel
"""

import sys
sys.path.append('..')

from src import sim
from src import planetaryConstants
from src import atmosphere
from src import conversions
from src import marvel

import time
import numpy as np
from scipy import stats
import datetime
import matplotlib.pyplot as plt

plt.close('all')
datestring = datetime.datetime.now().strftime('%m%d%H%M%S')

#%%
# =============================================================================
# Set up Planet, Vehicle, Params basics
# =============================================================================
mars = planetaryConstants.MARS
atmfilename = '../data/MarsGRAMNominal.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)
mars.MachFun = lambda r, u: u / np.interp((r - mars.rad), mars.sound[0,:],
                                          mars.sound[1,:])
mars.atmBaseFilename = '../data/MarsGRAM_MC10000/'

shield = sim.Vehicle('SHIELD')
marvel.loadSHIELDAeroData(shield)
shield.scaleAero = True
shield.LDscale = 1
shield.BCscale = 1
shield.getAero = marvel.getSHIELDAeroFun(shield, 'entry')
shield.Rn = 0.85 # m

# must define bank function, but it doesn't matter since L/D = 0
shield.bankFun = lambda t, yy: 0

# initial state
r0 = (mars.rad + mars.halt) # m
lon0 = np.radians(0)
lat0 = np.radians(0)
u0 = 6e3 # m/s
gam0degmag = 18
gam0 = np.radians(-gam0degmag)
hda0 = np.radians(90) # due-East
y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])

# define parameters for config changes
g0 = 1 # m/s^2
if gam0degmag == 12:
    tdeploy = 285
    tjettison = 295
if gam0degmag == 18:
    tdeploy = 170 # s
    tjettison = 180 # s
if gam0degmag == 24:
    tdeploy = 130
    tjettison = 140

params = sim.Params()
params.rtol = 1e-11
params.atol = 1e-11
params.rmin = mars.rad
params.rmax = (mars.rad + mars.halt)

# initial and final time
params.t0 = 7e6 # s
params.tf = params.t0 + 1e6 # s

shield.y0 = conversions.sphPR2cartI(y0sph, mars, params.t0)
shield.y0Center = conversions.sphPR2cartI(y0sph, mars, params.t0)

params.event1 = lambda t, y: sim.belowMinAltCart(t, y, params)
params.event1.terminal = True

params.event2 = lambda t, y: sim.aboveMaxAltCart(t, y, params)
params.event2.terminal = False
params.event2.direction = 1

params.pert = 1e-4


#%%
# =============================================================================
# Forward-propagate to get center landing site, with varying aero
# =============================================================================
# params.tspan = ((params.t0 - params.tback), params.tf)
params.tspan = (params.t0, params.tf)
solCenter, _ = marvel.runSHIELDtimed(mars, shield, tdeploy, tjettison, g0,
                                     tspan = params.tspan)

xxfvecCenter = solCenter.y[:,-1]
tfCenter = solCenter.t[-1]

# convert to spherical
xxfsphCenter = conversions.cartI2sphPR(solCenter.y[:,-1], mars,
                                      solCenter.t[-1])

lonfCenter = xxfsphCenter[1]
latfCenter = xxfsphCenter[2]

#%%
# =============================================================================
# Load in maneuvers - NOTE: this script takes DV vectors in LVLH frame
# =============================================================================
# data = np.load('../results/maneuvers_' + str(gam0degmag) + '.npz')
data = np.load('../results/maneuvers_large_18_alpha.npz')
DVvecs = data['DVvecs']
tbackList = data['tbackList']

#%%
# =============================================================================
# Set up dispersions
# =============================================================================
params.u0Mean = u0
params.u0STD = 2/3 # m/s
# params.u0STD = 0
params.gam0Mean = gam0
params.gam0STD = np.radians(0.2) / 3
# params.gam0STD = 0
params.DVloc = -0.1
params.DVscale = 0.2
params.BC_LB = -0.05 # +/- 5% uniform
params.BC_width = 0.1

#%%
# =============================================================================
# Monte Carlo trials
# =============================================================================
N = 500
Np = len(tbackList)
atmoffset = 500

lonfs = np.empty((N, Np))
lonfs[:] = np.NaN
latfs = np.empty((N, Np))
latfs[:] = np.NaN
qmaxs = np.empty((N, Np))
qmaxs[:] = np.NaN
vimps = np.empty((N, Np))
vimps[:] = np.NaN
trajs = np.empty((N, Np), dtype = object)

disperseDV = False

if disperseDV:
    DVpertList = stats.uniform.rvs(params.DVloc, params.DVscale, size = (N, Np))
    outname = '../results/networkMC_alpha' + str(gam0degmag) + '_N' + str(N) + '_' + datestring
else:
    outname = '../results/networkMC_alpha_N' + str(N) + '_NODVdisp_' + datestring

tic = time.time()

# generate dispersions
u0List = stats.norm.rvs(params.u0Mean, params.u0STD, size = N)
gam0List = stats.norm.rvs(params.gam0Mean, params.gam0STD, size = N)

for i in range(N):
    # pull atm profile
    aind = i + 1 + atmoffset
    mars.atmfilename = mars.atmBaseFilename + str(aind) + '.txt'
    mars.hList, mars.rhoList = atmosphere.readMarsGRAM(mars.atmfilename, True)
    # atmfilename = '../data/MarsGRAMNominal.txt'
    # mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
    mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)
    
    # build dispersed initial state
    u0 = u0List[i]
    gam0 = gam0List[i]
    y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])
    y0cart = conversions.sphPR2cartI(y0sph, mars, params.t0)
    
    # go through each tback value and propagate off the landers
    for j, (tback, DVvec) in enumerate(zip(tbackList, DVvecs)):        
        # back-propagate to jettison time
        shield.BCscale = 1
        shield.y0 = y0cart
        params.tspan = (params.t0, (params.t0 - tback))
        params.events = ()
        shield.getAero = marvel.getSHIELDAeroFun(shield, 'entry')
        solBack = sim.runCartesian(mars, shield, params)
        x0vecj = solBack.y[:3,-1]
        v0vecj = solBack.y[3:,-1]
        
        # convert DV from LVLH to inertial
        DVveck = DVvecs[j,:]
        rhat, thetahat, hhat = conversions.getLVLH(x0vecj, v0vecj)
        NO = np.block([[rhat], [thetahat], [hhat]]).T
        DVveck = NO @ DVveck
        
        if disperseDV:
            DVveck += DVveck * DVpertList[i,j]
        
        # apply delta-V
        v0veck = v0vecj + DVveck
        shield.y0 = np.block([x0vecj, v0veck]).flatten()
        
        # disperse aerodynamics
        shield.BCscale = stats.uniform.rvs(params.BC_LB, params.BC_width) + 1
        
        # propagate to surface
        params.tspan = (solBack.t[-1], params.tf)
        params.events = (params.event1, params.event2)
        solk, paramsk = marvel.runSHIELDtimed(mars, shield, tdeploy,
                                              tjettison, g0,
                                              tspan = params.tspan)
        
        # build and analyze traj
        trajk = sim.buildTrajCart(solk, mars)
        trajk = marvel.analyzeSHIELDTraj(mars, shield, paramsk, trajk)
        
        if solk.t[-1] == params.tf:
            trajk.lon[:] = np.NaN
            trajk.lat[:] = np.NaN
            trajk.missed = True
        else:
            trajk.missed = False
        
        # save off values
        lonfs[i,j] = trajk.lon[-1] # lonf, radians
        latfs[i,j] = trajk.lat[-1] # latf, radians
        trajs[i,j] = trajk        # full Traj object for this probe this trial
        
        print(np.degrees(trajk.lon[-1]), np.degrees(trajk.lat[-1]))
    
    if (i % 10 == 0) or (i == N-1):
        print('saving...')
        np.savez(outname,
                 lonfs = lonfs,
                 latfs = latfs,
                 lonfCenter = lonfCenter,
                 latfCenter = latfCenter,
                 trajs = trajs)
    
    toc = time.time()
    tstr = str(datetime.timedelta(seconds=(toc-tic)))
    print('run {0:d} complete, elapsed time: '.format(i+1) + tstr, '\n')

#%%
# =============================================================================
# Plots results
# =============================================================================
# img = plt.imread('../data/CF_CTX_screencap.png')
fig = plt.figure()
ax = fig.add_subplot(111)
# ax.imshow(img, extent=[160.1914394, 163.8111954, 8.2036983, 10.52815])
ax.grid()
ax.set_xlabel('longitude, deg')
ax.set_ylabel('latitude, deg')
# ax.set_aspect('equal', 'box')

def lon2dist(lon):
    return (np.radians(lon) - lonfCenter) * mars.rad / 1e3

def dist2lon(dist):
    return np.degrees((dist * 1e3 / mars.rad) + lonfCenter)

def lat2dist(lat):
    return (np.radians(lat) - latfCenter) * mars.rad / 1e3

def dist2lat(dist):
    return np.degrees((dist * 1e3 / mars.rad) + latfCenter)

secax = ax.secondary_xaxis('top', functions = (lon2dist, dist2lon))
secax.set_xlabel('distance, km')
secay = ax.secondary_yaxis('right', functions = (lat2dist, dist2lat))
secay.set_ylabel('distance, km')

ax.plot(np.degrees(lonfs.flatten()), np.degrees(latfs.flatten()), 'o')
            
    
    
















