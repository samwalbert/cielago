# -*- coding: utf-8 -*-
"""
SHIELD_NLopt.py:
    nonlinear targeting script for SHIELD probes, using variable aerodynamics
Created on Fri Jul 15 11:54:30 2022

@author: Samuel Albert
"""

import sys
sys.path.append('..')

from src import sim
from src import planetaryConstants
from src import atmosphere
from src import conversions
from src import marvel

import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize as opt
import datetime

datestring = datetime.datetime.now().strftime('%m%d%H%M%S')
plt.close('all')

#%%
# =============================================================================
# Set up Planet, Vehicle, Params
# =============================================================================
mars = planetaryConstants.MARS
atmfilename = '../data/MarsGRAMNominal.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)
mars.MachFun = lambda r, u: u / np.interp((r - mars.rad), mars.sound[0,:],
                                          mars.sound[1,:])

shield = sim.Vehicle('SHIELD')
marvel.loadSHIELDAeroData(shield)
shield.Rn = 0.85

# define Mach numbers for config changes
# Mdeploy = 1
# Mjettison = 0.75
Mdeploy = 0.7434939071247259
Mjettison = 0.5216789759142506

# must define bank function, but it doesn't matter since L/D = 0
shield.bankFun = lambda t, yy: 0

# initial state
t0 = 0 # s
r0 = (mars.rad + mars.halt) # m
lon0 = np.radians(0)
lat0 = np.radians(0)
u0 = 6e3 # m/s
gam0degmag = 18
gam0 = np.radians(-gam0degmag)
hda0 = np.radians(90) # due-East
y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])
shield.y0 = conversions.sphPR2cartI(y0sph, mars, t0)

params = sim.Params()
params.rtol = 1e-11
params.atol = 1e-11
params.rmin = mars.rad
params.rmax = (mars.rad + mars.halt)

# initial and final time
params.t0 = 7e6 # s
params.tf = params.t0 + 500 # s

shield.y0 = conversions.sphPR2cartI(y0sph, mars, params.t0)

params.event1 = lambda t, y: sim.belowMinAltCart(t, y, params)
params.event1.terminal = True

#%% define objective function
def jettison(DVvecLVLH, daysout, tolcap = True, final = False):    
    shield.getAero = marvel.getSHIELDAeroFun(shield, 'entry')
    
    shield.y0 = conversions.sphPR2cartI(y0sph, mars, params.t0)
    params.t0 = 7e6 # s
    params.tf = params.t0 + 500 # s
    params.tback = daysout * 24 * 60 * 60 # s, jettison time
    params.tspan = (params.t0, (params.t0 - params.tback))
    params.events = ()
    
    solBack = sim.runCartesian(mars, shield, params)
    xx0vecCenter = solBack.y[:,-1]
    
    # Forward-propagate to get center landing site, with varying aero
    shield.y0 = xx0vecCenter
    params.tspan = ((params.t0 - params.tback), params.tf)
    solCenter, _ = marvel.runSHIELDcartesian(mars, shield, Mdeploy, Mjettison, params.tspan)

    # xxfvecCenter = solCenter.y[:,-1]
    tfCenter = solCenter.t[-1]
    params.tf = tfCenter + 1e5
    params.tspan = ((params.t0 - params.tback), params.tf)

    # convert to spherical
    xxfsphCenter = conversions.cartI2sphPR(solCenter.y[:,-1], mars,
                                          solCenter.t[-1])

    x0vecCenter = xx0vecCenter[:3]
    v0vecCenter = xx0vecCenter[3:]
    lonfCenter = xxfsphCenter[1]
    latfCenter = xxfsphCenter[2]

    # compute LVLH unit vectors at jettison time (r, theta, h)
    rhat, thetahat, hhat = conversions.getLVLH(x0vecCenter, v0vecCenter)
    NO = np.block([[rhat], [thetahat], [hhat]]).T
    
    target = (lonfCenter + np.radians(offset[0]),
              latfCenter + np.radians(offset[1]))
    
    # convert LVLH to inertial
    DVvec = NO @ DVvecLVLH
    
    # apply DV
    v0vec = v0vecCenter + DVvec
    xx0vec = np.block([x0vecCenter, v0vec]).flatten()
    shield.y0 = xx0vec
    
    # propagate
    sol, paramsi = marvel.runSHIELDcartesian(mars, shield, Mdeploy, Mjettison, params.tspan)
    xxfvec = sol.y[:,-1]


    # convert target to inertial cartesian
    xxfTarget = conversions.sphPR2cartI(np.array([xxfsphCenter[0], target[0],
                                                  target[1], xxfsphCenter[3],
                                                  xxfsphCenter[4],
                                                  xxfsphCenter[5]]),
                                        mars, solCenter.t[-1])
    
    # compute error
    xxErr = xxfvec[:3] - xxfTarget[:3]
    errMag = np.linalg.norm(xxErr)
    DVmag = np.linalg.norm(DVvecLVLH)
    
    print('error: {0:.3f} km; DV: {1:.3e} m/s,  '.format(errMag/1e3, DVmag),
          DVvecLVLH)
    
    # if error is less than 1 km, round down to 0 error. this is necesssary
    #     because the tolerance setting on scipy.optimize.minimize is dumb.
    
    if tolcap and errMag < 2e3: # if error below 1 km,
        errMag = 0   # report 0 error
    
    if final:
        return errMag, DVvecLVLH, DVvec, sol, paramsi
    else:
        return errMag


#%% 
# =============================================================================
# optimize across desired daysout
# =============================================================================
# initialize
resList = []
solList = []
xxfsphList = []
DVinertialList = []
DVLVLHList = []
x0 = np.array([0, 0, 0])

# ## longitude maneuvers
# dayslist = [3]

# fname = '../results/maneuver_lon5.npz'
# offset = (5,0)
# x0 = np.array([9.8245950893205728e-04,  2.6657970803296055e-01,
#                3.1325665635401900e-03])

# fname = '../results/maneuver_lon10.npz'
# offset = (10,0)
# x0 = np.array([3.8430981522306687e-03,  4.5347811868568161e-01,
#                7.9572950203627875e-03])

# fname = '../results/maneuver_lon15.npz'
# offset = (15,0)
# x0 = np.array([-0.0010149874106669,  0.5754979342331376,  0.0012893320122255])

# fname = '../results/maneuver_lon30.npz'
# offset = (30,0)
# x0 = np.array([6.5018335764140383e-04,  6.7681815713924387e-01,
#                 5.0534708382132263e-03])

## latitude maneuvers
dayslist = [18]

# fname = '../results/maneuver_lat5.npz'
# offset = (0,5)
# x0 = np.array([ 0.1613572216749802, -0.0145454672437448,  0.3142425190380267])

# fname = '../results/maneuver_lat10.npz'
# offset = (0,10)
# x0 = np.array([ 0.0196391016645966, -0.0568025800227069,  0.6216056385351709])

# fname = '../results/maneuver_lat15.npz'
# offset = (0,15)
# x0 = np.array([-0.4801258351301237, -0.1273653730873761,  0.9282803675450663])

fname = '../results/maneuver_lat30.npz'
offset = (0,30)
x0 = np.array([-0.5101890568598914, -0.4982030086627693,  1.782739824416047 ])

# fname = '../results/maneuver_large_A.npz'
# offset = (30,0)
# dayslist = [5]
# x0 = np.array([ 0.007256341765666 ,  0.4082499073651222, -0.0069835762007683])

# fname = '../results/maneuver_large_B.npz'
# offset = (15,0)
# dayslist = [3]
# x0 = np.array([-0.0041729961302433,  0.5752077220828653, -0.0103286300398481])

# fname = '../results/maneuver_large_C.npz'
# offset = (0,5)
# dayslist = [14]
# x0 = np.array([-0.263555999850674 , -0.0285558464991036, -0.4015352297419394])

# fname = '../results/dummy.npz'
# offset = (15,0)
# dayslist = [50]
# x0 = np.array([ 0.02073582, -0.12308521, -0.00013674])

# set desired offset target
# offset = (30,0)
# offset = (0, 5)

# set timer list
# dayslist = [3,6,9,12,15,18]
# dayslist = [18,15,12,9,6,3]
# dayslist = [1,2,3,4,5,6]
# dayslist = [18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3]
# dayslist = [3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]
# dayslist = [12, 12.5, 13, 13.5, 14, 14.5, 15, 15.5, 16, 16.5, 17, 17.5, 18]
# dayslist = [7, 6.5, 6, 5.5, 5, 4.5, 4, 3.5, 3]
# dayslist = np.linspace(3,18,61)
# dayslist = [17,16,15,14,13,12,11,10,9,8,7,6,5,4,3]
# dayslist = [8,7.5,7]
# dayslist = [3,4,5,6,7,8,9]


# fname = '../results/NL_timer_' + str(gam0degmag) + '_' + str(offset[0]) + '_'\
#     + str(offset[1]) + '_' + datestring + '.npz'

# loop through desired timings
for daysout in dayslist:
    print('\n\nTARGETING: {0:.1f} days out, offset: '.format(daysout), offset)
    
    # # hard-coded initial guess
    # if (offset == (30, 0)):
        # if daysout == 3:
        #     x0 = np.array([-0.00140397,  0.67706844, -0.01078272])
    #     if daysout == 6:
    #         x0 = np.array([ 0.01612576, -0.34070821, -0.06088113])
    #     if daysout == 9:
    #         x0 = np.array([ 0.02734256, -0.22715997, -0.00243928])
    #     if daysout == 27:
    #         x0 = np.array([ 5.64876632e-02, -2.63468040e-02,  5.13237311e-05])
    # if (offset == (0, 5)):
    #     if daysout == 3:
    #         x0 = np.array([ 0.46403691, -0.08711993,  1.85705219])
    #     if daysout == 6:
    #         x0 = np.array([0.49177874, 0.05010023, -0.93816034])
    #     if daysout == 9:
    #         x0 = np.array([-0.30805015,  0.01362826, -0.62599633])
    #     # if daysout == 12:
    #     #     x0 = np.array([-2.40597694, -0.24859561, -0.47069232])
    #     if daysout == 27:
    #         x0 = np.array([-0.44749289, -0.94758745, -0.21120539])
    
    # hard-coded initial guess
    if (offset == (30, 0)):
        if daysout == 18:
            x0 = np.array([ 0.04111925, -0.10927921, -0.00015589])
        if daysout == 3:
            x0 = np.array([-0.00140397,  0.67706844, -0.01078272])
        if daysout == 4:
            x0 = np.array([ 0.00102411, -0.50931297, -0.01552649])
        # if daysout == 5:
        #     x0 = np.array([ 0.01076107, -0.40855649, -0.00814821])
    if (offset == (0,5)):
        if daysout == 18:
            x0 = np.array([-0.0744016036950769, -0.014460440290464 ,  0.3127344404779516])
        if daysout == 17:
            x0 = np.array([-0.0744392132230748, -0.015264017834841 ,  0.3313682540686534])
        if daysout == 16:
            x0 = np.array([-0.0671687314802231, -0.0163185470573713,  0.3520584730129209])
        if daysout == 15:
            x0 = np.array([-0.0672319324460519, -0.0173282992421819,  0.3750602844699867])
        if daysout == 14:
            x0 = np.array([-0.1019475733210366, -0.0186390735013222,  0.4021710041382134])
        if daysout == 13:
            x0 = np.array([-0.1018995662378384, -0.0196111291252884,  0.4301915747236466])
        if daysout == 12:
            x0 = np.array([-0.1017296336099531, -0.0207829872720093,  0.4673665092643141])
        if daysout == 11:
            x0 = np.array([-0.0971418991706499, -0.0234453752398199,  0.5135338594244618])
        if daysout == 10:
            x0 = np.array([ 0.1283375297396156, -0.0261060188989807,  0.563424131140104 ])
        if daysout == 9:
            x0 = np.array([ 0.0677283539932094, -0.0288257345081094,  0.6260904378158365])
        if daysout == 8:
            x0 = np.array([-0.2864104182630949, -0.031463118546212,   0.6992326815686625])
        if daysout == 7.5:
            x0 = np.array([-0.2863968692967606, -0.0326489766554569,  0.7474858913868   ])
        if daysout == 7:
            x0 = np.array([-0.286640557184459,  -0.0366612575076692,  0.8064950135806809])
        if daysout == 6:
            x0 = np.array([-0.366355133579792 , -0.0430815016573145,  0.9367990539335586])
        if daysout == 5:
            x0 = np.array([-0.4865353304557504, -0.0511427524409883,  1.1199856307660647])
        if daysout == 4:
            x0 = np.array([-0.6742037344845977, -0.0611534301716816,  1.394462918838868 ])
        if daysout == 3:
            x0 = np.array([ 0.23806684, -0.0859011 ,  1.85396519])
        
    
    # optimize
    jfun = lambda DVvecLVLH: jettison(DVvecLVLH, daysout)
    res = opt.minimize(jfun, x0, options = {'disp': True, 'return_all': True})
    print(res)
    
    #%%
    # run optimized solution
    errMag, DVvecLVLH, DVvec, sol, paramsk = jettison(res.x, daysout, True, True)
    xxfsph = conversions.cartI2sphPR(sol.y[:,-1], mars, sol.t[-1])
    np.set_printoptions(precision=16)
    print(np.degrees(xxfsph[1]), np.degrees(xxfsph[2]))
    
    # append to lists
    resList.append(res)
    DVLVLHList.append(DVvecLVLH)
    DVinertialList.append(DVvec)
    
    # save to file
    np.savez(fname,
              resList = resList,
              DVLVLHList = DVLVLHList,
              DVinertialList = DVinertialList,
              dayslist = dayslist,
              allow_pickle = True)
    
    # print result
    np.set_printoptions(precision=16)
    print(res.x)
    
    # update initial guess
    x0 = res.x



#%% plot
fig, ax = plt.subplots()
ax.plot(sol.y[0,:]/1e3, sol.y[1,:]/1e3, label = 'targeted')
# ax.plot(solCenter.y[0,:]/1e3, solCenter.y[1,:]/1e3, '--', label = 'center')
ratm = mars.rad + mars.halt
rsurf = mars.rad
circ1 = plt.Circle((0,0), ratm/1e3, edgecolor = 'k', linestyle = '--',
                    facecolor = 'none', label = 'atmosphere')
ax.add_patch(circ1)
circ2 = plt.Circle((0,0), rsurf/1e3, edgecolor = 'k',
                    facecolor = 'k', alpha = 0.5, label = 'surface')
ax.add_patch(circ2)
ax.add_patch(circ1)
ax.grid()
ax.legend()
plt.axis('equal')

traj = sim.buildTrajCart(sol, mars)
traj = marvel.analyzeSHIELDTraj(mars, shield, paramsk, traj)
k = (np.abs(traj.t - traj.tentry)).argmin()
efpa = np.degrees(traj.gam[k])
print(efpa)

# fig, ax = plt.subplots()
# ax.plot(sol.y[1,:]/1e3, sol.y[2,:]/1e3, label = 'targeted')
# ax.plot(solCenter.y[1,:]/1e3, solCenter.y[2,:]/1e3, '--', label = 'center')
# circ1 = plt.Circle((0,0), params.rmin/1e3, facecolor = None, label = 'surface')
# ax.add_patch(circ1)
# ax.grid()
# ax.legend()
# plt.axis('equal')