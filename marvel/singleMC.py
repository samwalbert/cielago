# -*- coding: utf-8 -*-
"""
singleMC.py:
    Monte Carlo analysis for a single SHIELD probe
Created on Tue Jul 19 13:56:58 2022

@author: Samuel Albert
"""

import sys
sys.path.append('..')

from src import sim, planetaryConstants, atmosphere, conversions, marvel

import dill
import time
import datetime
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt

plt.close('all')
date = datetime.datetime.now().strftime('%m%d%H%M%S')

#%%
# =============================================================================
# Set up Planet, Vehicle, Params
# =============================================================================
mars = planetaryConstants.MARS
atmfilename = '../data/MarsGRAMNominal.txt'
mars.atmBaseFilename = '../data/MarsGRAM_MC5000/'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)
mars.MachFun = lambda r, u: u / np.interp((r - mars.rad), mars.sound[0,:],
                                          mars.sound[1,:])

shield = sim.Vehicle('SHIELD')
marvel.loadSHIELDAeroData(shield)
shield.scaleAero = True
shield.LDscale = 1
shield.getAero = marvel.getSHIELDAeroFun(shield, 'entry')
shield.Rn = 0.85 # m

# must define bank function, but it doesn't matter since L/D = 0
shield.bankFun = lambda t, yy: 0

# initial state
t0 = 0 # s
r0 = (mars.rad + mars.halt) # m
lon0 = np.radians(0)
lat0 = np.radians(0)
u0 = 6e3 # m/s
gam0degmag = 18
gam0 = np.radians(-gam0degmag)
hda0 = np.radians(90) # due-East
y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])
shield.y0 = conversions.sphPR2cartI(y0sph, mars, t0)

# define parameters for config changes
g0 = 1 # Earth g's
if gam0degmag == 12:
    tdeploy = 225   # s, from entry
    tjettison = 235
if gam0degmag == 18:
    tdeploy = 140
    tjettison = 150
if gam0degmag >= 24:
    tdeploy = 105
    tjettison = 115

#%%
# =============================================================================
# Set up dispersions
# =============================================================================
ONLYATM = False

u0Mean = u0
u0STD = 2/3 # m/s
gam0Mean = gam0
gam0STD = np.radians(0.2) / 3
BC_LB = -0.05 # +/- 5% uniform
BC_width = 0.1

if ONLYATM:
    u0STD = 0
    gam0STD = 0
    BC_LB = 0
    BC_width = 0
    modstr = 'ONLYATM_'
else:
    modstr = ''
    
# modstr = 'divebomb_'
# gam0STD = 0
# u0STD = 0

#%%
# =============================================================================
# Monte Carlo trials
# =============================================================================
N = 1000
atmoffset = 0

# initialize
trajList = [None]*N
fname = '../results/singleMC_' + modstr + str(N) + '_' + str(gam0degmag)\
    + '_' + date + '.npz'
    

tic = time.time()

for i in range(N):
    # pull atm profile
    aind = i + 1 + atmoffset
    mars.atmfilename = mars.atmBaseFilename + str(aind) + '.txt'
    mars.hList, mars.rhoList = atmosphere.readMarsGRAM(mars.atmfilename, True)
    mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)
    
    # build dispersed initial state
    u0 = stats.norm.rvs(u0Mean, u0STD)
    gam0 = stats.norm.rvs(gam0Mean, gam0STD)
    y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])
    shield.y0 = conversions.sphPR2cartI(y0sph, mars, t0)
    shield.BCscale = stats.uniform.rvs(BC_LB, BC_width) + 1

    # Propagate
    # sol, params = marvel.runSHIELDcartesian(mars, shield, Mdeploy, Mjettison)
    sol, params = marvel.runSHIELDtimed(mars, shield, tdeploy, tjettison, g0)
    
    # build Traj object from results
    traj = sim.Traj()
    traj = sim.buildTrajCart(sol, mars, traj)
    
    # get properties along trajectory
    traj = marvel.analyzeSHIELDTraj(mars, shield, params, traj)
    
    # save off values
    trajList[i] = traj
    
    with open(fname, 'wb') as pfile:
        dill.dump(trajList, pfile)
    
    toc = time.time()
    tstr = str(datetime.timedelta(seconds=(toc-tic)))
    print('run {0:d} complete, elapsed time: '.format(i+1) + tstr)
    


#%% plots
fig1, ax1 = plt.subplots()

def lon2dist(lon):
    return (np.radians(lon)) * mars.rad / 1e3

def dist2lon(dist):
    return np.degrees((dist * 1e3 / mars.rad))

def lat2dist(lat):
    return (np.radians(lat)) * mars.rad / 1e3

def dist2lat(dist):
    return np.degrees((dist * 1e3 / mars.rad))

secax = ax1.secondary_xaxis('top', functions = (lon2dist, dist2lon))
secax.set_xlabel('distance, km')
secay = ax1.secondary_yaxis('right', functions = (lat2dist, dist2lat))
secay.set_ylabel('distance, km')

for traj in trajList:
    ax1.plot(np.degrees(traj.lon[-1]), np.degrees(traj.lat[-1]), '.')

ax1.grid()
plt.tight_layout()
    


