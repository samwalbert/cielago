# -*- coding: utf-8 -*-
"""
SHIELD_NLopt.py:
    nonlinear targeting script for SHIELD probes, using variable aerodynamics
Created on Fri Jul 15 11:54:30 2022

@author: Samuel Albert
"""

import sys
sys.path.append('..')

from src import sim
from src import planetaryConstants
from src import atmosphere
from src import conversions
from src import marvel

import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize as opt
import datetime

datestring = datetime.datetime.now().strftime('%m%d%H%M%S')
plt.close('all')

#%%
# =============================================================================
# Set up Planet, Vehicle, Params
# =============================================================================
mars = planetaryConstants.MARS
atmfilename = '../data/MarsGRAMNominal.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)
mars.MachFun = lambda r, u: u / np.interp((r - mars.rad), mars.sound[0,:],
                                          mars.sound[1,:])

shield = sim.Vehicle('SHIELD')
marvel.loadSHIELDAeroData(shield)

# define Mach numbers for config changes
Mdeploy = 1
Mjettison = 0.75

# must define bank function, but it doesn't matter since L/D = 0
shield.bankFun = lambda t, yy: 0

# initial state
t0 = 0 # s
r0 = (mars.rad + mars.halt) # m
lon0 = np.radians(0)
lat0 = np.radians(0)
u0 = 6e3 # m/s
gam0degmag = 18
gam0 = np.radians(-gam0degmag)
hda0 = np.radians(90) # due-East
y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])
shield.y0 = conversions.sphPR2cartI(y0sph, mars, t0)

params = sim.Params()
params.rtol = 1e-11
params.atol = 1e-11
params.rmin = mars.rad
params.rmax = (mars.rad + mars.halt)

# initial and final time
params.t0 = 7e6 # s
params.tf = params.t0 + 500 # s

shield.y0 = conversions.sphPR2cartI(y0sph, mars, params.t0)

params.event1 = lambda t, y: sim.belowMinAltCart(t, y, params)
params.event1.terminal = True

#%%
# =============================================================================
# Back-propagate to jettison time, with constant aero
# =============================================================================
shield.getAero = marvel.getSHIELDAeroFun(shield, 'entry')
daysout = 3
params.tback = daysout * 24 * 60 * 60 # s, jettison time

params.tspan = (params.t0, (params.t0 - params.tback))
params.events = ()

solBack = sim.runCartesian(mars, shield, params)
xx0vecCenter = solBack.y[:,-1]


#%%
# =============================================================================
# Forward-propagate to get center landing site, with varying aero
# =============================================================================
shield.y0 = xx0vecCenter
params.tspan = ((params.t0 - params.tback), params.tf)
solCenter, _ = marvel.runSHIELDcartesian(mars, shield, Mdeploy, Mjettison, params.tspan)

xxfvecCenter = solCenter.y[:,-1]
tfCenter = solCenter.t[-1]
params.tf = tfCenter + 1e5
params.tspan = ((params.t0 - params.tback), params.tf)

# convert to spherical
xxfsphCenter = conversions.cartI2sphPR(solCenter.y[:,-1], mars,
                                      solCenter.t[-1])

x0vecCenter = xx0vecCenter[:3]
v0vecCenter = xx0vecCenter[3:]
lonfCenter = xxfsphCenter[1]
latfCenter = xxfsphCenter[2]

# compute LVLH unit vectors at jettison time (r, theta, h)
rhat, thetahat, hhat = conversions.getLVLH(x0vecCenter, v0vecCenter)
NO = np.block([[rhat], [thetahat], [hhat]]).T


#%% define objective function
def jettison(DVvecLVLH, target, tolcap = True):
    '''
    target = (lon, lat) in radians
    '''
    
    # convert LVLH to inertial
    DVvec = NO @ DVvecLVLH
    
    # apply DV
    v0vec = v0vecCenter + DVvec
    xx0vec = np.block([x0vecCenter, v0vec]).flatten()
    shield.y0 = xx0vec
    
    # propagate
    sol, _ = marvel.runSHIELDcartesian(mars, shield, Mdeploy, Mjettison, params.tspan)
    xxfvec = sol.y[:,-1]


    # convert target to inertial cartesian
    xxfTarget = conversions.sphPR2cartI(np.array([xxfsphCenter[0], target[0],
                                                  target[1], xxfsphCenter[3],
                                                  xxfsphCenter[4],
                                                  xxfsphCenter[5]]),
                                        mars, solCenter.t[-1])
    
    # compute error
    xxErr = xxfvec - xxfTarget
    errMag = np.linalg.norm(xxErr)
    DVmag = np.linalg.norm(DVvecLVLH)
    
    np.set_printoptions(precision=16)
    print('error: {0:.3f} km; DV: {1:.3e} m/s,  '.format(errMag/1e3, DVmag),
          DVvecLVLH)
    print(xxfvec - xxfTarget)
    xxfsph = conversions.cartI2sphPR(xxfvec, mars, sol.t[-1])
    print(np.degrees(xxfsph[1]), np.degrees(xxfsph[2]))
    print()
    # print(errMag, DVvecLVLH)
    # # print(DVvecLVLH)
    # print()
    
    # if error is less than 1 km, round down to 0 error. this is necesssary
    #     because the tolerance setting on scipy.optimize.minimize is dumb.
    
    if tolcap and errMag < 2e3: # if error below 1 km,
        errMag = 0   # report 0 error
    
    return errMag


#%% 
# =============================================================================
# optimize across desired targets
# =============================================================================
# initialize
resList = []
solList = []
xxfsphList = []
DVinertialList = []
DVLVLHList = []
x0 = np.array([0, 0, 0])

# set target list
# offsetList = [5, 10, 15, 30, 60, 90, 120, 150, 180]
# offsetList = np.linspace(1,180,180)
axisOff = 'lon'
offsetList = [180]
# x0 = np.array([-0.00056478,  0.07622511, -0.0011712 ])

# offsetList = [5, 10, 15, 30, 45, 60, 75, 90]
# offsetList = [30,45]
# offsetList = [5, 10, 15, 20, 22, 24, 26, 28, 30, 45, 60, 75, 90]
# offsetList = [20, 22, 24, 26, 28, 30]
# offsetList = [1,2,3,4,5]
# offsetList = [5, 10, 15, 30]
# offsetList = np.linspace(5,90,171)
# axisOff = 'lat'
# x0 = np.array([ 4.71251922e-06, -1.37433599e-03,  1.82260913e-01])

# fname = '../results/NL_' + str(gam0degmag) + '_' + str(daysout) + '_'\
#     + axisOff + '_' + datestring + '.npz'
fname = '../results/dummy.npz'

# loop through each desired target
for offset in offsetList:
    print('\n\nTARGETING: {0:.1f} deg offset, '.format(offset) + axisOff)
    
    # set target
    if axisOff == 'lon':
        target = (lonfCenter + np.radians(offset), latfCenter)
    else:
        target = (lonfCenter, latfCenter + np.radians(offset))
        
    # hard-coded initial guesses
    if (axisOff == 'lat') and (gam0degmag == 12) and (daysout == 3):
        if (offset == 5):
            x0 = np.array([ 0.3029021140888882, -0.0986635067351634,  2.0351108079804385])
        if (offset == 10):
            x0 = np.array([-0.7116687769798705, -0.3897168410169396,  4.040508395010302 ])
        if (offset == 15):
            x0 = np.array([-0.30121754, -0.87185902,  5.99858263])
        if (offset == 30):
            x0 = np.array([-4.951813341127683 , -3.3444283449359804, 11.3838260285554   ])
    if (axisOff == 'lat') and (gam0degmag == 18) and (daysout == 3):
        if (offset == 5):
            x0 = np.array([ 0.23806684, -0.0859011 ,  1.85396519])
        if (offset == 10):
            x0 = np.array([-0.28319593, -0.33660767,  3.69475098])
        if (offset == 15):
            x0 = np.array([ 0.05645908, -0.75274684,  5.49167027])
        if (offset == 30):
            x0 = np.array([ 6.76449486275796  , -2.9641252557855027, 10.563343367391573 ])
    if (axisOff == 'lat') and (gam0degmag == 24) and (daysout == 3):
        if (offset == 5):
            x0 = np.array([ 0.23808795, -0.07808996,  1.77145235])
        if (offset == 10):
            x0 = np.array([-1.09080096, -0.30195012,  3.51644304])
        if (offset == 15):
            x0 = np.array([ 1.7997621798544865, -0.6946536844138687,  5.24309926248348  ])
        if (offset == 30):
            x0 = np.array([ 0.60146543, -2.71448654, 10.12996567])
    if (axisOff == 'lat') and (gam0degmag == 18) and (daysout == 10):
        if (offset == 5):
            x0 = np.array([-3.80343867e-01, -1.28487508e-05, -5.65364986e-01])
        if (offset == 10):
            x0 = np.array([ 1.06722031,  0.17243542, -1.1141016 ])
        if (offset == 15):
            x0 = np.array([-0.32492119,  0.2106529 , -1.67172379])
    if (axisOff == 'lon') and (gam0degmag == 18) and (daysout == 10):
        if (offset == 5):
            x0 = np.array([ 5.45532405e-03, -7.90932282e-02,  7.40895855e-05])
        if (offset == 10):
            x0 = np.array([ 0.01254546, -0.20904882, -0.00175061])
    if (axisOff == 'lat') and (gam0degmag == 18) and (daysout == 15):
        if offset == 5:
            x0 = np.array([ 0.5173381307391559, -0.0171899283327607,  0.3733522738354625])
        if offset == 10:
            x0 = np.array([ 0.1428850915482407, -0.0685742145089159,  0.7489567832926789])
        if offset == 15:
            x0 = np.array([ 0.9627947297491725, -0.1537076771687514,  1.1142005291166166])
        # if offset == 20:
        #     x0 = np.array([ 2.179932276891496 , -0.2704077306658857,  1.4677182389344008])
        if offset == 30:
            x0 = np.array([ 0.2830785544278595, -0.5973370908782726,  2.137096205233056 ])
        if offset == 45:
            x0 = np.array([-1.0236733405666476, -1.2915120507965137,  2.995066374085423 ])
        if offset == 60:
            x0 = np.array([-1.479025790090045, -2.178490580660841,  3.628720716097204])
        if offset == 75:
            x0 = np.array([-1.5699576240277322, -3.1871565179932455,  3.999642171917277 ])
        if offset == 90:
            x0 = np.array([-2.8884185993123865, -4.243247749502456 ,  4.0906633501229415])
            
    if (axisOff == 'lat') and (gam0degmag == 18) and (daysout == 18):
        if offset == 4:
            x0 = np.array([ 0.1270618527454109, -0.0096745326394685,  0.2514210292216906])
        if offset == 4.5:
            x0 = np.array([ 0.1270516928060372, -0.0104376654608953,  0.2824099325116055])
        if offset == 5:
            x0 = np.array([ 0.1613572216749802, -0.0145454672437448,  0.3142425190380267])
        if offset == 10:
            x0 = np.array([ 0.0196391016645966, -0.0568025800227069,  0.6216056385351709])
        if offset == 15:
            x0 = np.array([-0.4801258351301237, -0.1273653730873761,  0.9282803675450663])
    #     if offset == 20:
    #         x0 = np.array([-5.38179196, -1.86177367, -1.23027084])
    #     if offset == 25:
    #         x0 = np.array([-5.42594894, -1.74688653, -1.51408101])
        if offset == 30:
            x0 = np.array([-0.5101890568598914, -0.4982030086627693,  1.782739824416047 ])
    #     if offset == 31:
    #         x0 = np.array([-0.93935902,  0.23027439, -1.89146971])
    #     if offset == 32:
    #         x0 = np.array([-0.93935902,  0.23027439, -1.89146971])
    #     if offset == 33:
    #         x0 = np.array([-0.9503076 ,  0.25840619, -1.94546401])
    #     if offset == 34:
    #         x0 = np.array([-0.96181128,  0.28759327, -1.98953496])
    #     if offset == 35:
    #         x0 = np.array([-1.96401897, -0.0563146 , -2.05192889])
    #     if offset == 36:
    #         x0 = np.array([-1.97421968, -0.03018481, -2.08945406])
    #     if offset == 38:
    #         x0 = np.array([-1.70608695,  0.16226607, -2.19288157])
    #     if offset == 40:
    #         x0 = np.array([-1.73444902,  0.23399932, -2.28828522])
        if offset == 45:
            x0 = np.array([-0.5003951258630458, -1.0766980637328214,  2.4966136364809937])
        if offset == 60:
            x0 = np.array([ 1.798035141337484 , -1.8173135361970016,  3.0245935175300582])
        if offset == 75:
            x0 = np.array([-0.2778769783999381, -2.656630185933563 ,  3.3340921031376167])
        if offset == 90:
            x0 = np.array([-0.6393816740277691, -3.537764237082608 ,  3.410020671964197 ])
    if (axisOff == 'lon') and (gam0degmag == 18) and (daysout == 3):
        if offset == 5:
            x0 = np.array([ 9.8245950893205728e-04,  2.6657970803296055e-01,
                     3.1325665635401900e-03])
        if offset == 10:
            x0 = np.array([ 3.8430981522306687e-03,  4.5347811868568161e-01,
              7.9572950203627875e-03])
        if offset == 15:
            x0 = np.array([-1.0357354632583222e-03,  5.7396694939481196e-01,
              1.2423519712307159e-03])
        if offset == 30:
            x0 = np.array([ 6.5018335764140383e-04,  6.7681815713924387e-01,
              5.0534708382132263e-03])
        if offset == 60:
            x0 = np.array([ 9.6931665527937646e-04,  6.8475951028887860e-01,
              5.9705468479768192e-03])
        if offset == 90:
            x0 = np.array([ 9.9891598840278015e-04,  6.8547073269552505e-01,
              5.9889383628028677e-03])
        if offset == 120:
            x0 = np.array([ 1.0090979189344531e-03,  6.8574518957175290e-01,
              6.0164955992760795e-03])
        if offset == 150:
            x0 = np.array([0.001038387957945 , 0.6859167282619177, 0.0060192708311772])
        if offset == 180:
            x0 = np.array([0.0010276887231684, 0.6860503686526114, 0.0060045182997016])
    
    if (axisOff == 'lon') and (gam0degmag == 12) and (daysout == 3):
        if offset == 5:
            x0 = np.array([-0.0006106093014743,  0.0767936756245616, -0.0012008151368756])
        if offset == 10:
            x0 = np.array([ 0.0015862747015353,  0.1116569820949881,  0.0003317053860014])
        if offset == 15:
            x0 = np.array([ 0.0013146956538192,  0.1247319657424744,  0.000321577166619 ])
        if offset == 30:
            x0 = np.array([0.0009291390886986, 0.1327873018504749, 0.0006775485610158])
        if offset == 60:
            x0 = np.array([0.0008937900715439, 0.1347132709371865, 0.0006498524089015])
        if offset == 90:
            x0 = np.array([0.0008362510347867, 0.1351302543205513, 0.0005616679103912])
        if offset == 120:
            x0 = np.array([0.000781792739224 , 0.1353432282151066, 0.0006404324484751])
        if offset == 150:
            x0 = np.array([0.0008100056970955, 0.1354927809156429, 0.0005313933293892])
        if offset == 180:
            x0 = np.array([0.0008137809353292, 0.1356175300292223, 0.0005343755575991])
            
    if (axisOff == 'lon') and (gam0degmag == 24) and (daysout == 3):
        if offset == 5:
            x0 = np.array([ 1.7241334156937391e-03,  4.2496837200131338e-01,
                     9.4408854326725805e-04])
        if offset == 10:
            x0 = np.array([-0.0018061557460938,  0.7683249218674052, -0.0021579967371077])
        if offset == 15:
            x0 = np.array([-8.0798735934519209e-04,  1.0333726911913197e+00,
             -2.0988608511725683e-04])
        if offset == 30:
            x0 = np.array([0.0023162940013973, 1.4057357119362957, 0.0061097695662274])
        if offset == 60:
            x0 = np.array([0.0036302623370687, 1.4507574972498953, 0.007184596679944 ])
        if offset == 90:
            x0 = np.array([ 0.022630937374545 ,  1.4519542193296802, -0.0062217695016571])
        if offset == 120:
            x0 = np.array([ 0.0226791238915344,  1.4523026727665038, -0.0061765208564994])
        if offset == 150:
            x0 = np.array([-0.0255971954929227,  1.4525988522220945,  0.0064998805036021])
        if offset == 180:
            x0 = np.array([-0.0064769306423927,  1.4527024811012772, -0.0047078726140101])
        
    
    # optimize
    jfun = lambda DVvecLVLH: jettison(DVvecLVLH, target)
    res = opt.minimize(jfun, x0, options = {'disp': True, 'return_all': True})
    np.set_printoptions(precision=16)
    print(res)
    
    # run optimized solution
    # apply DV
    DVvec = NO @ res.x
    v0vec = v0vecCenter + DVvec
    xx0vec = np.block([x0vecCenter, v0vec]).flatten()
    shield.y0 = xx0vec

    # propagate
    sol, _ = marvel.runSHIELDcartesian(mars, shield, Mdeploy, Mjettison, params.tspan)
    xxfvec = sol.y[:,-1]
    xxfsph = conversions.cartI2sphPR(sol.y[:,-1], mars, sol.t[-1])
    
    # append to lists
    resList.append(res)
    solList.append(sol)
    xxfsphList.append(xxfsph)
    DVinertialList.append(DVvec)
    DVLVLHList.append(res.x)
    
    # save to file
    np.savez(fname,
             resList = resList,
             solList = solList,
             xxfsphList = xxfsphList,
             DVinertialList = DVinertialList,
             DVLVLHList = DVLVLHList,
             offsetList = offsetList,
             lonfCenter = lonfCenter,
             latfCenter = latfCenter,
             allow_pickle = True)
    
    # update initial guess
    x0 = res.x



# #%% plot
# fig, ax = plt.subplots()
# ax.plot(sol.y[0,:]/1e3, sol.y[1,:]/1e3, label = 'targeted')
# ax.plot(solCenter.y[0,:]/1e3, solCenter.y[1,:]/1e3, '--', label = 'center')
# circ1 = plt.Circle((0,0), params.rmin/1e3, facecolor = None, label = 'surface')
# ax.add_patch(circ1)
# ax.grid()
# ax.legend()
# plt.axis('equal')

# fig, ax = plt.subplots()
# ax.plot(sol.y[1,:]/1e3, sol.y[2,:]/1e3, label = 'targeted')
# ax.plot(solCenter.y[1,:]/1e3, solCenter.y[2,:]/1e3, '--', label = 'center')
# circ1 = plt.Circle((0,0), params.rmin/1e3, facecolor = None, label = 'surface')
# ax.add_patch(circ1)
# ax.grid()
# ax.legend()
# plt.axis('equal')