# -*- coding: utf-8 -*-
"""
testMarvel.py:
    tests probe targeting-related functions
Created on Fri Nov 26 15:08:12 2021

@author: Samuel
"""

import sys
sys.path.append('..')

from src import sim
from src import planetaryConstants
from src import atmosphere
from src import conversions
from src import marvel

import numpy as np
import matplotlib.pyplot as plt

# plt.close('all')
plt.rcParams.update({'font.size': 20})

dueEast = True
N = 16

if dueEast:
    hda0 = np.radians(90)
    label = 'due-East'
    plt.close('all')
    fig1 = plt.figure()
    fig2 = plt.figure()
    ax1 = fig1.add_subplot(111)
    ax2 = fig2.add_subplot(111)
    ax1.grid()
    ax1.set_xlabel('longitude offset, deg')
    ax1.set_ylabel('latitude offset, deg')
    ax2.grid()
    ax2.set_xlabel('angle, deg')
    ax2.set_ylabel(r'$V_j$, m/s')
else:
    hda0 = 0
    label = 'due-North'
    
    

#%%
# =============================================================================
# Set up Planet, Vehicle, Params
# =============================================================================
mars = planetaryConstants.MARS
atmfilename = '../data/MarsGRAMNominal.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)

shield = sim.Vehicle('SHIELD')
shield.BC = 10 # kg/m^2
shield.LD = 0
shield.getAero = lambda r, u, Planet: (shield.BC, shield.LD)

# must define bank function, but it doesn't matter since L/D = 0
shield.bankFun = lambda t, yy: 0

# initial state
r0 = (mars.rad + mars.halt) # m
lon0 = np.radians(0)
lat0 = np.radians(0)
u0 = 6e3 # m/s
gam0 = np.radians(-12)
# hda0 = np.radians(90)
y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])

params = sim.Params()
params.rtol = 1e-11
params.atol = 1e-11
params.rmin = mars.rad
params.rmax = (mars.rad + mars.halt)

# initial and final time
params.t0 = 1e6 # s
params.tf = params.t0 + 5e3 # s

shield.y0 = conversions.sphPR2cartI(y0sph, mars, params.t0)

params.event1 = lambda t, y: sim.belowMinAltCart(t, y, params)
params.event1.terminal = True

params.event2 = lambda t, y: sim.aboveMaxAltCart(t, y, params)
params.event2.terminal = True
params.event2.direction = 1

#%%
# =============================================================================
# Get center states at sep time and at surface
# =============================================================================
params.tback = 1 * 24 * 60 * 60 # s
xx0vecCenter, xxfsphCenter = marvel.getCenterState(mars, shield, params)
x0vecCenter = xx0vecCenter[:3]
v0vecCenter = xx0vecCenter[3:]
lonfCenter = xxfsphCenter[1]
latfCenter = xxfsphCenter[2]

#%%
# =============================================================================
# Get Jacobian
# =============================================================================
params.pert = 1e-4
J = marvel.getJ(mars, shield, params, xx0vecCenter, xxfsphCenter)


#%%
# =============================================================================
# Target N point along a circle, report delta-V for each
# =============================================================================
r = 0.5
dlons90 = np.asarray([np.cos(2*np.pi/N*x)*r for x in range(0,N)])
dlats90 = np.asarray([np.sin(2*np.pi/N*x)*r for x in range(0,N)])

dlons = dlons90 * np.sin(hda0) - dlats90 * np.cos(hda0)
dlats = dlats90 * np.sin(hda0) + dlons90 * np.cos(hda0)

# points = [(np.cos(2*np.pi/N*x)*r, np.sin(2*np.pi/N*x)*r) for x in range(0,N)]
angles = [np.degrees(2*np.pi/N*x) for x in range(0, N)]
DVvecList = []
DVList = []
lonfList = []
latfList = []
lonErrList = []
latErrList = []


for dlon, dlat in zip(dlons, dlats):
    
    DVvec, (lonf, latf), (lonErr, latErr) = \
        marvel.targetLonLat(mars, shield, params, xx0vecCenter, xxfsphCenter,
                            J, np.radians(dlon), np.radians(dlat))
    
    # record
    DVvecList.append(DVvec)
    DVList.append(np.linalg.norm(DVvec))
    lonfList.append(lonf)
    latfList.append(latf)
    lonErrList.append(lonErr)
    latErrList.append(latErr)
    
    # print
    print()
    print('({0:.3f}, {1:.3f}) deg'.format(dlon, dlat))
    print('longitude error: {0:.3e} deg'.format(np.degrees(lonErr)))
    print('latitude error: {0:.3e} deg'.format(np.degrees(latErr)))


#%%
# fig = plt.figure()
# ax = fig1.add_subplot(111)
# ax1.plot(dlons, dlats, 'C2o', label = 'target sites', markersize = 12)
ax1.plot(np.degrees(lonfList - lonfCenter),
         np.degrees(latfList - latfCenter), 'C1^', label = label,
         markersize = 12)
ax1.legend()


# fig = plt.figure()
# ax = fig2.add_subplot(111)
ax2.plot(angles, DVList, 'C1^', label = label, markersize = 12)
ax2.legend()



