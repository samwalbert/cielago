# -*- coding: utf-8 -*-
"""
analyzeNetworkMC.py:
    combine and analyze results of Monte Carlo analysis
Created on Tue Nov 30 13:02:33 2021

@author: Samuel
"""

from src import conversions, planetaryConstants

import pickle
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 16, 'lines.linewidth': 2, 'lines.markersize': 10})

#%%
# # =============================================================================
# # Combine data files into one
# # =============================================================================
# fnames = []

# fnames.append('../results/networkMC_alpha_N500_NODVdisp_0826074516.npz')
# fnames.append('../results/networkMC_alpha_N500_NODVdisp_0826074528.npz')


# lonfsList = []
# latfsList = []
# trajList = []

# for fname in fnames:
#     data = np.load(fname, allow_pickle = True)
#     lonfsList.append(data['lonfs'])
#     latfsList.append(data['latfs'])
#     lonfCenter = data['lonfCenter']
#     latfCenter = data['latfCenter']
#     trajList.append(data['trajs'])

# fname = '../results/networkMC_alpha_1000_NODVdisp_new.npz'
# np.savez(fname,
#           lonfsList = lonfsList,
#           latfsList = latfsList,
#           lonfCenter = lonfCenter,
#           latfCenter = latfCenter,
#           trajList = trajList)

#%% combine two compiled data files
# dat1 = np.load('../results/networkMC_alpha_1000.npz', allow_pickle = True)
# dat2 = np.load('../results/networkMC_alpha_1000_2.npz', allow_pickle = True)

# lonfs1 = np.concatenate(dat1['lonfsList'], axis = 0)
# lonfs2 = np.concatenate(dat2['lonfsList'], axis = 0)
# lonfs = np.append(lonfs1, lonfs2, axis = 0)
# latfs1 = np.concatenate(dat1['latfsList'], axis = 0)
# latfs2 = np.concatenate(dat2['latfsList'], axis = 0)
# latfs = np.append(latfs1, latfs2, axis = 0)
# trajs1 = np.concatenate(dat1['trajList'], axis = 0)
# trajs2 = np.concatenate(dat2['trajList'], axis = 0)
# trajs = np.append(trajs1, trajs2, axis = 0)

# fname = '../results/networkMC_alpha_2000.npz'
# np.savez(fname,
#           lonfs = lonfs,
#           latfs = latfs,
#           lonfCenter = lonfCenter,
#           latfCenter = latfCenter,
#           trajs = trajs)
# preprocessed = True

#%%
# =============================================================================
# Load and process data
# =============================================================================
preprocessed = False
fname = '../results/networkMC_alpha_1000_new.npz'
# fname = '../results/networkMC_alpha_1000_NODVdisp_new.npz'
data = np.load(fname, allow_pickle = True)
lonfCenter = data['lonfCenter']
latfCenter = data['latfCenter']

if preprocessed:
    lonfs = data['lonfs']
    latfs = data['latfs']
    trajs = data['trajs']
else:
    lonfsList = data['lonfsList']
    latfsList = data['latfsList']
    trajList = data['trajList']
    
    # below are shape (i, j, k) where i = number of trials, j = number of unique
    #   targets, and k = 2 to contain reciprocal target pairs (i.e. (lon,-lon))
    lonfs = np.concatenate(lonfsList, axis = 0)
    latfs = np.concatenate(latfsList, axis = 0)
    trajs = np.concatenate(trajList, axis = 0)


#%%
# =============================================================================
# Set up desired network geometry
# =============================================================================
# hard-code for network alpha
dlonList = np.radians(np.array([11.22851011565675,
                                15.913621162873783,
                                20.5320478050663,
                                32.03372359113943,
                                6.6149446919144586,
                                6.4733843996581975,
                                7.330588737548735,
                                7.439861647465777]))

dlatList = np.radians(np.array([0.00817695485883306,
                                0.019913451783904922,
                                0.0031245863987390695,
                                -0.022997639558262128,
                                4.997103255917885,
                                9.976563234483319,
                                15.008455033240374,
                                30.015436233758038]))

#%% Process data
mars = planetaryConstants.MARS
# get QoIs for individual probes from traj objects
(N, Np) = lonfs.shape
missed = np.empty((N, Np))
peakq = np.empty((N, Np))
vimp = np.empty((N, Np))
Mdeploy = np.empty((N, Np))
efpa = np.empty((N, Np))
v0 = np.empty((N, Np))
err = np.empty((N, Np))

for i, trajj in enumerate(trajs):
    for j in range(Np):
        missed[i,j] = trajj[j].missed
        if not trajj[j].missed:
            peakq[i,j] = trajj[j].peakq
            vimp[i,j] = trajj[j].v[-1]
            Mdeploy[i,j] = trajj[j].Mdeploy
            
            # find index at atm interface, save off efpa and v0
            # k = np.abs(trajj[j].h - mars.halt).argmin()
            k = np.argmax(trajj[j].h < mars.halt)
            efpa[i,j] = trajj[j].gam[k]
            v0[i,j] = trajj[j].v[k]
            
            # # find time of entry, save off efpa and v0
            # k = (np.abs(trajj[j].t - trajj[j].tentry)).argmin()
            # efpa[i,j] = trajj[j].gam[k]
            # v0[i,j] = trajj[j].v[k]
            
            # find miss distance
            err[i,j] = conversions.greatCircle(dlonList[j], lonfs[i,j],
                                               dlatList[j], latfs[i,j], mars) # m
        else:
            peakq[i,j] = np.NaN
            vimp[i,j] = np.NaN
            Mdeploy[i,j] = np.NaN
            efpa[i,j] = np.NaN
            v0[i,j] = np.NaN
            err[i,j] = np.NaN

# mask NaN values for later statistical operations
err = np.ma.array(err, mask = np.isnan(err))
peakq = np.ma.array(peakq, mask = np.isnan(peakq))
vimp = np.ma.array(vimp, mask = np.isnan(vimp))
Mdeploy = np.ma.array(Mdeploy, mask = np.isnan(Mdeploy))
efpa = np.ma.array(efpa, mask = np.isnan(efpa))
v0 = np.ma.array(v0, mask = np.isnan(v0))
        
# # example of computing statistics
# print(np.mean(v0[:,0]))
    
# missed_A = [trajj[0].missed for trajj in trajs]
# missed_B = [trajj[1].missed for trajj in trajs]
# missed_C = [trajj[2].missed for trajj in trajs]


#%%
# =============================================================================
# Statistics and histograms
# =============================================================================
plt.close('all')
fig1, ax1 = plt.subplots()
ax1.grid()
plt.xlabel('landing error, km')
plt.ylabel('frequency')
plt.tight_layout()

bw = 10
maxerr = 500
bw2 = 10
maxerr2 = 500

# bw = 2
# maxerr = 100
# bw2 = 0.1
# maxerr2 = 10

fig2, ax2 = plt.subplots()
ax2.grid()
plt.xlabel('landing error, km')
plt.ylabel('frequency')
plt.tight_layout()

fig3, ax3 = plt.subplots()
ax3.grid()
plt.xlabel('EFPA, deg')
plt.ylabel('frequency')
plt.tight_layout()
bwe1 = 0.1

fig4, ax4 = plt.subplots()
ax4.grid()
plt.xlabel('EFPA, deg')
plt.ylabel('frequency')
plt.tight_layout()

efpaMeans = [-18]
lonMeans = [np.degrees(lonfCenter)]

def isOutlier(x):
    return x > maxerr*1e3

for j in range(Np):
    print('\nANALYZING PROBE {0:d}: Nominal landing site: ({1:.2f},{2:.2f})'\
          .format(j, np.degrees(dlonList[j]),np.degrees(dlatList[j])))
    print('\tLanding error: mean {0:.3f} km, 3sigma {1:.3f} km'\
          .format(np.mean(err[:,j])/1e3, 3*np.std(err[:,j])/1e3))
    print('\tEFPA: mean {0:.3e} deg, 3sigma {1:.3e} deg'\
          .format(np.degrees(np.mean(efpa[:,j])), 3*np.degrees(np.std(efpa[:,j]))))
    print('\tv0: mean {0:.3f} km/s, 3sigma {1:.3f} km/s'\
          .format(np.mean(v0[:,j])/1e3, 3*np.std(v0[:,j])/1e3))
    print('\tvf: mean {0:.3f} m/s, 3sigma {1:.3f} m/s'\
          .format(np.mean(vimp[:,j]), 3*np.std(vimp[:,j])))
    print('\tMach at deploy: mean {0:.3f}, 3sigma {1:.3f}'\
          .format(np.mean(Mdeploy[:,j]), 3*np.std(Mdeploy[:,j])))
    print('\tpeakq: mean {0:.3e} W/cm^2, 3sigma {1:.3e} W/cm^2'\
          .format(np.mean(peakq[:,j]), 3*np.std(peakq[:,j])))
    
    if j < 4:
        if j == 3:
            errFj = err[~isOutlier(err[:,j]),j]
            ax1.hist(errFj/1e3, bins = np.arange(0, maxerr + bw, bw),
                 rwidth = 0.9, alpha = 0.5,
                 label = r'{0:.2f}$\degree$'.format(np.degrees(dlonList[j] - lonfCenter)))
        else:
            ax1.hist(err[:,j]/1e3, bins = np.arange(0, maxerr + bw, bw),
                 rwidth = 0.9, alpha = 0.5,
                 label = r'{0:.2f}$\degree$'.format(np.degrees(dlonList[j] - lonfCenter)))
        efpaMeans.append(np.degrees(np.mean(efpa[:,j])))
        lonMeans.append(np.degrees(np.mean(lonfs[:,j])))
        
        dat = np.degrees(efpa[:,j])
        ax3.hist(dat, bins = np.arange(np.min(dat), np.max(dat) + bwe1, bwe1),
                 rwidth = 0.9, alpha = 0.5,
                 label = r'{0:.2f}$\degree$'.format(np.degrees(dlonList[j] - lonfCenter)),
                 zorder = -j)
        
    else:
        ax2.hist(err[:,j]/1e3, bins = np.arange(0, maxerr2 + bw2, bw2),
                 rwidth = 0.9, alpha = 0.5,
                 label = r'{0:.2f}$\degree$'.format(np.degrees(dlatList[j] - latfCenter)))
        dat = np.degrees(efpa[:,j])
        ax4.hist(dat, bins = np.arange(np.min(dat), np.max(dat) + bwe1, bwe1),
                 rwidth = 0.9, alpha = 0.5,
                 label = r'{0:.2f}$\degree$'.format(np.degrees(dlatList[j] - latfCenter)),
                 zorder = -j)
    
    

ax1.legend(title = 'nominal separation:')
ax2.legend(title = 'nominal separation:')
ax3.legend(title = 'nominal separation:')
ax4.legend(title = 'nominal separation:')

pout = (1 - np.sum(err[:,3] <= maxerr*1e3)/len(err[:,3]))*100
ax1.annotate('{0:.0f}% of {1:.2f}$\degree$ cases'\
             .format(pout, np.degrees(dlonList[3] - lonfCenter)),
               # xy = (100,100), xytext=(20,104),arrowprops = dict(color = 'C3',
               #                                                    alpha = 0.5))
    
                xy = (500,100), xytext=(150,104),arrowprops = dict(color = 'C3',
                                                               alpha = 0.5))

# fig, ax = plt.subplots()
# ax.plot(lonMeans, efpaMeans, '.-')
# ax.grid()


#%%
# =============================================================================
# Plots
# =============================================================================
plt.rcParams.update({'font.size': 14})
mars = planetaryConstants.MARS

plt.close('all')

# img = plt.imread('../data/CF_CTX_screencap.png')
fig = plt.figure()
ax = fig.add_subplot(111)
# ax.imshow(img, extent=[160.1914394, 163.8111954, 8.2036983, 10.52815])
ax.grid()
ax.set_xlabel('longitude, deg')
ax.set_ylabel('latitude, deg')
# ax.set_xlim((-180,180))
# ax.set_ylim((-90,90))
ax.set_xlim((-1,41))
ax.set_ylim((-2,7))
ax.set_aspect('equal', 'box')

def lon2dist(lon):
    return (np.radians(lon) - lonfCenter) * mars.rad / 1e3

def dist2lon(dist):
    return np.degrees((dist * 1e3 / mars.rad) + lonfCenter)

def lat2dist(lat):
    return (np.radians(lat) - latfCenter) * mars.rad / 1e3

def dist2lat(dist):
    return np.degrees((dist * 1e3 / mars.rad) + latfCenter)

secax = ax.secondary_xaxis('top', functions = (lon2dist, dist2lon))
secax.set_xlabel('distance, km')
secay = ax.secondary_yaxis('right', functions = (lat2dist, dist2lat))
secay.set_ylabel('distance, km')

for i in range(len(dlonList)):
    ax.plot(np.degrees(lonfs[:,i]), np.degrees(latfs[:,i]), '.',
            markersize = 4, label = 'A actual')

# ax.plot(np.degrees(lonfs[:,0]), np.degrees(latfs[:,0]), '.',
#         markersize = 4, label = 'A actual')
# ax.plot(np.degrees(lonfs[:,1]), np.degrees(latfs[:,1]), '.',
#         markersize = 4, label = 'B actual')
# ax.plot(np.degrees(lonfs[:,2]), np.degrees(latfs[:,2]), '.',
#         markersize = 4, label = 'C actual')

ax.plot(np.degrees(dlonList), np.degrees(dlatList),
        'C3*', markersize = 8, label = 'nominal landing sites')

ax.plot(np.degrees(lonfCenter), np.degrees(latfCenter), 'C3P',
        label = 'nominal center point')


ax.legend()

# pickle.dump(fig, open('results/networkMC.fig.pickle', 'wb'))

# #%% add labels for nominal plot
# ax.annotate('A', (np.degrees(dlonList[0] + lonfCenter),
#                   np.degrees(dlatList[0] + latfCenter)), fontsize = 20,
#             label = 'nominal landing sites')
# ax.annotate('B', (np.degrees(dlonList[1] + lonfCenter),
#                   np.degrees(dlatList[1] + latfCenter)), fontsize = 20,)
# ax.annotate('C', (np.degrees(dlonList[2] + lonfCenter),
#                   np.degrees(dlatList[2] + latfCenter)), fontsize = 20,)
# ax.annotate('-A', (np.degrees(dlonList[3] + lonfCenter),
#                    np.degrees(dlatList[3] + latfCenter)), fontsize = 20,)
# ax.annotate('-B', (np.degrees(dlonList[4] + lonfCenter),
#                    np.degrees(dlatList[4] + latfCenter)), fontsize = 20,)
# ax.annotate('-C', (np.degrees(dlonList[5] + lonfCenter),
#                    np.degrees(dlatList[5] + latfCenter)), fontsize = 20,)
# # add arrow showing downrange direction
# x1 = np.degrees(lonfCenter)
# y1 = np.degrees(latfCenter)
# dx = 0.5 * np.sin(np.radians(80))
# dy = 0.5 * np.cos(np.radians(80))
# ax.arrow(x1, y1, dx, dy, color = 'C3', width = 0.02,
#          label = 'downrange direction')

# ax.legend()





#%%
# =============================================================================
# Analysis
# =============================================================================
# # get nominal dists
# # NOTE: dlonList and dlatList are ordered differently than coords, so reorder
# coordsNom = []
# for i in [3, 0, 4, 1, 5, 2]:
#     coordsNom.append(((dlonList[i] + lonfCenter), (dlatList[i] + latfCenter)))

# distsNom = []
# distsNom = np.empty(int((len(coordsNom) * (len(coordsNom) - 1)) / 2))
# distsNom[:] = np.NaN
# di = 0
# for j in range(len(coordsNom)):
#     for k in range(j+1, len(coordsNom)):
#         dist = conversions.greatCircle(coordsNom[j][0], coordsNom[k][0],
#                                        coordsNom[j][1], coordsNom[k][1], mars)
#         distsNom[di] = dist/1e3 # km
        
#         di += 1

# # verify that average point equals central point
# coordsNomLon = [coord[0] for coord in coordsNom]
# coordsNomLat = [coord[1] for coord in coordsNom]

# avgLonNom = np.mean(coordsNomLon)
# avgLatNom = np.mean(coordsNomLat)
# # print(lonfCenter - avgLonNom)
# # print(latfCenter - avgLatNom)

#%% now analyze dispersed coordinates
# coordsList = []
# for i, (lonfsi, latfsi) in enumerate(zip(lonfs, latfs)):
#     coordsList.append([(lon,lat) for (lon, lat) in zip(lonfsi, latfsi)])

# mindistList = []
# maxdistList = []
# avgdistList = []
# shapeErrList = []
# centerErrList = []
# # get all separation distances
# for i, coords in enumerate(coordsList):
#     dists = np.empty(int((len(coords) * (len(coords) - 1)) / 2))
#     dists[:] = np.NaN
#     distErrs = np.empty(dists.shape)
#     distErrs[:] = np.NaN
    
#     di = 0
    
#     # get center point and compute error
#     coordsLon = [coord[0] for coord in coords]
#     coordsLat = [coord[1] for coord in coords]
#     avgLon = np.mean(coordsLon)
#     avgLat = np.mean(coordsLat)
    
#     if i == 0:
#         ax.plot(np.degrees(avgLon), np.degrees(avgLat), 'C2+',
#                 label = 'Monte Carlo center points')
#     else:
#         ax.plot(np.degrees(avgLon), np.degrees(avgLat), 'C2+')
    
#     centerErrList.append(np.sqrt((avgLonNom - avgLon)**2\
#                                 + (avgLatNom - avgLat)**2))
    
#     for j in range(len(coords)):
#         for k in range(j+1, len(coords)):
#             dist = conversions.greatCircle(coords[j][0], coords[k][0],
#                                            coords[j][1], coords[k][1], mars)
#             dists[di] = dist/1e3 # km
            
#             distErrs[di] = (distsNom[di] - dists[di])**2
            
#             di += 1
            
#     shapeErrList.append(np.sqrt(np.sum(distErrs)) / len(coords))
    
#     mindistList.append(min(dists))
#     maxdistList.append(max(dists))
#     avgdistList.append(np.mean(dists))
    

# centerErrListKM = [err * mars.rad / 1e3 for err in centerErrList]

# for i, coord in enumerate(coordsList[np.argmax(shapeErrList)]):
#     if i == 0:
#         ax.plot(np.degrees(coord[0]), np.degrees(coord[1]), 'pC3',
#                 label = 'landing sites with worst shape error')
#     else:
#         ax.plot(np.degrees(coord[0]), np.degrees(coord[1]), 'pC3')

#%%
# print('center error:')
# print(stats.describe(centerErrListKM))
# print('3sig: {0:.3f} km'.format(3 * np.sqrt(stats.describe(centerErrListKM)[3])))

# print('\nshape error:')
# print(stats.describe(shapeErrList))
# print('3sig: {0:.3f} km'.format(3 * np.sqrt(stats.describe(shapeErrList)[3])))
    
# print('\nminimum separation distances:')
# print(stats.describe(mindistList))
# print('3sig: {0:.3f} km'.format(3 * np.sqrt(stats.describe(mindistList)[3])))

# print('\nmaximum separation distances:')
# print(stats.describe(maxdistList))
# print('3sig: {0:.3f} km'.format(3 * np.sqrt(stats.describe(maxdistList)[3])))

# print('\naverage separation distances:')
# print(stats.describe(avgdistList))
# print('3sig: {0:.3f} km'.format(3 * np.sqrt(stats.describe(avgdistList)[3])))










