# -*- coding: utf-8 -*-
"""
analyze.py:
    analysis script for Monte Carlo runs of a single SHIELD probe
Created on Tue Jul 19 14:27:11 2022

@author: Samuel Albert
"""

import sys
sys.path.append('..')

from src import planetaryConstants, atmosphere

import dill
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt

plt.close('all')
plt.rcParams.update({'font.size': 14})

# =============================================================================
# Compare single-probe performance
# =============================================================================
fig1, ax1 = plt.subplots()
ax1.grid()
plt.xlabel('landing error, km')
plt.ylabel('frequency')
plt.tight_layout()

fig2, ax2 = plt.subplots()
ax2.grid()
plt.xlabel(r'peak heat flux, $W/cm^2$')
plt.ylabel('frequency')
plt.tight_layout()

fig3, ax3 = plt.subplots()
ax3.grid()
plt.xlabel('Mach number at drag skirt deploy')
plt.ylabel('frequency')
plt.tight_layout()

fig4, ax4 = plt.subplots()
ax4.grid()
plt.xlabel('longitude, deg')
plt.ylabel('latitude, deg')

fig5, ax5 = plt.subplots()
ax5.grid()
plt.xlabel('density variation from nominal')
plt.ylabel('altitude, km')

fig6, ax6 = plt.subplots()
ax6.grid()
plt.xlabel('efpa, deg')
plt.ylabel('frequency')

fig7, ax7 = plt.subplots()
ax7.grid()
plt.xlabel('entry velocity, km/s')
plt.ylabel('frequency')


mars = planetaryConstants.MARS
atmfilename = '../data/MarsGRAMNominal.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)

def lon2dist(lon):
    return (np.radians(lon)) * mars.rad / 1e3

def dist2lon(dist):
    return np.degrees((dist * 1e3 / mars.rad))

def lat2dist(lat):
    return (np.radians(lat)) * mars.rad / 1e3

def dist2lat(dist):
    return np.degrees((dist * 1e3 / mars.rad))

N = 1000
gamList = [12,18,24]
# gamList = [24]
for gam in gamList:
    print('ANALYZING: EFPA = -{0:.0f}'.format(gam))
    # load and process data
    fname = '../results/singleMC_' + str(N) + '_' + str(gam) + '.npz'
    # fname = '../results/singleMC_ONLYATM_1000_24_0817155334.npz'
    with open(fname, 'rb') as pfile:
        trajList = dill.load(pfile)
    
    N = len(trajList)
    
    qmax = []
    vimp = []
    Mdeploy = []
    xfvecs = np.empty((N,3))
    lonfs = []
    latfs = []
    rhovecList = []
    efpa = []
    v0 = []
    
    for i, traj in enumerate(trajList):
        qmax.append(traj.peakq)
        vimp.append(traj.v[-1])
        Mdeploy.append(traj.Mdeploy)
        xfvecs[i,:] = traj.xxvec[:3,-1]
        lonfs.append(np.degrees(traj.lon[-1]))
        latfs.append(np.degrees(traj.lat[-1]))
        efpa.append(np.degrees(traj.gam[0]))
        v0.append(traj.v[0]/1e3)
        ax5.plot((traj.rho - mars.rhoFun(traj.h+mars.rad)) / mars.rhoFun(traj.h+mars.rad),
                     traj.h/1e3, 'C0', alpha = 0.5)
    qmax = np.asarray(qmax)
    vimp = np.asarray(vimp)
    
    xxfMean = np.mean(xfvecs, axis = 0)
    xfErr = np.linalg.norm(xfvecs - xxfMean, axis = 1)

    # print stats
    print('peak heat flux, W/cm^2: \n\t', stats.describe(qmax/1e4),
          np.std(qmax/1e4), '\n')
    print('impact velocity, m/s: \n\t', stats.describe(vimp),
          np.std(vimp), '\n')
    print('landing error, km: \n\t', stats.describe(xfErr/1e3),
          np.std(xfErr/1e3),'\n')
    print('deployment Mach number: \n\t', stats.describe(Mdeploy),
          np.std(Mdeploy), '\n')
    
    # plots
    ax1.hist(xfErr/1e3, bins = 20, rwidth = 0.9, alpha = 0.5,
              label = r'$\gamma_0 = -' + str(gam) +'\degree$')
    ax2.hist(qmax/1e4, bins = 20, rwidth = 0.9, alpha = 0.5,
              label = r'$\gamma_0 = -' + str(gam) +'\degree$')
    ax3.hist(Mdeploy, bins = 20, rwidth = 0.9, alpha = 0.5,
              label = r'$\gamma_0 = -' + str(gam) +'\degree$')
    # ax1.hist(xfErr/1e3, rwidth = 0.9, alpha = 0.5,
    #          label = r'$\gamma_0 = -' + str(gam) +'\degree$')
    ax4.plot(lonfs, latfs, '.')
    ax6.hist(efpa, bins = 20, rwidth = 0.9, alpha = 0.5,
             label = r'$\gamma_0 = -' + str(gam) +'\degree$')
    ax7.hist(v0, bins = 20, rwidth = 0.9, alpha = 0.5,
             label = r'$\gamma_0 = -' + str(gam) +'\degree$')
    
    
    
    
    
ax1.legend()
plt.tight_layout()
ax2.legend()
plt.tight_layout()
ax3.legend()
plt.tight_layout()
ax6.legend()
plt.tight_layout()
ax7.legend()
plt.tight_layout()

secax = ax4.secondary_xaxis('top', functions = (lon2dist, dist2lon))
secax.set_xlabel('distance, km')
secay = ax4.secondary_yaxis('right', functions = (lat2dist, dist2lat))
secay.set_ylabel('distance, km')
ax4.set_ylim(-0.1,0.1)
plt.tight_layout()

#%% plots


# plt.figure()
# plt.hist(vimp, bins = 20, rwidth = 0.9, alpha = 0.5)
# plt.grid()
# plt.xlabel('impact velocity, m/s')
# plt.tight_layout()