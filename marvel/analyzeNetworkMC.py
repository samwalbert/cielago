# -*- coding: utf-8 -*-
"""
analyzeNetworkMC.py:
    combine and analyze results of Monte Carlo analysis
Created on Tue Nov 30 13:02:33 2021

@author: Samuel
"""

import sys
sys.path.append('..')

from src import conversions
from src import planetaryConstants

import pickle
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt

#%%
# =============================================================================
# Combine data files into one
# =============================================================================
fnames = []
# fnames.append('../results/networkMC_N1000_1129224301.npz')
# fnames.append('../results/networkMC_N1000_1129224307.npz')
# fnames.append('../results/networkMC_N1000_1129224313.npz')
# fnames.append('../results/networkMC_N1000_1129224324.npz')
# fnames.append('../results/networkMC_N1000_1129224329.npz')

# fnames.append('../results/networkMC_12_N1000_0720115850.npz')
# fnames.append('../results/networkMC_18_N1000_0526152014.npz')
# fnames.append('../results/networkMC_24_N1000_0526152022.npz')

fnames.append('../results/networkMC_12_N500_0828114123.npz')
fnames.append('../results/networkMC_12_N500_0828114133.npz')

lonfsList = []
latfsList = []

for fname in fnames:
    data = np.load(fname)
    lonfsList.append(data['lonfs'])
    latfsList.append(data['latfs'])
    lonfCenter = data['lonfCenter']
    latfCenter = data['latfCenter']

# below are shape (i, j, k) where i = number of trials, j = number of unique
#   targets, and k = 2 to contain reciprocal target pairs (i.e. (lon,-lon))
lonfs = np.concatenate(lonfsList, axis = 0)
latfs = np.concatenate(latfsList, axis = 0)

# reshape to i x (j*k)
lonfs = lonfs.reshape(lonfs.shape[0], (lonfs.shape[1] * lonfs.shape[2]))
latfs = latfs.reshape(latfs.shape[0], (latfs.shape[1] * latfs.shape[2]))

#%%
# =============================================================================
# Set up desired network geometry
# =============================================================================
hda0 = np.radians(80)

L = np.radians(0.5)
dlonList90 = np.array([2*L, L/2, L/2])
dlatList90 = np.array([0, L/2, -L/(2*np.sqrt(3))])
dlonList = dlonList90 * np.sin(hda0) - dlatList90 * np.cos(hda0)
dlatList = dlatList90 * np.sin(hda0) + dlonList90 * np.cos(hda0)

dlonList = np.append(dlonList, -dlonList)
dlatList = np.append(dlatList, -dlatList)


#%%
# =============================================================================
# Plots
# =============================================================================
plt.rcParams.update({'font.size': 14})
mars = planetaryConstants.MARS

plt.close('all')

img = plt.imread('../data/CF_CTX_screencap.png')
fig = plt.figure()
ax = fig.add_subplot(111)
ax.imshow(img, extent=[160.1914394, 163.8111954, 8.2036983, 10.52815])
ax.grid()
ax.set_xlabel('longitude, deg')
ax.set_ylabel('latitude, deg')
ax.set_aspect('equal', 'box')

def lon2dist(lon):
    return (np.radians(lon) - lonfCenter) * mars.rad / 1e3

def dist2lon(dist):
    return np.degrees((dist * 1e3 / mars.rad) + lonfCenter)

def lat2dist(lat):
    return (np.radians(lat) - latfCenter) * mars.rad / 1e3

def dist2lat(dist):
    return np.degrees((dist * 1e3 / mars.rad) + latfCenter)

secax = ax.secondary_xaxis('top', functions = (lon2dist, dist2lon))
secax.set_xlabel('distance, km')
secay = ax.secondary_yaxis('right', functions = (lat2dist, dist2lat))
secay.set_ylabel('distance, km')

ax.plot(np.degrees(lonfs.flatten()), np.degrees(latfs.flatten()), '.',
        markersize = 4, label = 'Monte Carlo landing sites')
plt.tight_layout()

ax.legend()

# pickle.dump(fig, open('results/networkMC.fig.pickle', 'wb'))

# #%% add labels for nominal plot
# ax.annotate('A', (np.degrees(dlonList[0] + lonfCenter),
#                   np.degrees(dlatList[0] + latfCenter)), fontsize = 20,
#             label = 'nominal landing sites')
# ax.annotate('B', (np.degrees(dlonList[1] + lonfCenter),
#                   np.degrees(dlatList[1] + latfCenter)), fontsize = 20,)
# ax.annotate('C', (np.degrees(dlonList[2] + lonfCenter),
#                   np.degrees(dlatList[2] + latfCenter)), fontsize = 20,)
# ax.annotate('-A', (np.degrees(dlonList[3] + lonfCenter),
#                    np.degrees(dlatList[3] + latfCenter)), fontsize = 20,)
# ax.annotate('-B', (np.degrees(dlonList[4] + lonfCenter),
#                    np.degrees(dlatList[4] + latfCenter)), fontsize = 20,)
# ax.annotate('-C', (np.degrees(dlonList[5] + lonfCenter),
#                    np.degrees(dlatList[5] + latfCenter)), fontsize = 20,)
# # add arrow showing downrange direction
# x1 = np.degrees(lonfCenter)
# y1 = np.degrees(latfCenter)
# dx = 0.5 * np.sin(np.radians(80))
# dy = 0.5 * np.cos(np.radians(80))
# ax.arrow(x1, y1, dx, dy, color = 'C3', width = 0.02,
#          label = 'downrange direction')

# ax.legend()


#%%
# =============================================================================
# Analysis
# =============================================================================
# get nominal dists
# NOTE: dlonList and dlatList are ordered differently than coords, so reorder
coordsNom = []
for i in [3, 0, 4, 1, 5, 2]:
    coordsNom.append(((dlonList[i] + lonfCenter), (dlatList[i] + latfCenter)))

distsNom = []
distsNom = np.empty(int((len(coordsNom) * (len(coordsNom) - 1)) / 2))
distsNom[:] = np.NaN
di = 0
for j in range(len(coordsNom)):
    for k in range(j+1, len(coordsNom)):
        dist = conversions.greatCircle(coordsNom[j][0], coordsNom[k][0],
                                       coordsNom[j][1], coordsNom[k][1], mars)
        distsNom[di] = dist/1e3 # km
        
        di += 1

# verify that average point equals central point
coordsNomLon = [coord[0] for coord in coordsNom]
coordsNomLat = [coord[1] for coord in coordsNom]

avgLonNom = np.mean(coordsNomLon)
avgLatNom = np.mean(coordsNomLat)
# print(lonfCenter - avgLonNom)
# print(latfCenter - avgLatNom)

#%% now analyze dispersed coordinates
coordsList = []
for i, (lonfsi, latfsi) in enumerate(zip(lonfs, latfs)):
    coordsList.append([(lon,lat) for (lon, lat) in zip(lonfsi, latfsi)])

mindistList = []
maxdistList = []
avgdistList = []
shapeErrList = []
centerErrList = []
# get all separation distances
for i, coords in enumerate(coordsList):
    dists = np.empty(int((len(coords) * (len(coords) - 1)) / 2))
    dists[:] = np.NaN
    distErrs = np.empty(dists.shape)
    distErrs[:] = np.NaN
    
    di = 0
    
    # get center point and compute error
    coordsLon = [coord[0] for coord in coords]
    coordsLat = [coord[1] for coord in coords]
    avgLon = np.mean(coordsLon)
    avgLat = np.mean(coordsLat)
    
    if i == 0:
        ax.plot(np.degrees(avgLon), np.degrees(avgLat), 'C2+',
                label = 'Monte Carlo center points')
    else:
        ax.plot(np.degrees(avgLon), np.degrees(avgLat), 'C2+')
    
    centerErrList.append(np.sqrt((avgLonNom - avgLon)**2\
                                + (avgLatNom - avgLat)**2))
    
    for j in range(len(coords)):
        for k in range(j+1, len(coords)):
            dist = conversions.greatCircle(coords[j][0], coords[k][0],
                                           coords[j][1], coords[k][1], mars)
            dists[di] = dist/1e3 # km
            
            distErrs[di] = (distsNom[di] - dists[di])**2
            
            di += 1
            
    shapeErrList.append(np.sqrt(np.sum(distErrs)) / len(coords))
    
    mindistList.append(min(dists))
    maxdistList.append(max(dists))
    avgdistList.append(np.mean(dists))
    

centerErrListKM = [err * mars.rad / 1e3 for err in centerErrList]

for i, coord in enumerate(coordsList[np.argmax(shapeErrList)]):
    if i == 0:
        ax.plot(np.degrees(coord[0]), np.degrees(coord[1]), 'pC3',
                label = 'landing sites with worst shape error')
    else:
        ax.plot(np.degrees(coord[0]), np.degrees(coord[1]), 'pC3')

#%%
ax.plot(np.degrees(dlonList + lonfCenter), np.degrees(dlatList + latfCenter),
        'C1*', markersize = 8, label = 'nominal landing sites')

ax.plot(np.degrees(lonfCenter), np.degrees(latfCenter), 'C1P',
        label = 'nominal center point')

ax.legend()
plt.tight_layout()

print('center error:')
print(stats.describe(centerErrListKM))
print('3sig: {0:.3f} km'.format(3 * np.sqrt(stats.describe(centerErrListKM)[3])))

print('\nshape error:')
print(stats.describe(shapeErrList))
print('3sig: {0:.3f} km'.format(3 * np.sqrt(stats.describe(shapeErrList)[3])))
    
print('\nminimum separation distances:')
print(stats.describe(mindistList))
print('3sig: {0:.3f} km'.format(3 * np.sqrt(stats.describe(mindistList)[3])))

print('\nmaximum separation distances:')
print(stats.describe(maxdistList))
print('3sig: {0:.3f} km'.format(3 * np.sqrt(stats.describe(maxdistList)[3])))

print('\naverage separation distances:')
print(stats.describe(avgdistList))
print('3sig: {0:.3f} km'.format(3 * np.sqrt(stats.describe(avgdistList)[3])))










