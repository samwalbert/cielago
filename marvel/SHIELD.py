# -*- coding: utf-8 -*-
"""
SHIELD.py:
    runs a trajectory for a single SHIELD lander, outputs basic results
Created on Mon Mar  7 12:57:26 2022

@author: Samuel Albert
"""

import sys
sys.path.append('..')

from src import sim, planetaryConstants, atmosphere, conversions, marvel

import numpy as np
import matplotlib.pyplot as plt

plt.close('all')
plt.rcParams.update({'font.size': 16})

#%%
# =============================================================================
# Set up Planet, Vehicle, Params
# =============================================================================
mars = planetaryConstants.MARS
atmfilename = '../data/MarsGRAMNominal.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)
mars.MachFun = lambda r, u: u / np.interp((r - mars.rad), mars.sound[0,:],
                                          mars.sound[1,:])

shield = sim.Vehicle('SHIELD')
shield.BC = 10 # kg/m^2
shield.LD = 0
# shield.getAero = lambda r, u, Planet: (shield.BC, shield.LD) # constant aero

marvel.loadSHIELDAeroData(shield)
shield.scaleAero = False


# must define bank function, but it doesn't matter since L/D = 0
shield.bankFun = lambda t, yy: 0

# initial state
t0 = 0 # s
r0 = (mars.rad + mars.halt) # m
lon0 = np.radians(0)
lat0 = np.radians(0)
u0 = 6e3 # m/s
gam0 = np.radians(-90)
hda0 = np.radians(90) # due-East
y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])
shield.y0 = conversions.sphPR2cartI(y0sph, mars, t0)

# define event timing
# tdeploy = 140
# tjettison = 150
# # earliest permissible jettison, for EFPA of -18
# tdeploy = 106
# tjettison = 110
# earliest permissible jettison, for EFPA of -24
tdeploy = 77
tjettison = 81
g0 = 1 # Earth g

#%% Propagate
sol, params = marvel.runSHIELDtimed(mars, shield, tdeploy, tjettison, g0)

#%% build Traj object from results
traj = sim.buildTrajCart(sol, mars)

#%% get properties along trajectory
shield.Rn = 0.85 # m, nose radius
traj = marvel.analyzeSHIELDTraj(mars, shield, params, traj)

#%% Plots
handles = sim.plotTraj(traj)

print('SHIELD landed at a (lon, lat) of {0:.3f}, {1:.3f} (deg)'\
      .format(np.degrees(traj.lon[-1]), np.degrees(traj.lat[-1])))

# add plot labels for deployment and jettison
fig, ax = handles[0]
ideploy = np.argmax(traj.t >= params.tdeployed)
ax.plot(traj.v[ideploy]/1e3, traj.h[ideploy]/1e3, 'o', label = 'deploy')
ax.annotate('E+{0:.1f} s'.format(traj.t[ideploy]),
             (traj.v[ideploy]/1e3+0.01, traj.h[ideploy]/1e3+0.1), color = 'C1')

ijett = np.argmax(traj.t >= params.tjettisoned)
ax.plot(traj.v[ijett]/1e3, traj.h[ijett]/1e3, 'P', label = 'jettison')
ax.annotate('E+{0:.1f} s'.format(traj.t[ijett]),
             (traj.v[ijett]/1e3, traj.h[ijett]/1e3-1), color = 'C2')

ax.plot(traj.v[-1]/1e3, traj.h[-1]/1e3, 'X', label = 'impact')
ax.annotate('E+{0:.1f} s'.format(traj.t[-1]),
            (traj.v[-1]/1e3+0.01, traj.h[-1]/1e3), color = 'C3')

ax.legend()




#%%
# =============================================================================
# Now change config Mach numbers and re-run
# =============================================================================
# # define latest permissible timing, for EFPA = -18
# tdeploy = 166
# tjettison = 170
# define latest permissible timing, for EFPA = -24
tdeploy = 123
tjettison = 133

# re-scale aerodynamics
shield.scaleAero = True
shield.BCscale = 1.1
shield.LDscale = 1.1

#%% Propagate
shield.y0 = conversions.sphPR2cartI(y0sph, mars, t0)
sol2, params = marvel.runSHIELDtimed(mars, shield, tdeploy, tjettison, g0)

#%% build Traj object from results
traj2 = sim.buildTrajCart(sol2, mars)

#%% get properties along trajectory
traj2 = marvel.analyzeSHIELDTraj(mars, shield, params, traj2)

#%% Plots
fig, ax = handles[0]
ax.plot(traj2.v/1e3, traj2.h/1e3, color = 'C3')

fig, ax = handles[1]
ax[0].plot(traj2.t, traj2.h/1e3, color = 'C3')
ax02 = ax[0].twinx()
ax02.tick_params(axis = 'y', which = 'both', labelright = False)
ax02.plot(traj2.t, traj2.v/1e3, 'C4--')
ax[1].plot(traj2.t, traj2.gload, color = 'C3')
ax12 = ax[1].twinx()
ax12.tick_params(axis = 'y', which = 'both', labelright = False)
ax12.plot(traj2.t, traj2.dynp/1e3, 'C4--')
ax[2].plot(traj2.t, traj2.qflux/1e4, color = 'C3')

fig, ax = handles[2]
ax[0].plot(traj2.t, traj2.Mach, color = 'C3')
ax[1].plot(traj2.t, traj2.BC, color = 'C3')

print('SHIELD landed at a (lon, lat) of {0:.3f}, {1:.3f} (deg)'\
      .format(np.degrees(traj2.lon[-1]), np.degrees(traj2.lat[-1])))

sep = conversions.greatCircle(traj.lon[-1], traj2.lon[-1], traj.lat[-1],
                              traj2.lat[-1], mars)
print('Separation between 2 landers: {0:.3f} km'.format(sep/1e3))

fig, ax = plt.subplots()
ax.plot(mars.rad/1e3 * traj.lon, traj.h/1e3, label = 'earliest')
ax.plot(mars.rad/1e3 * traj2.lon, traj2.h/1e3, '--', label = 'latest')
# ax.plot(traj2.v/1e3, traj2.h/1e3, label = 'latest')
ax.grid()
ax.legend()
ax.set_xlabel('downrange, km')
ax.set_ylabel('altitude, km')
plt.tight_layout()