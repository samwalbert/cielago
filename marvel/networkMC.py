# -*- coding: utf-8 -*-
"""
networkMC.py:
    Monte Carlo analysis for delivery of a network to the Martian surface
Created on Mon Nov 29 12:08:45 2021

@author: Samuel
"""

import sys
sys.path.append('..')

from src import sim
from src import planetaryConstants
from src import atmosphere
from src import conversions
from src import marvel

import time
import numpy as np
from scipy import stats
import datetime
import matplotlib.pyplot as plt

plt.close('all')
datestring = datetime.datetime.now().strftime('%m%d%H%M%S')

#%%
# =============================================================================
# Set up Planet, Vehicle, Params basics
# =============================================================================
mars = planetaryConstants.MARS
atmfilename = '../data/MarsGRAMNominal.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)
mars.atmBaseFilename = '../data/MarsGRAM_MC5000/'

shield = sim.Vehicle('SHIELD')
shield.BC = 10 # kg/m^2
shield.LD = 0
shield.getAero = lambda r, u, planet: (shield.BC, shield.LD)

# must define bank function, but it doesn't matter since L/D = 0
shield.bankFun = lambda t, yy: 0

# initial state
r0 = (mars.rad + mars.halt) # m
lon0 = np.radians(151)
lat0 = np.radians(7.5)
u0 = 6e3 # m/s
gam0degmag = 18
gam0 = np.radians(-gam0degmag)
hda0 = np.radians(80)
y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])

params = sim.Params()
params.rtol = 1e-10
params.atol = 1e-10
params.rmin = mars.rad
params.rmax = (mars.rad + mars.halt)

# initial and final time
params.t0 = 1e6 # s
params.tf = params.t0 + 5e3 # s

shield.y0 = conversions.sphPR2cartI(y0sph, mars, params.t0)
shield.y0Center = conversions.sphPR2cartI(y0sph, mars, params.t0)

params.event1 = lambda t, y: sim.belowMinAltCart(t, y, params)
params.event1.terminal = True

params.event2 = lambda t, y: sim.aboveMaxAltCart(t, y, params)
params.event2.terminal = True
params.event2.direction = 1

params.pert = 1e-4

#%%
# =============================================================================
# Get nominal center states at surface
# =============================================================================
params.tback = 0 # s
_, xxfsphCenter = marvel.getCenterState(mars, shield, params)
lonfCenter = xxfsphCenter[1]
latfCenter = xxfsphCenter[2]

#%%
# =============================================================================
# Load in maneuvers
# =============================================================================
data = np.load('../results/maneuvers_' + str(gam0degmag) + '.npz')
DVvecs = data['DVvecs']
tbackList = data['tbackList']

#%%
# =============================================================================
# Set up dispersions
# =============================================================================
params.u0Mean = u0
params.u0STD = 2/3 # m/s
params.gam0Mean = gam0
params.gam0STD = np.radians(0.2) / 3
params.BCMean = shield.BC
params.BCloc = shield.BC - 0.05 * shield.BC
params.BCscale = 0.1 * shield.BC
params.DVloc = -0.1
params.DVscale = 0.2

#%%
# =============================================================================
# Monte Carlo trials
# =============================================================================
N = 1000
atmoffset = 0
daysback = 5 # should be further back than the earliest tback value

lonfs = np.empty((N, len(tbackList), 2))
lonfs[:] = np.NaN
latfs = np.empty((N, len(tbackList), 2))
latfs[:] = np.NaN

disperseDV = True

if disperseDV:
    outname = '../results/networkMC_' + str(gam0degmag) + '_N' + str(N) + '_' + datestring
else:
    outname = '../results/networkMC_N' + str(N) + '_NODVdisp_' + datestring

tic = time.time()

for i in range(N):
    # pull atm profile
    aind = i + 1 + atmoffset
    mars.atmfilename = mars.atmBaseFilename + str(aind) + '.txt'
    mars.hList, mars.rhoList = atmosphere.readMarsGRAM(mars.atmfilename, True)
    mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)
    
    # build dispersed initial state
    u0 = stats.norm.rvs(params.u0Mean, params.u0STD)
    gam0 = stats.norm.rvs(params.gam0Mean, params.gam0STD)
    y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])
    shield.y0 = conversions.sphPR2cartI(y0sph, mars, params.t0)
    
    # back-propagate by daysback
    params.tspan = (params.t0, (params.t0 - daysback * 86400))
    params.events = ()
    solBack = sim.runCartesian(mars, shield, params)
    
    # update initial condition with back-propagated state
    # shield.y0 = solBack.y[:,-1]
    x0vecBack = solBack.y[:3,-1]
    v0vecBack = solBack.y[3:,-1]
    params.events = (params.event1, params.event2)
    
    # go through each tback value and propagate off the landers
    for j, (tback, DVvec) in enumerate(zip(tbackList, DVvecs)):
        # start from back-propagated state
        shield.y0 = solBack.y[:,-1]
        
        # propagate until tback
        params.tspan = (solBack.t[-1], (params.t0 - tback))
        params.events = ()
        solj = sim.runCartesian(mars, shield, params)
        x0vecj = solj.y[:3,-1]
        v0vecj = solj.y[3:,-1]
        
        # apply maneuvers in +/- directions, then propagate to surface
        for k, sign in enumerate([-1, 1]):
            # get nominal delta-V
            DVveck = sign * DVvecs[j,:]
            # and then disperse
            if disperseDV:
                DVveck += DVveck * stats.uniform.rvs(params.DVloc,
                                                     params.DVscale)
            
            # apply delta-V
            v0veck = v0vecj + DVveck
            
            # update initial state
            shield.y0 = np.block([x0vecj, v0veck]).flatten()
            
            # propagate to surface
            params.tspan = (solj.t[-1], params.tf)
            params.events = (params.event1, params.event2)
            solk = sim.runCartesian(mars, shield, params)
            
            # save off values
            xxfsphk = conversions.cartI2sphPR(solk.y[:,-1], mars, solk.t[-1])
            lonfs[i,j,k] = xxfsphk[1] # lonf, radians
            latfs[i,j,k] = xxfsphk[2] # latf, radians
            
            np.savez(outname,
                     lonfs = lonfs,
                     latfs = latfs,
                     lonfCenter = lonfCenter,
                     latfCenter = latfCenter)
    toc = time.time()
    tstr = str(datetime.timedelta(seconds=(toc-tic)))
    print('run {0:d} complete, elapsed time: '.format(i+1) + tstr)



#%%
# =============================================================================
# Plots results
# =============================================================================
img = plt.imread('../data/CF_CTX_screencap.png')
fig = plt.figure()
ax = fig.add_subplot(111)
ax.imshow(img, extent=[160.1914394, 163.8111954, 8.2036983, 10.52815])
ax.grid()
ax.set_xlabel('longitude, deg')
ax.set_ylabel('latitude, deg')
ax.set_aspect('equal', 'box')

def lon2dist(lon):
    return (np.radians(lon) - lonfCenter) * mars.rad / 1e3

def dist2lon(dist):
    return np.degrees((dist * 1e3 / mars.rad) + lonfCenter)

def lat2dist(lat):
    return (np.radians(lat) - latfCenter) * mars.rad / 1e3

def dist2lat(dist):
    return np.degrees((dist * 1e3 / mars.rad) + latfCenter)

secax = ax.secondary_xaxis('top', functions = (lon2dist, dist2lon))
secax.set_xlabel('distance, km')
secay = ax.secondary_yaxis('right', functions = (lat2dist, dist2lat))
secay.set_ylabel('distance, km')

ax.plot(np.degrees(lonfs.flatten()), np.degrees(latfs.flatten()), 'o')
            
    
    
















