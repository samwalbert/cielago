# -*- coding: utf-8 -*-
"""
findSepTime.py:
    finds separation time to reach a given target point using given DV mag.
    use marvelTimer.py to find bounds on solutions
Created on Sun Nov 28 19:45:00 2021

@author: Samuel
"""

import sys
sys.path.append('..')


from src import sim
from src import planetaryConstants
from src import atmosphere
from src import conversions
from src import marvel

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import root_scalar

plt.close('all')

#%%
# =============================================================================
# Set up Planet, Vehicle, Params basics
# =============================================================================
mars = planetaryConstants.MARS
atmfilename = '../data/MarsGRAMNominal.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)

shield = sim.Vehicle('SHIELD')
shield.BC = 10 # kg/m^2
shield.LD = 0
shield.getAero = lambda r, u, planet: (shield.BC, shield.LD)

# must define bank function, but it doesn't matter since L/D = 0
shield.bankFun = lambda t, yy: 0

# initial state
r0 = (mars.rad + mars.halt) # m
lon0 = np.radians(151)
lat0 = np.radians(7.5)
u0 = 6e3 # m/s
gam0degmag = 12
gam0 = np.radians(-gam0degmag)
hda0 = np.radians(80)
y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])

params = sim.Params()
params.rtol = 1e-11
params.atol = 1e-11
params.rmin = mars.rad
params.rmax = (mars.rad + mars.halt)

# initial and final time
params.t0 = 1e6 # s
params.tf = params.t0 + 5e3 # s

shield.y0 = conversions.sphPR2cartI(y0sph, mars, params.t0)
shield.y0Center = conversions.sphPR2cartI(y0sph, mars, params.t0)

params.event1 = lambda t, y: sim.belowMinAltCart(t, y, params)
params.event1.terminal = True

params.event2 = lambda t, y: sim.aboveMaxAltCart(t, y, params)
params.event2.terminal = True
params.event2.direction = 1

params.pert = 1e-4

#%%
# =============================================================================
# Get center states at surface
# =============================================================================
params.tback = 0 # s
xx0vecCenter, xxfsphCenter = marvel.getCenterState(mars, shield, params)
lonfCenter = xxfsphCenter[1]
latfCenter = xxfsphCenter[2]

#%%
# =============================================================================
# Set up desired target points and desired DV magnitude
# =============================================================================
L = np.radians(0.5)
dlonList90 = np.array([2*L, L/2, L/2])
dlatList90 = np.array([0, L/2, -L/(2*np.sqrt(3))])
dlonList = dlonList90 * np.sin(hda0) - dlatList90 * np.cos(hda0)
dlatList = dlatList90 * np.sin(hda0) + dlonList90 * np.cos(hda0)
# dlonList = np.radians([0.1])
# dlatList = np.radians([0])

# check desired network geometry
img = plt.imread('../data/CF_CTX_screencap.png')
fig = plt.figure()
ax = fig.add_subplot(111)
ax.imshow(img, extent=[160.1914394, 163.8111954, 8.2036983, 10.52815])
ax.plot(np.degrees(dlonList + lonfCenter),
        np.degrees(dlatList + latfCenter), 'o')
ax.plot(np.degrees(lonfCenter), np.degrees(latfCenter), 'x')
ax.grid()
ax.set_xlabel('longitude, deg')
ax.set_ylabel('latitude, deg')
ax.set_aspect('equal', 'box')

def lon2dist(lon):
    return (np.radians(lon) - lonfCenter) * mars.rad / 1e3

def dist2lon(dist):
    return np.degrees((dist * 1e3 / mars.rad) + lonfCenter)

def lat2dist(lat):
    return (np.radians(lat) - latfCenter) * mars.rad / 1e3

def dist2lat(dist):
    return np.degrees((dist * 1e3 / mars.rad) + latfCenter)

secax = ax.secondary_xaxis('top', functions = (lon2dist, dist2lon))
secax.set_xlabel('distance, km')
secay = ax.secondary_yaxis('right', functions = (lat2dist, dist2lat))
secay.set_ylabel('distance, km')


DVdes = 0.1 # m/s

#%%
# =============================================================================
# Root-solve to get tback for each target point
# =============================================================================
def targetPoint(tback, dlon, dlat, DVdes):
    params.tback = tback
    shield.y0 = shield.y0Center
    DVvec, (lonf, latf), (lonErr, latErr) = \
            marvel.sep2surface(mars, shield, params, dlon, dlat)
    DV = np.linalg.norm(DVvec)
    print('tback = {0:.6f} days, DV = {1:.6f} m/s'.format(tback/86400, DV))
    
    return DV - DVdes


tbackList = []
DVvecs = np.empty((len(dlonList), 3))
DVvecs[:] = np.NaN

for i, (dlon, dlat) in enumerate(zip(dlonList, dlatList)):
    res = root_scalar(lambda tback: targetPoint(tback, dlon, dlat, DVdes),
                      bracket = (0.5*86400, 4*86400), method = 'brentq',
                      maxiter = 10)
    # assert res.converged, 'failed to root-find tback'
    print(res.flag)
    tback = res.root
    tbackList.append(tback)
    print('tback = {0:.4f} days for target point {1:d}'.format(tback/86400, i))
    
    # get final DVvec
    params.tback = tback
    shield.y0 = shield.y0Center
    DVvec, (lonf, latf), (lonErr, latErr) = \
            marvel.sep2surface(mars, shield, params, dlon, dlat)
            
    DVvecs[i,:] = DVvec


#%%
# =============================================================================
# Open-loop propagate each target using solved tback values
# =============================================================================
xxsphList = []

for i, (dlon, dlat, tback) in enumerate(zip(dlonList, dlatList, tbackList)):
    for sign in [-1,1]:
        params.tback = tback
        shield.y0 = shield.y0Center
        
        # back-propagate by tback
        params.tspan = (params.t0, (params.t0 - params.tback))
        params.events = ()
        print('\nBack-propagating center trajectory...')
        solBack = sim.runCartesian(mars, shield, params)
        x0vecBack = solBack.y[:3,-1]
        v0vecBack = solBack.y[3:,-1]
        
        # get LVLH vectors prior to maneuver
        rhat, thetahat, hhat = conversions.getLVLH(x0vecBack, v0vecBack)
        
        # print DV direction
        DVhat = DVvecs[i,:] / np.linalg.norm(DVvecs[i,:])
        print(np.dot(rhat, DVhat))
        print(np.dot(thetahat, DVhat))
        print(np.dot(hhat, DVhat))
        
        # apply delta-V
        v0vecBack += sign * DVvecs[i,:]
        shield.y0 = np.block([x0vecBack, v0vecBack]).flatten()
        
        # forward-propagate to surface
        params.tspan = ((params.t0 - params.tback), params.tf)
        params.events = (params.event1, params.event2)
        sol = sim.runCartesian(mars, shield, params)
        
        # convert to spherical along entire trajectory
        xxsph = conversions.cartI2sphPR(sol.y, mars, sol.t)
        
        # save off values
        xxsphList.append(xxsph)

#%%
# do some list comprehension
lonfList = np.asarray([xxsph[1,-1] for xxsph in xxsphList])
latfList = np.asarray([xxsph[2,-1] for xxsph in xxsphList])
# lonErrList = (lonfList - lonfCenter) - dlonList
# latErrList = (latfList - latfCenter) - dlatList

#%%
# =============================================================================
# Plots and prints
# =============================================================================
ax.plot(np.degrees(lonfList), np.degrees(latfList), 'X')
ax.grid()

#%%
# =============================================================================
# Save data
# =============================================================================
np.savez('../results/maneuvers_' + str(gam0degmag) + '.npz',
         DVvecs = DVvecs,
         tbackList = tbackList)

#%%
# =============================================================================
# Do some analysis on the nominal network
# =============================================================================
L = np.radians(0.5)
dlonList90 = np.array([2*L, L/2, L/2])
dlatList90 = np.array([0, L/2, -L/(2*np.sqrt(3))])
dlonList = dlonList90 * np.sin(hda0) - dlatList90 * np.cos(hda0)
dlatList = dlatList90 * np.sin(hda0) + dlonList90 * np.cos(hda0)
dlonList = np.append(dlonList, -dlonList)
dlatList = np.append(dlatList, -dlatList)

lonNomList = []
latNomList = []
for i in [3, 0, 4, 1, 5, 2]:
    lonNomList.append(dlonList[i] + lonfCenter)
    latNomList.append(dlatList[i] + latfCenter)

# nominal network targeting errors
for i, pname in enumerate(['A', '-A', 'B', '-B', 'C', '-C']):
    # get great circle error
    errDist = conversions.greatCircle(lonNomList[i], lonfList[i],
                                      latNomList[i], latfList[i], mars)
    print(pname)
    print(errDist/1e3)
    
# nominal downrange and crossrange distances from center, km
print(mars.rad * dlonList90 / 1e3)
print(mars.rad * dlatList90 / 1e3)

# get LVLH unit vectors
rhat, thetahat, hhat = conversions.getLVLH(xx0vecCenter[:3], xx0vecCenter[3:])

# DV components
print('\nA:')
print(np.dot(DVvecs[0], rhat))
print(np.dot(DVvecs[0], thetahat))
print(np.dot(DVvecs[0], hhat))
print('\nB:')
print(np.dot(DVvecs[1], rhat))
print(np.dot(DVvecs[1], thetahat))
print(np.dot(DVvecs[1], hhat))
print('\nC:')
print(np.dot(DVvecs[2], rhat))
print(np.dot(DVvecs[2], thetahat))
print(np.dot(DVvecs[2], hhat))


















