# -*- coding: utf-8 -*-
"""
marvelTimer.py:
    sweep through values for tback and see how DV magnitude changes
Created on Sat Nov 27 21:37:59 2021

@author: Samuel
"""

import sys
sys.path.append('..')

from src import sim
from src import planetaryConstants
from src import atmosphere
from src import conversions
from src import marvel

import numpy as np
import matplotlib.pyplot as plt

plt.close('all')

#%%
# =============================================================================
# Set up Planet, Vehicle, Params basics
# =============================================================================
mars = planetaryConstants.MARS
atmfilename = '../data/MarsGRAMNominal.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)

shield = sim.Vehicle('SHIELD')
shield.BC = 10 # kg/m^2
shield.LD = 0

# must define bank function, but it doesn't matter since L/D = 0
shield.bankFun = lambda t, yy: 0

# initial state
r0 = (mars.rad + mars.halt) # m
lon0 = np.radians(156)
lat0 = np.radians(10)
u0 = 6e3 # m/s
gam0 = np.radians(-12)
hda0 = np.radians(80)
y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])

params = sim.Params()
params.rtol = 1e-11
params.atol = 1e-11
params.rmin = mars.rad
params.rmax = (mars.rad + mars.halt)

# initial and final time
params.t0 = 1e6 # s
params.tf = params.t0 + 5e3 # s

shield.y0 = conversions.sphPR2cartI(y0sph, mars, params.t0)
shield.y0Center = conversions.sphPR2cartI(y0sph, mars, params.t0)

params.event1 = lambda t, y: sim.belowMinAltCart(t, y, params)
params.event1.terminal = True

params.event2 = lambda t, y: sim.aboveMaxAltCart(t, y, params)
params.event2.terminal = True
params.event2.direction = 1

params.pert = 1e-4

#%%
# =============================================================================
# Set up desired target points
# =============================================================================
L = np.radians(0.5)
dlonList90 = np.array([2*L, L/2, L/2])
dlatList90 = np.array([0, L/2, -L/(2*np.sqrt(3))])
dlonList = dlonList90 * np.sin(hda0) - dlatList90 * np.cos(hda0)
dlatList = dlatList90 * np.sin(hda0) + dlonList90 * np.cos(hda0)
# dlonList = np.radians([0.1])
# dlatList = np.radians([0])

#%%
# =============================================================================
# Iterate through a list of tback values
# =============================================================================
tbackList = 86400 * np.array([0.25, 0.5, 1, 2, 3, 5])
# tbackList = 86400 * np.array([0.25, 0.5])

DVvecs = np.empty((len(tbackList), len(dlonList), 3))
DVvecs[:] = np.NaN
DVs = np.empty((len(tbackList), len(dlonList)))
DVs[:] = np.NaN
coordsf = np.empty((len(tbackList), len(dlonList), 2))
coordsf[:] = np.NaN
errs = np.empty((len(tbackList), len(dlonList), 2))
errs[:] = np.NaN

for i, tback in enumerate(tbackList):
    params.tback = tback
    for j, (dlon, dlat) in enumerate(zip(dlonList, dlatList)):
        shield.y0 = shield.y0Center
        DVvec, (lonf, latf), (lonErr, latErr) = \
            marvel.sep2surface(mars, shield, params, dlon, dlat)
        
        DVvecs[i, j, :] = DVvec
        DVs[i, j] = np.linalg.norm(DVvec)
        coordsf[i, j, 0] = lonf
        coordsf[i, j, 1] = latf
        errs[i, j, 0] = lonErr
        errs[i, j, 1] = latErr

#%%
# =============================================================================
# Plots
# =============================================================================
plt.close('all')
plt.rcParams.update({'font.size': 14})
fig = plt.figure()
ax = fig.add_subplot(111)
labs = ['A', 'B', 'C']
for j, (dlon, dlat) in enumerate(zip(dlonList, dlatList)):
    ax.plot(tbackList/86400, DVs[:,j], '.-', label = labs[j], linewidth = 2)
plt.axhline(y = 0.1, color = 'C3', linestyle = '--', linewidth = 2,
            label = r'desired $V_j$')
ax.grid()
ax.legend()
ax.set_xlabel('time from separation to entry, days')
ax.set_ylabel(r'required $V_j$, m/s')

fig = plt.figure()
ax = fig.add_subplot(111)
for j, (dlon, dlat) in enumerate(zip(dlonList, dlatList)):
    ax.plot(tbackList/86400, np.degrees(errs[:,j,0]), '.-', 
            label = '({0:.3f}, {1:.3f})'.format(np.degrees(dlon),
                                                np.degrees(dlat)))
ax.grid()
ax.legend()
ax.set_xlabel('tback, days')
ax.set_ylabel('longitude error, deg')

fig = plt.figure()
ax = fig.add_subplot(111)
for j, (dlon, dlat) in enumerate(zip(dlonList, dlatList)):
    ax.plot(tbackList/86400, np.degrees(errs[:,j,1]), '.-', 
            label = '({0:.3f}, {1:.3f})'.format(np.degrees(dlon),
                                                np.degrees(dlat)))
ax.grid()
ax.legend()
ax.set_xlabel('tback, days')
ax.set_ylabel('latitude error, deg')






















