# -*- coding: utf-8 -*-
"""
getTiming.py:
    computes bounds on event timing for SHIELD, and selects nominal values
Created on Tue Jul 19 17:37:56 2022

@author: Samuel Albert
"""

import sys
sys.path.append('..')

from src import sim, planetaryConstants, atmosphere, conversions, marvel

import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize as opt

plt.close('all')
plt.rcParams.update({'font.size': 16})

#%%
# =============================================================================
# Set up Planet, Vehicle, Params
# =============================================================================
mars = planetaryConstants.MARS
atmfilename = '../data/MarsGRAMNominal.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)
mars.MachFun = lambda r, u: u / np.interp((r - mars.rad), mars.sound[0,:],
                                          mars.sound[1,:])

shield = sim.Vehicle('SHIELD')
marvel.loadSHIELDAeroData(shield)
shield.Rn = 0.85

# must define bank function, but it doesn't matter since L/D = 0
shield.bankFun = lambda t, yy: 0

# initial state
t0 = 0 # s
r0 = (mars.rad + mars.halt) # m
lon0 = np.radians(0)
lat0 = np.radians(0)
u0 = 6e3 # m/s
gam0 = np.radians(-24)
hda0 = np.radians(90) # due-East
y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])
y0sph = conversions.sphPR2cartI(y0sph, mars, t0)
shield.y0 = y0sph

# set deceleration threshold to define entry
g0 = 1 # Earth g

#%%
# =============================================================================
# Find tdeploy such that Mdeploy = 0.9
# =============================================================================
# define objective function
def timeDeploy(tdeploy, Mtarget):
    tjettison = tdeploy + 5
    
    # propagate
    shield.y0 = y0sph
    sol, params = marvel.runSHIELDtimed(mars, shield, tdeploy, tjettison, g0)

    # build Traj object from results
    traj = sim.buildTrajCart(sol, mars)

    # get properties along trajectory
    traj = marvel.analyzeSHIELDTraj(mars, shield, params, traj)
    
    # return Mach deploy error
    print(tdeploy, traj.Mdeploy)
    return traj.Mdeploy - Mtarget

# set target Mach number at deployment
Mtarget = 0.9

res = opt.root_scalar(lambda td: timeDeploy(td, Mtarget), bracket = (50,100), # 12: (150,250), 18: (100,150), 24: (50,100)
                      method = 'brentq')
print(res)
td = res.root

#%%
# =============================================================================
# Find tjettison such that impact velocity = 50 m/s
# =============================================================================
# define objective function
def timeJettison(tjettison, vTarget):
    tdeploy = tjettison - 5
    
    # propagate
    shield.y0 = y0sph
    sol, params = marvel.runSHIELDtimed(mars, shield, tdeploy, tjettison, g0)

    # build Traj object from results
    traj = sim.buildTrajCart(sol, mars)

    # get properties along trajectory
    traj = marvel.analyzeSHIELDTraj(mars, shield, params, traj)
    
    # return impact velocity error
    print(traj.v[-1])
    return traj.v[-1] - vTarget

# set target impact velocity
vTarget = 50 # m/s

res = opt.root_scalar(lambda tj: timeJettison(tj, vTarget), bracket = (td,150), #12: 300, 18: 200, 24: 150
                      method = 'brentq')

print(res)
tj = res.root

#%%
# =============================================================================
# Simulate nominal trajectory
# =============================================================================
# define nominal event times, based on feasible range

## for gam = -24:
tdeploy = 105
tjettison = 115

# ## for gam = -18:
# tdeploy = 140
# tjettison = 150

# ## for gam = -12:
# tdeploy = 225
# tjettison = 235

# propagate
shield.y0 = y0sph
sol, params = marvel.runSHIELDtimed(mars, shield, tdeploy, tjettison, g0)

# build Traj object from results
traj = sim.buildTrajCart(sol, mars)

# get properties along trajectory
traj = marvel.analyzeSHIELDTraj(mars, shield, params, traj)

print(traj.tentry)
print(traj.Mdeploy)
print(traj.v[-1])

# Plots
handles = sim.plotTraj(traj)

print('SHIELD landed at a (lon, lat) of {0:.3f}, {1:.3f} (deg)'\
      .format(np.degrees(traj.lon[-1]), np.degrees(traj.lat[-1])))

# add plot labels for deployment and jettison
fig, ax = handles[0]
ideploy = np.argmax(traj.t >= params.tdeployed)
ax.plot(traj.v[ideploy]/1e3, traj.h[ideploy]/1e3, 'o', label = 'deploy')
ax.annotate('E+{0:.1f} s'.format(traj.t[ideploy]),
             (traj.v[ideploy]/1e3+0.01, traj.h[ideploy]/1e3+0.1), color = 'C1')

ijett = np.argmax(traj.t >= params.tjettisoned)
ax.plot(traj.v[ijett]/1e3, traj.h[ijett]/1e3, 'P', label = 'jettison')
ax.annotate('E+{0:.1f} s'.format(traj.t[ijett]),
             (traj.v[ijett]/1e3, traj.h[ijett]/1e3-1), color = 'C2')

ax.plot(traj.v[-1]/1e3, traj.h[-1]/1e3, 'X', label = 'impact')
ax.annotate('E+{0:.1f} s'.format(traj.t[-1]),
            (traj.v[-1]/1e3+0.01, traj.h[-1]/1e3), color = 'C3')
ax.legend()

#%% plot with shading for feasible range
istart = np.argmax(traj.t >= (td+traj.tentry))
iend = np.argmax(traj.t >= (tj+traj.tentry))

fig, ax = plt.subplots()
ax.set_xlim((-0.02, 1.1))
ax.set_ylim((-0.5,22))
ax.plot(traj.v/1e3, traj.h/1e3)
ax.plot(traj.v[istart:iend]/1e3, traj.h[istart:iend]/1e3, 'C4',
        lw=4, alpha = 0.7, label = 'acceptable')
ax.plot(traj.v[ideploy]/1e3, traj.h[ideploy]/1e3, 'o', label = 'deploy')
ax.annotate('E+{0:.1f} s'.format(traj.t[ideploy]-traj.tentry),
             (traj.v[ideploy]/1e3+0.01, traj.h[ideploy]/1e3+0.1), color = 'C1')
ax.plot(traj.v[ijett]/1e3, traj.h[ijett]/1e3, 'P', label = 'jettison')
ax.annotate('E+{0:.1f} s'.format(traj.t[ijett]-traj.tentry),
             (traj.v[ijett]/1e3, traj.h[ijett]/1e3-1), color = 'C2')
ax.plot(traj.v[-1]/1e3, traj.h[-1]/1e3, 'X', label = 'impact')
ax.annotate('E+{0:.1f} s'.format(traj.t[-1]-traj.tentry),
            (traj.v[-1]/1e3+0.01, traj.h[-1]/1e3), color = 'C3')
ax.set_xlabel('velocity, km/s')
ax.set_ylabel('altitude, km')
ax.grid()
ax.legend()
plt.tight_layout()
