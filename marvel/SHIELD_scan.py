# -*- coding: utf-8 -*-
"""
SHIELD_scan.py:
    explores central trajectories across the parameter space
Created on Mon Mar  7 16:46:10 2022

@author: Samuel Albert
"""

import sys
sys.path.append('..')

from src import sim
from src import planetaryConstants
from src import atmosphere
from src import conversions

import numpy as np
import matplotlib.pyplot as plt

plt.close('all')

#%%
# =============================================================================
# Set up Planet, Vehicle, Params
# =============================================================================
mars = planetaryConstants.MARS
atmfilename = '../data/MarsGRAMNominal.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)

shield = sim.Vehicle('SHIELD')
shield.BC = 10 # kg/m^2
shield.LD = 0
shield.Rn = 1 # 1 m nose radius

# must define bank function, but it doesn't matter since L/D = 0
shield.bankFun = lambda t, yy: 0

# initial state
r0 = (mars.rad + mars.halt) # m
lon0 = np.radians(0)
lat0 = np.radians(0)
u0 = 6e3 # m/s
hda0 = np.radians(90) # due-East

params = sim.Params()
params.rtol = 1e-11
params.atol = 1e-11
params.rmin = mars.rad
params.rmax = (mars.rad + mars.halt)

# initial and final time
params.t0 = 0 # s
params.tf = params.t0 + 5e3 # s
params.tspan = (params.t0, params.tf)

params.event1 = lambda t, y: sim.belowMinAltCart(t, y, params)
params.event1.terminal = True

params.event2 = lambda t, y: sim.aboveMaxAltCart(t, y, params)
params.event2.terminal = True
params.event2.direction = 1
params.events = (params.event1, params.event2)

#%%
# =============================================================================
# Sweep through different EFPA values, analyze and save traj for each
# =============================================================================
gamList = np.arange(-10, -30, -0.5)
# gamList = [-12]
peakqList = []
peakgList = []
velh0List = []

for gam0 in gamList:
    gam0 = np.radians(gam0)
    # update initial state accordingly
    y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])
    shield.y0 = conversions.sphPR2cartI(y0sph, mars, params.t0)
    
    # propagate
    sol = sim.runCartesian(mars, shield, params)
    
    # analyze
    traj = sim.buildTrajCart(sol, mars)
    traj = sim.analyzeTraj(mars, shield, params, traj)
    
    # append certain values
    peakqList.append(traj.peakq)
    peakgList.append(traj.peakg)
    velh0List.append(traj.v[-1])
    
    # sim.plotTraj(traj)
    
peakqList = np.asarray(peakqList)
peakgList = np.asarray(peakgList)
velh0List = np.asarray(velh0List)

#%%
# =============================================================================
# Plots
# =============================================================================
fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(gamList, peakqList/1e4)
ax.grid()
ax.set_xlabel('EFPA, deg')
ax.set_ylabel('peak heat flux, W/cm^2', color = 'C0')
ax.tick_params(axis = 'y', labelcolor = 'C0')

axx = ax.twinx()
axx.plot(gamList, peakgList, 'C1--')
axx.set_ylabel("peak g's, m/s", color = 'C1')
axx.tick_params(axis = 'y', labelcolor = 'C1')

plt.tight_layout()
