# -*- coding: utf-8 -*-
"""
combineManeuvers.py:
    takes individual maneuver files and combines with approriate formatting
Created on Wed Jul 20 17:14:40 2022

@author: Samuel Albert
"""

import numpy as np

#%% import each file
datList = []
datList.append(np.load('../results/maneuver_lon5.npz', allow_pickle = True))
datList.append(np.load('../results/maneuver_lon10.npz', allow_pickle = True))
datList.append(np.load('../results/maneuver_lon15.npz', allow_pickle = True))
datList.append(np.load('../results/maneuver_lon30.npz', allow_pickle = True))
datList.append(np.load('../results/maneuver_lat5.npz', allow_pickle = True))
datList.append(np.load('../results/maneuver_lat10.npz', allow_pickle = True))
datList.append(np.load('../results/maneuver_lat15.npz', allow_pickle = True))
datList.append(np.load('../results/maneuver_lat30.npz', allow_pickle = True))

N = 8

#%% format for networkMC.py
DVvecs = np.empty((N,3))
tbackList = []

for i, dat in enumerate(datList):
    # DVvecs[i,:] = dat['DVinertialList'][0]
    DVvecs[i,:] = dat['DVLVLHList'][0]
    tbackList.append(86400 * dat['dayslist'][0])

# #%% save to single output file
# fname = '../results/maneuvers_large_18_alpha.npz'
# np.savez(fname, DVvecs = DVvecs, tbackList = tbackList)
