# -*- coding: utf-8 -*-
"""
analyzeNetworkMC.py:
    combine and analyze results of Monte Carlo analysis
Created on Tue Nov 30 13:02:33 2021

@author: Samuel
"""

import sys
sys.path.append('..')

from src import conversions
from src import planetaryConstants

# import pickle
import numpy as np
# from scipy import stats
import matplotlib.pyplot as plt

plt.close('all')
plt.rcParams.update({'font.size': 14})
fig = plt.figure()
mars = planetaryConstants.MARS
pind = 5 # probe under current analysis (0:-A, 1:A, 2:-B, 3:B, 4:-C, 5:C)

# =============================================================================
# Load data files
# =============================================================================
fnames = []
fnames.append('../results/networkMC_12_N1000_0526152005.npz')
fnames.append('../results/networkMC_18_N1000_0526152014.npz')
fnames.append('../results/networkMC_24_N1000_0526152022.npz')

# =============================================================================
# Set up desired network geometry
# =============================================================================
hda0 = np.radians(80)

L = np.radians(0.5)
dlonList90 = np.array([2*L, L/2, L/2])
dlatList90 = np.array([0, L/2, -L/(2*np.sqrt(3))])
dlonList = dlonList90 * np.sin(hda0) - dlatList90 * np.cos(hda0)
dlatList = dlatList90 * np.sin(hda0) + dlonList90 * np.cos(hda0)

dlonList = np.append(dlonList, -dlonList)
dlatList = np.append(dlatList, -dlatList)

# =============================================================================
# Analyze data one file at a time
# =============================================================================
for fname in fnames:
    # load data
    lonfsList = []
    latfsList = []
    data = np.load(fname)
    lonfsList.append(data['lonfs'])
    latfsList.append(data['latfs'])
    lonfCenter = data['lonfCenter']
    latfCenter = data['latfCenter']
    
    # below are shape (i, j, k) where i = number of trials, j = number of unique
    #   targets, and k = 2 to contain reciprocal target pairs (i.e. (lon,-lon))
    lonfs = np.concatenate(lonfsList, axis = 0)
    latfs = np.concatenate(latfsList, axis = 0)
    
    # reshape to i x (j*k)
    lonfs = lonfs.reshape(lonfs.shape[0], (lonfs.shape[1] * lonfs.shape[2]))
    latfs = latfs.reshape(latfs.shape[0], (latfs.shape[1] * latfs.shape[2]))
    
    # get coordinates
    coordsList = []
    for i, (lonfsi, latfsi) in enumerate(zip(lonfs, latfs)):
        coordsList.append([(lon,lat) for (lon, lat) in zip(lonfsi, latfsi)])
    
    # get nominal coordinates
    coordsNom = []
    for i in [3, 0, 4, 1, 5, 2]:
        coordsNom.append(((dlonList[i] + lonfCenter), (dlatList[i] + latfCenter)))
    
    # compute distance error for this probe for each trial
    perr = []
    for i, coord in enumerate(coordsList):
        perr.append(conversions.greatCircle(coordsNom[pind][0], coord[pind][0], 
                                            coordsNom[pind][1], coord[pind][1],
                                            mars) / 1e3)
    
    # plot
    plt.hist(perr, bins = 20, rwidth = 0.9, alpha = 0.5,
             label = 'EFPA = -' + fname[21:23])
    plt.legend()

plt.grid()
plt.title('Error magnitude for probe C')
plt.xlabel('error, km')
plt.ylabel('frequency')

    















