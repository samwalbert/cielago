# -*- coding: utf-8 -*-
"""
testmarvel.py:
    tests B-plane-related functions
Created on Fri Nov 26 15:08:12 2021

@author: Samuel
"""

import sys
sys.path.append('..')

from src import sim
from src import planetaryConstants
from src import atmosphere
from src import conversions
from src import marvel

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

# plt.rcParams.update({'font.size': 20})
    
    

#%%
# =============================================================================
# Set up Planet, Vehicle, Params
# =============================================================================
mars = planetaryConstants.MARS
atmfilename = '../data/MarsGRAMNominal.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)

shield = sim.Vehicle('SHIELD')
shield.BC = 10 # kg/m^2
shield.LD = 0

# must define bank function, but it doesn't matter since L/D = 0
shield.bankFun = lambda t, yy: 0

# initial state
r0 = (mars.rad + mars.halt) # m
lon0 = np.radians(0)
lat0 = np.radians(0)
u0 = 6e3 # m/s
gam0 = np.radians(-12)
hda0 = np.radians(90)
y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])

params = sim.Params()
params.rtol = 1e-11
params.atol = 1e-11
params.rmin = mars.rad
params.rmax = (mars.rad + mars.halt)

# initial and final time
params.t0 = 1e6 # s
params.tf = params.t0 + 5e3 # s

shield.y0 = conversions.sphPR2cartI(y0sph, mars, params.t0)

params.event1 = lambda t, y: sim.belowMinAltCart(t, y, params)
params.event1.terminal = True

params.event2 = lambda t, y: sim.aboveMaxAltCart(t, y, params)
params.event2.terminal = True
params.event2.direction = 1

#%%
# =============================================================================
# Get center states at sep time and at surface
# =============================================================================
params.tback = 1 * 24 * 60 * 60 # s
xx0vecCenter, xxfsphCenter = marvel.getCenterState(mars, shield, params)
x0vecCenter = xx0vecCenter[:3]
v0vecCenter = xx0vecCenter[3:]
lonfCenter = xxfsphCenter[1]
latfCenter = xxfsphCenter[2]

lonfCenterd = np.degrees(lonfCenter)
latfCenterd = np.degrees(latfCenter)

#%%
# =============================================================================
# Get Jacobian
# =============================================================================
params.pert = 1e-4
J = marvel.getJ(mars, shield, params, xx0vecCenter, xxfsphCenter)


#%%
# =============================================================================
# Target points with increasing delta-downrange, report DV for each
# =============================================================================
dlons = np.array([0, 0.25, 0.5, 0.75, 1, 2, 3, 4, 5, 6, 6.25, 6.5, 7])
dlats = np.zeros(dlons.shape)

DVvecList = []
DVList = []
lonfList = []
latfList = []
lonErrList = []
latErrList = []
errDist = []


for dlon, dlat in zip(dlons, dlats):
    
    DVvec, (lonf, latf), (lonErr, latErr) = \
        marvel.targetLonLat(mars, shield, params, xx0vecCenter, xxfsphCenter,
                            J, np.radians(dlon), np.radians(dlat))
    
    # record
    DVvecList.append(DVvec)
    DVList.append(np.linalg.norm(DVvec))
    lonfList.append(lonf)
    latfList.append(latf)
    lonErrList.append(lonErr)
    latErrList.append(latErr)
    lonDes = lonfCenter + np.radians(dlon)
    latDes = latfCenter + np.radians(dlat)
    errDist.append(conversions.greatCircle(lonDes, lonf, latDes, latf,
                                           mars))
    
    # print
    print('({0:.3f}, {1:.3f}) deg'.format(dlon, dlat))
    print('longitude error: {0:.3e} deg'.format(np.degrees(lonErr)))
    print('latitude error: {0:.3e} deg'.format(np.degrees(latErr)))
    
    print()


#%%
# =============================================================================
# Plots
# =============================================================================
plt.close('all')

def lon2dist(lon):
    return np.radians(lon) * mars.rad / 1e3

def dist2lon(dist):
    return np.degrees(dist * 1e3 / mars.rad)

fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
ax1.plot(dlons, np.asarray(errDist)/1e3, 'C0o-', label = 'downrange error')
ax1.set_ylim(bottom = -50, top = 500)
ax1.grid()
ax1.set_xlabel('angle, deg')
ax1.set_ylabel('error, km')
# ax1.tick_params(axis = 'y', labelcolor = 'C0')
# ax1.yaxis.label.set_color('C0')
secax = ax1.secondary_xaxis('top', functions = (lon2dist, dist2lon))
secax.set_xlabel('distance, km')

ax2 = ax1.twinx()
ax2.set_ylabel(r'$V_j$, m/s')
ax2.plot(dlons, DVList, 'C0:', label = r'downrange $\Delta V$')
# ax2.tick_params(axis = 'y', labelcolor = 'C1')
# ax2.yaxis.label.set_color('C1')

#%% analyze direction of computed delta-V
rhat, thetahat, hhat = conversions.getLVLH(xx0vecCenter[:3], xx0vecCenter[3:])
DVhat = DVvecList[1] / np.linalg.norm(DVvecList[1])
DVr = np.dot(DVhat, rhat)
DVtheta = np.dot(DVhat, thetahat)
DVh = np.dot(DVhat, hhat)


#%%
# =============================================================================
# Repeat varying latitude (crossrange)
# =============================================================================
dlats = np.array([0, 0.25, 0.5, 0.75, 1, 2, 3, 4, 5, 6, 6.25, 6.5, 7]) 
dlons = np.zeros(dlats.shape)

DVvecList = []
DVList = []
lonfList = []
latfList = []
lonErrList = []
latErrList = []
errDist = []

for dlon, dlat in zip(dlons, dlats):
    
    DVvec, (lonf, latf), (lonErr, latErr) = \
        marvel.targetLonLat(mars, shield, params, xx0vecCenter, xxfsphCenter,
                            J, np.radians(dlon), np.radians(dlat))
    
    # record
    DVvecList.append(DVvec)
    DVList.append(np.linalg.norm(DVvec))
    lonfList.append(lonf)
    latfList.append(latf)
    lonErrList.append(lonErr)
    latErrList.append(latErr)
    lonDes = lonfCenter + np.radians(dlon)
    latDes = latfCenter + np.radians(dlat)
    errDist.append(conversions.greatCircle(lonDes, lonf, latDes, latf,
                                           mars))
    
    # print
    print('({0:.3f}, {1:.3f}) deg'.format(dlon, dlat))
    print('longitude error: {0:.3e} deg'.format(np.degrees(lonErr)))
    print('latitude error: {0:.3e} deg'.format(np.degrees(latErr)))
    
    print()

#%%
ax1.plot(dlats, np.asarray(errDist)/1e3, 'C1o-', label = 'crossrange error')

ax2.plot(dlats, DVList, 'C1:', label = r'crossrange $\Delta V$')

# deal with legend
lab1 = mlines.Line2D([], [], color = 'C0', marker = 'o', ls='-',
                     label = 'downrange error')
lab2 = mlines.Line2D([], [], color = 'C0', marker = '', ls=':',
                     label = r'downrange $V_j$')
lab3 = mlines.Line2D([], [], color = 'C1', marker = 'o', ls='-',
                     label = 'crossrange error')
lab4 = mlines.Line2D([], [], color = 'C1', marker = '', ls=':',
                     label = r'crossrange $V_j$')
# leg = ax1.legend()
# leg.legendHandles[0].set_color('k')
# leg.legendHandles[1].set_color('k')
plt.legend(handles = [lab1, lab2, lab3, lab4])

#%% analyze direction of computed delta-V
rhat, thetahat, hhat = conversions.getLVLH(xx0vecCenter[:3], xx0vecCenter[3:])
DVhat = DVvecList[1] / np.linalg.norm(DVvecList[1])
DVr = np.dot(DVhat, rhat)
DVtheta = np.dot(DVhat, thetahat)
DVh = np.dot(DVhat, hhat)

























