# -*- coding: utf-8 -*-
"""
SHIELD_distribute.py:
    find DV required to achieve range of separations for range of sep times
Created on Mon Mar 21 21:44:51 2022

@author: Samuel Albert
"""

import sys
sys.path.append('..')

from src import sim
from src import planetaryConstants
from src import atmosphere
from src import conversions
from src import marvel

import numpy as np
import matplotlib.pyplot as plt

plt.close('all')

#%%
# =============================================================================
# Set up Planet, Vehicle, Params
# =============================================================================
mars = planetaryConstants.MARS
atmfilename = '../data/MarsGRAMNominal.txt'
mars.hList, mars.rhoList = atmosphere.readMarsGRAM(atmfilename)
mars.rhoFun = lambda r: np.interp((r - mars.rad), mars.hList, mars.rhoList)

shield = sim.Vehicle('SHIELD')
shield.BC = 10 # kg/m^2
shield.LD = 0
shield.getAero = lambda r, u, Planet: (shield.BC, shield.LD)

# must define bank function, but it doesn't matter since L/D = 0
shield.bankFun = lambda t, yy: 0

# initial state
r0 = (mars.rad + mars.halt) # m
lon0 = np.radians(0)
lat0 = np.radians(0)
u0 = 6e3 # m/s
gam0 = np.radians(-12)
hda0 = np.radians(90) # due-East
y0sph = np.array([r0, lon0, lat0, u0, gam0, hda0])

params = sim.Params()
params.rtol = 1e-11
params.atol = 1e-11
params.rmin = mars.rad
params.rmax = (mars.rad + mars.halt)

# initial and final time
params.t0 = 1e6 # s
params.tf = params.t0 + 5e3 # s

shield.y0 = conversions.sphPR2cartI(y0sph, mars, params.t0)

params.event1 = lambda t, y: sim.belowMinAltCart(t, y, params)
params.event1.terminal = True

params.event2 = lambda t, y: sim.aboveMaxAltCart(t, y, params)
params.event2.terminal = True
params.event2.direction = 1

params.pert = 1e-4

#%%
# =============================================================================
# Get center states at surface
# =============================================================================
params.tback = 0 # s
_, xxfsphCenter = marvel.getCenterState(mars, shield, params)
lonfCenter = xxfsphCenter[1]
latfCenter = xxfsphCenter[2]

#%%
# =============================================================================
# Set Desired distances and times
# =============================================================================
tbackList = np.array([0.5, 1, 2]) * 86400.0
# distList = np.array([50, 500, 5000]) * 1000.0
distList = np.array([50]) * 1000.0

#%%
# =============================================================================
# Loop through each permutation
# =============================================================================
for dist in distList:
    for tback in tbackList:
        params.tback = tback
        print('\n{0:.1f} km separation, E-{1:.1f} day jettison time:'.\
              format(dist/1e3, tback/86400))
        print('    along-track:')
        dlon = dist/mars.rad
        dlat = 0
        DVvec, (lonf, latf), (lonErr, latErr) = marvel.sep2surface(mars,
                                                                   shield,
                                                                   params,
                                                                   dlon,
                                                                   dlat)
        sep = conversions.greatCircle(lonfCenter, lonf, latfCenter, latf, mars)
        print('        achieved separation of {0:.3f} km on the surface via'\
              ' {1:.3f} m/s maneuver'.format(sep/1e3, np.linalg.norm(DVvec)))
        print('    cross-track:')
        dlon = 0
        dlat = dist/mars.rad
        DVvec, (lonf, latf), (lonErr, latErr) = marvel.sep2surface(mars,
                                                                   shield,
                                                                   params,
                                                                   dlon,
                                                                   dlat)
        sep = conversions.greatCircle(lonfCenter, lonf, latfCenter, latf, mars)
        print('        achieved separation of {0:.3f} km on the surface via'\
              ' {1:.3f} m/s maneuver'.format(sep/1e3, np.linalg.norm(DVvec)))

# #%%
# # =============================================================================
# # Test, for sep time of 1 day and 50 km along-track separation
# # =============================================================================
# params.tback = 86400/2
# dist = 50
# dlon = (dist * 1e3 / mars.rad)
# dlat = 0
# DVvec, (lonf, latf), (lonErr, latErr) = marvel.sep2surface(mars, shield, params, dlon, dlat)
# sep = conversions.greatCircle(lonfCenter, lonf, latfCenter, latf, mars)
# print('achieved separation of {0:.3f} km on the surface via {1:.3f} m/s maneuver'.format(sep/1e3, np.linalg.norm(DVvec)))
