# -*- coding: utf-8 -*-
"""
testUQ.py:
    test script for developing UQ functions
Created on Wed Nov 17 11:39:33 2021

@author: Samuel Albert
"""

import numpy as np
from scipy import stats
import matplotlib.pyplot as plt

from src import UQ

plt.close('all')

filename = 'data/MarsGRAM_MC5000/all_perts.npz'
# filename = 'data/MarsGRAM_MC5000/all.npz'
pfact = 1
alpha = 0.95

# get eigenproblem solution
evals, evecs, rhoSampMean, d, h = UQ.getEproblem(filename, pfact, alpha)

# # overwrite to d = 50
# d = 50

# now adjust these values from perturbations to actual density values
dat = np.load(filename)
h = dat['h']
rhoPerts = dat['rhoData']
rhoMean = dat['rhoMean']
# rhoFun, Ys = UQ.getKLEfun(evals, evecs, rhoSampMean, d, h, rhoMean, True)

# # get GRAM density values
# rhoGRAM = (rhoPerts[:,0] + 1) * rhoMean

# fig = plt.figure()
# ax = fig.add_subplot(111)
# plt.semilogx(rhoGRAM, h)
# plt.semilogx(rhoFun(h), h)

# compute and plot perturbation values
fig = plt.figure()
ax = fig.add_subplot(111)
rhoGRAMList = []
rhoKLEList = []
for i in range(500):
    rhoPertFun, Ys = UQ.getKLEfun(evals, evecs, rhoSampMean, d, h, rhoMean, False)
    # print(Ys[0])
    rhoPertGRAM = rhoPerts[:,i]
    rhoPertKLE = rhoPertFun(h)
    rhoGRAMList.append(rhoPertGRAM)
    rhoKLEList.append(rhoPertKLE)
    if i < 3:
        # ax.plot(rhoPertGRAM, h/1e3, 'C2', alpha = 0.5)
        ax.plot(rhoPertGRAM, h/1e3, alpha = 0.5)
        # ax.plot(rhoPertFun(h), h/1e3, 'C1', alpha = 0.5)
    
ax.grid()


rhoGRAM = np.asarray(rhoGRAMList)
rhoKLE = np.asarray(rhoKLEList)

KLEMEAN = np.mean(rhoKLE, axis = 0)
KLESTD = np.std(rhoKLE, axis = 0)

GRAMMEAN = np.mean(rhoGRAM, axis = 0)
GRAMSTD = np.std(rhoGRAM, axis = 0)

# ax.plot(KLEMEAN - 3 * KLESTD, h/1e3, 'C1--', linewidth = 3)
# ax.plot(KLEMEAN + 3 * KLESTD, h/1e3, 'C1--', linewidth = 3, label = 'KLE')
ax.plot(GRAMMEAN - 3 * GRAMSTD, h/1e3, 'C2--', linewidth = 3)
ax.plot(GRAMMEAN + 3 * GRAMSTD, h/1e3, 'C2--', linewidth = 3, label = r'$3\sigma$ bounds')
ax.legend()
ax.set_xlabel('normalized density perturbation')
ax.set_ylabel('altitude, km')
plt.tight_layout()

fig, ax = plt.subplots()
ind = 100
ax.hist(rhoPerts[ind,:], color = 'C2', bins='auto', density=True, label='histogram')
rv = stats.norm(loc=np.mean(rhoPerts[ind,:]), scale=np.std(rhoPerts[ind,:]))
x_eval = np.linspace(rhoPerts[ind,:].min(), rhoPerts[ind,:].max(), 1000)
ax.plot(x_eval, rv.pdf(x_eval), 'C1', label='Gaussian pdf', linewidth=4)
ax.grid()
ax.legend()
ax.set_xlabel('normalized density perturbation')
ax.set_ylabel('probability')


    










