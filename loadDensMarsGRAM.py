# -*- coding: utf-8 -*-
"""
loadDensMarsGRAM.py:
    utility script to load large MarsGRAM data file and save as .npz archive
Created on Wed Nov 17 11:16:24 2021

@author: Samuel Albert
"""


import numpy as np

from src import atmosphere

# =============================================================================
# NOTES: assumes all files are generated the same way and have the same h prof.
# =============================================================================

datadir = 'data/MarsGRAM_MC5000/'
N = 5000

# load single profile to get length
hList, rhoList = atmosphere.readMarsGRAM(datadir + '1.txt', True)
p = len(rhoList)

# get GRAM mean profile
h, rhoMean = atmosphere.readMarsGRAM(datadir + '1.txt', False)

# create blank numpy arrays
rhoData = np.empty((p, N))
rhoData[:] = np.NaN
rhoPerts = np.empty((p, N))
rhoPerts[:] = np.NaN

# load each file and add to data block
for i in range(N):
    fname = datadir + str(i+1) + '.txt'
    _, rhoList = atmosphere.readMarsGRAM(fname, True) # dispersed density
    rhoData[:,i] = rhoList
    rhoPerts[:,i] = rhoList/rhoMean - 1

# save data to .npz file
outname = datadir + 'all.npz'
np.savez(outname,
         h = h,
         rhoData = rhoData,
         rhoMean = rhoMean)

# now save another file that has density perturbations instead of values
outname2 = datadir + 'all_perts.npz'
np.savez(outname2,
         h = h,
         rhoData = rhoPerts,
         rhoMean = rhoMean)